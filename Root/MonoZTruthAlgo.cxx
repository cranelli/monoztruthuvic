// #include <EventLoop/Job.h>
// #include <EventLoop/StatusCode.h>
// #include <EventLoop/Worker.h>
#include <MonoZTruthUVic/MonoZTruthAlgo.h>
// #include <MonoZUVic/MonoZEvent.h>
// #include <MonoZUVic/MonoZHistos.h>
// #include <MonoZUVic/MonoZSelections.h>
// #include "xAODEventInfo/EventInfo.h"
// #include "xAODTruth/TruthEventContainer.h"
// #include "xAODTruth/TruthParticleContainer.h"
// #include "xAODTruth/TruthVertexContainer.h"
// ? #include "xAODTruth/TruthParticle.h"
// #include "xAODMissingET/MissingETContainer.h"
// #include "xAODJet/JetContainer.h"
// #include "xAODJet/JetAuxContainer.h"
// #include "xAODJet/JetConstituentVector.h"
// #include "xAODJet/Jet.h"
#include "xAODBase/IParticleContainer.h"
// #include "xAODRootAccess/Init.h"
// #include "xAODRootAccess/TEvent.h"
// #include "xAODRootAccess/tools/Message.h"
// #include "xAODRootAccess/TStore.h"
#include "TSystem.h"

#include <MonoZUVic/MonoZEvent.h>
#include <MonoZUVic/MonoZHistos.h>
#include <MonoZUVic/MonoZSelections.h>
#include <MonoZUVic/MonoZSumOfWeights.h>
#include <MonoZUVic/MonoZTools.h>

// accessors for decorations for preselection
//static SG::AuxElement::ConstAccessor<char> lowPtAcc("lowPt");
//static SG::AuxElement::ConstAccessor<char> highPtAcc("highPt");
//static SG::AuxElement::ConstAccessor<char> vetoAcc("veto");
//static SG::AuxElement::ConstAccessor<char> selectAcc("selected");
static SG::AuxElement::ConstAccessor<int>  ghostBHadronsAcc("GhostBHadronsFinalCount");
// decorations for overlap removal
//static SG::AuxElement::ConstAccessor<char> overlapAcc("overlaps");


MonoZTruthAlgo :: MonoZTruthAlgo ()
// MonoZTruthAlgo :: MonoZTruthAlgo () :
// m_truthWeightTool ("PMGTools::PMGTruthWeightTool/truthWeightTool", this)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
  m_doTruth1 = false;
}

// inherited from MonoZAlgo
// EL::StatusCode MonoZTruthAlgo :: setupJob (EL::Job& job) {}

// inherited from MonoZAlgo
// EL::StatusCode MonoZTruthAlgo :: histInitialize () {}

EL::StatusCode MonoZTruthAlgo :: fileExecute ()
{
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    ANA_MSG_INFO("In MonoZTruthAlgo::fileExecute()");
    
    m_event = wk()->xaodEvent();

    // Since proper handling of the weights depends on the TRUTH Derivation,
    // best to have the user specify in MonoZTruthRun.
    /* 
    //Use presence of TruthBSM Container to check whether file is a 
    //TRUTH1 or TRUTH3 DAOD
    m_doTruth1 = false;  //By Default assume TRUTH3 DAOD
    const xAOD::EventFormat * inputFormat = m_event->inputEventFormat();
    if(!inputFormat->exists( "TruthBSM" )){ 
      m_doTruth1 = true;
    }
    */

    // xAOD::TEvent::connectM... WARNING Branch "CutBookkeepers" not available on input
    // FIXME: does not work, problem in getSumOfWeightsInfo()
    // ANA_CHECK(this->getSumOfWeightsInfo());
    // fill the weights meta data
    // m_MonoZSumOfWeights->fill(m_sumOfWeightsInfo);
    // m_MonoZSumOfWeights->print(m_sumOfWeightsInfo);
    
    return EL::StatusCode::SUCCESS;
}

// inherited from MonoZAlgo
// EL::StatusCode MonoZTruthAlgo :: changeInput (bool firstFile) {}

EL::StatusCode MonoZTruthAlgo :: initialize ()
{
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.
    ANA_MSG_INFO("In MonoZTruthAlgo::initialize()");
    
    // clear some MonoZAlgo event variables
    this->clear();
    
    // constants
    m_GeV = 1000.0;
    
    // GENERAL TOOLS ******
    // PMG truth weight tool
    ANA_CHECK(m_truthWeightTool.retrieve());
    // METSignificance Tool (For Truth-Based MET Significance)
    // Following VHbb Example
    //ANA_CHECK(m_METSignificanceTool.setProperty("OutputLevel", MSG::VERBOSE));
    ANA_CHECK(m_METSignificanceTool.setProperty("SoftTermParam", 0));
    ANA_CHECK(m_METSignificanceTool.setProperty("SoftTermReso", 0));
    ANA_CHECK(m_METSignificanceTool.setProperty("TreatPUJets",   false));
    ANA_CHECK( m_METSignificanceTool.setProperty("DoPhiReso",     true) );
    ANA_CHECK(m_METSignificanceTool.setProperty("IsDataJet",   false));
    //ANA_CHECK( m_METSignificanceTool.setProperty("DoJerForEMu",   false) );
    ANA_CHECK(m_METSignificanceTool.setProperty("IsAFII",   false));
    ANA_CHECK(m_METSignificanceTool.retrieve())
    
    
    // count number of events
    m_eventCounter = 0;
    
    // overlap removal checks
    // electron events
    m_nTotElRem_e = 0, m_nTotMuRem_e = 0, m_nTotJetRem_e = 0;
    m_nTotElSel_e = 0, m_nTotMuSel_e = 0, m_nTotJetSel_e = 0;
    // muon events
    m_nTotElRem_m = 0, m_nTotMuRem_m = 0, m_nTotJetRem_m = 0;
    m_nTotElSel_m = 0, m_nTotMuSel_m = 0, m_nTotJetSel_m = 0;
    
    
    // instantiate MonoZClasses
    // MonoZEvent
    m_MonoZEvent = new MonoZEvent();
    
    // init selections and related tags
    m_MonoZSelections = new MonoZSelections();
    ANA_MSG_INFO("Regions:");
    m_MonoZSelections->setMasterRegions(m_userRegionsList);
    m_MonoZSelections->applyWeightsAtALL(m_applyWeightsAtALL);
    m_MonoZSelections->print();
    
    // as a check, let's see the number of events in our xAOD
    m_event = wk()->xaodEvent();
    ANA_MSG_INFO("Number of events = " << m_event->getEntries()); // print long long int
    
    // get sampleName, filled in run script under "sampleName"
    {
        TString sampleName = wk()->metaData()->castString("sampleName"); // could also use "sample_name" already defined by RootCore
        ANA_MSG_INFO("sample name: " << sampleName.Data());
    }
    
    // BOOK HISTOGRAMS **** (Associated to MonoZHistos, filled by MonoZSelections using MonoZEvent)
    {
        MonoZHistos *monoZh = new MonoZHistos();
        TString sysTag = ""; // nominal
        monoZh->setHistoNameTag(sysTag);
        std::cout << " Booking MonoZHistos for sysTag = " << sysTag << ((sysTag == "") ? "(nominal)" : "") << std::endl;
        monoZh->book(m_MonoZSelections);
        m_MonoZHistosMap[sysTag] = monoZh;
        std::vector<TObject*> hV =  monoZh->getHistoVector();
        for (TObject* h : hV) wk()->addOutput(h);
    }
    
    return EL::StatusCode::SUCCESS;
}


EL::StatusCode MonoZTruthAlgo :: execute ()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.
    
    // debugging
    int nDebugEvents = 10;
    
    // ********** EVENT/PARTICLE RETRIEVAL ********** //
    
    // obtain the event
    // m_event = wk()->xaodEvent();
    
    // actions on MonoZAlgo variables that are not cleared by this->clear()
    m_nEntries = m_event->getEntries();
    if ((m_eventCounter % 1000) == 0) ANA_MSG_INFO("Event #" << m_eventCounter);
    // retrieve event information
    ANA_CHECK(m_event->retrieve(m_eventInfo, "EventInfo"));
    
    m_isMC = m_eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);
    if (!m_isMC) {
        ANA_MSG_INFO("Not an MC event!!!");
        return EL::StatusCode::SUCCESS;
    }
    m_eventNumber     = m_eventInfo->eventNumber();
    m_lbNum           = m_eventInfo->lumiBlock();
    m_tStamp          = m_eventInfo->timeStamp();
    m_bcIdNum         = m_eventInfo->bcid();
    m_runNumber       = m_eventInfo->runNumber(); // data: run number;  MC: unique identifier for MC condition (campaign.x.y label)
    m_avIntPerXing    = m_eventInfo->averageInteractionsPerCrossing();
    m_mcChannelNumber = m_eventInfo->mcChannelNumber(); // MC datasetID
    
    // get MC weight
    // look for weightName == "" using PMG tool.  If not found, use m_eventInfo->mcEventWeights()[0]
    std::vector<std::string> wv = m_truthWeightTool->getWeightNames();
    std::vector<std::string>::iterator it = std::find(wv.begin(), wv.end(), "");
    if (it != wv.end()) {
        m_mcWeight = m_truthWeightTool->getWeight(*it);
    } else {
        m_mcWeight = m_eventInfo->mcEventWeights()[0];
    }
    m_extraWeight = this->getExtraWeight(); // the MonoZAlgo implementation always return 1
    
    // monitoring on first events
    if (m_eventCounter < nDebugEvents) {
        // look for nominal weight
        std::cout << "Event # " << m_eventCounter << "; nominal weight [0] = " << m_eventInfo->mcEventWeights()[0] << std::endl;
        std::vector<std::string> wv = m_truthWeightTool->getWeightNames();
        std::vector<std::string>::iterator it = std::find(wv.begin(), wv.end(), "");
        if (it != wv.end()) {
            std::cout << "Event # " << m_eventCounter << "; nominal weight \"\" = " << m_truthWeightTool->getWeight(*it) << " --> will be used" << std::endl;
            if (m_eventInfo->mcEventWeights()[0] != m_truthWeightTool->getWeight(*it)) {
                std::cout << "WARNING: EventWeight\"\" not equal to EventWeight[0]!!! But EventWeight\"\" used" << std::endl;
            }
        } else {
            std::cout << "Event # " << m_eventCounter << "; nominal weight \"\" not found" << std::endl;
        }
        // print truth event weights vector and names for first events
        for (auto weightName : m_truthWeightTool->getWeightNames()) {
            std::cout << "  weight name: [" << weightName << "] = " << m_truthWeightTool->getWeight(weightName) << std::endl;
        }
        
        ANA_MSG_INFO("Event number from eventInfo = " << m_eventNumber);
        ANA_MSG_INFO("mcChannelNumber = " << m_mcChannelNumber); // *** for MC should be the datasetID???
        ANA_MSG_INFO("Event MCweight = " << m_mcWeight);
        ANA_MSG_INFO("runNumber = " << m_runNumber); // *** for MC this should be randRunNumber of a data run???
    }
    
    this->clear();
    m_MonoZEvent->clear(); // FIXME: is this really needed?  setEvent should be complete...
    
    // event weight (used in object selection and event preselection cutflow histograms)
    m_weight = m_mcWeight * m_extraWeight;
    
    // store MC weights in m_sysMCWeightsMap
    // loop over types of MCWeights variations (interPDF, intraPDF, QCDSCale, MGWeights)
    for (TString sType : m_sysMCWeightTypes) {
        int n = 0; // keep track of sys index
        // loop over all MC weights and look for relevant weights
        for (auto weightName : m_truthWeightTool->getWeightNames()) {
            TString wName = weightName;
            if (isSysMCWeight(sType, wName)) {
                ++n;
                TString sTag = sType; sTag += n;
                if (sType == "MGWeights") sTag = sType + "_" + wName;
                m_sysMCWeightsMap[sTag] = m_truthWeightTool->getWeight(weightName);
            }
        }
        
        // book related histos on first event
        // put normal ones in m_MonoZHistosMap
        // special histos to keep track of sum of weights for all events kept as MonoZAlgo members
        if (m_eventCounter == 0) {
            this->bookMCWeightVariationsHistos();
        }
        
        // fill special histos to keep track of sum of weights for MC weight variations (PDF, QCD scale) for all events
        this->fillSumOfMCWeightsHistos();
    }
    
    // retrieve containers
    const xAOD::TruthEventContainer* truthEventContainer = NULL;
    ANA_CHECK(m_event->retrieve(truthEventContainer, "TruthEvents"));
    const xAOD::TruthParticleContainer* electrons = NULL;
    ANA_CHECK(m_event->retrieve(electrons, "TruthElectrons"));
    const xAOD::TruthParticleContainer* muons = NULL;
    ANA_CHECK(m_event->retrieve(muons, "TruthMuons"));
    const xAOD::TruthParticleContainer* taus = NULL;
    ANA_CHECK(m_event->retrieve(taus, "TruthTaus"));
    const xAOD::TruthParticleContainer* photons = NULL;
    ANA_CHECK(m_event->retrieve(photons, "TruthPhotons"));
    const xAOD::MissingETContainer* met = NULL;
    ANA_CHECK(m_event->retrieve(met, "MET_Truth"));
    // Containers that depend on the Truth Derivation
    const xAOD::JetContainer* jets = NULL;
    const xAOD::TruthParticleContainer* particles = NULL;
    const xAOD::TruthParticleContainer* bsm_particles = NULL;
    if(m_doTruth1) { // For TRUTH1_DAOD
      ANA_CHECK(m_event->retrieve(jets, "AntiKt4TruthJets"));
      ANA_CHECK(m_event->retrieve(particles, "TruthParticles"));
    } else { // Default TRUTH3_DAOD
      ANA_CHECK(m_event->retrieve(jets, "AntiKt4TruthDressedWZJets")); 
      ANA_CHECK(m_event->retrieve(bsm_particles, "TruthBSM")); 
    }
    
    if (m_eventCounter < nDebugEvents) {
        ///float mcTruthWeight = 0;  //just for debug print
        //for (xAOD::TruthEventContainer::const_iterator itr = truthEventContainer->begin(); itr != truthEventContainer->end(); ++itr) {
        //mcTruthWeight = (*itr)->weights()[0];
        //}
        // ANA_MSG_INFO("Event truth weight = " << mcTruthWeight);
        //ANA_MSG_INFO("Number of truth particles = " << particles->size());
      if(m_doTruth1) {
	ANA_MSG_INFO("Using TRUTH1 DAOD Settings"); //For TRUTH1_DAOD
	ANA_MSG_INFO("Number of particles = " << particles->size()); //For TRUTH1_DAOD
      } else {
	ANA_MSG_INFO("Using TRUTH3 DAOD Settings"); //For TRUTH3_DAOD
	ANA_MSG_INFO("Number of bsm particles = " << bsm_particles->size()); //For TRUTH3_DAOD
      }
      ANA_MSG_INFO("Number of truth electrons = " << electrons->size());
      ANA_MSG_INFO("Number of truth muons = " << muons->size());
      ANA_MSG_INFO("Number of truth jets = " << jets->size());
    }
    
    // ********** OBJECT PRESELECTION: ELECTRONS ********** //
    
    auto  lowPtElectrons = std::make_unique<xAOD::TruthParticleContainer>();
    auto lowPtElectronsAux = std::make_unique<xAOD::AuxContainerBase>();
    lowPtElectrons->setStore(lowPtElectronsAux.get());
    
    auto  selectedElectrons = std::make_unique<xAOD::TruthParticleContainer>();
    auto selectedElectronsAux = std::make_unique<xAOD::AuxContainerBase>();
    selectedElectrons->setStore(selectedElectronsAux.get());
    
    // loop over truth electrons
    if (m_eventCounter < nDebugEvents) Info("execute()", "\t Looping over truth electrons...");
    for (unsigned int i = 0; i < electrons->size(); ++i) {
        const xAOD::TruthParticle* electron = electrons->at(i);
        
        // only look at status 1 electrons
        if (electron->status() != 1) continue;
        
        // debug printouts
        if (m_eventCounter < nDebugEvents) {
            Info("execute()", "\t\t Barcode = %i", electron->barcode());
            Info("execute()", "\t\t  PDG ID = %i", electron->pdgId());
            Info("execute()", "\t\t  Status = %i", electron->status());
            Info("execute()", "\t\t      pT = %f GeV", electron->pt()/m_GeV);
            Info("execute()", "\t\t     eta = %f", electron->eta());
        }
        
        // electron preselections
        // obtain low pT electrons
        if (electron->pt() > 7.*m_GeV && std::abs(electron->eta()) < 2.47) {
            xAOD::TruthParticle * lowPtElectron = new xAOD::TruthParticle;
            lowPtElectrons->push_back(lowPtElectron);
            *lowPtElectron = *electron;
        }
        // obtain selected electrons
        if (electron->pt() > 20.*m_GeV && std::abs(electron->eta()) < 2.47) {
            xAOD::TruthParticle * selectedElectron = new xAOD::TruthParticle;
            selectedElectrons->push_back(selectedElectron);
            *selectedElectron = *electron;
        }
    } // end loop over truth electrons
    if (m_eventCounter < nDebugEvents) Info("execute()", "\t\t\t Number of truth electrons = %i", (int)electrons->size());
    
    
    // ********** OBJECT PRESELECTION: MUONS ********** //
    
    auto  lowPtMuons = std::make_unique<xAOD::TruthParticleContainer>();
    auto lowPtMuonsAux = std::make_unique<xAOD::AuxContainerBase>();
    lowPtMuons->setStore(lowPtMuonsAux.get());
    
    auto  selectedMuons = std::make_unique<xAOD::TruthParticleContainer>();
    auto selectedMuonsAux = std::make_unique<xAOD::AuxContainerBase>();
    selectedMuons->setStore(selectedMuonsAux.get());
    
    // loop over truth muons
    if (m_eventCounter < nDebugEvents) Info("execute()", "\t Looping over truth muons...");
    for (unsigned int i = 0; i < muons->size(); ++i) {
        const xAOD::TruthParticle* muon = muons->at(i);
        
        // only look at status 1 muons
        if (muon->status() != 1) continue;
        
        // debug printouts
        if (m_eventCounter < nDebugEvents) {
            Info("execute()", "\t\t Barcode = %i", muon->barcode());
            Info("execute()", "\t\t  PDG ID = %i", muon->pdgId());
            Info("execute()", "\t\t  Status = %i", muon->status());
            Info("execute()", "\t\t      pT = %f GeV", muon->pt()/m_GeV);
        }
        
        // muon preselections
        // obtain low pT muons
        if (muon->pt() > 7.*m_GeV && std::abs(muon->eta()) < 2.5) {
            xAOD::TruthParticle * lowPtMuon = new xAOD::TruthParticle;
            lowPtMuons->push_back(lowPtMuon);
            *lowPtMuon = *muon;
            //muon->auxdecor<char>("lowPt") = 1;
        }
        // obtain selected muons
        if (muon->pt() > 20.*m_GeV && std::abs(muon->eta()) < 2.5) {
            xAOD::TruthParticle * selectedMuon = new xAOD::TruthParticle;
            selectedMuons->push_back(selectedMuon);
            *selectedMuon = *muon;
            //muon->auxdecor<char>("selected") = 1;
        }
    } // end loop over truth muons
    
    if (m_eventCounter < nDebugEvents) Info("execute()", "\t\t\t Number of truth muons = %i", (int)muons->size());
    
    
    // ********** OBJECT PRESELECTION: JETS ********** //
    
    auto  selectedJets = std::make_unique<xAOD::JetContainer>();
    auto selectedJetsAux = std::make_unique<xAOD::AuxContainerBase>();
    selectedJets->setStore(selectedJetsAux.get());
    
    // loop over jets
    if (m_eventCounter < nDebugEvents) Info("execute()", "Looping over truth jets...");
    for (int i = 0; i < (int)jets->size(); ++i) {
        const xAOD::Jet* jet = jets->at(i);
        
        // set all decorations to null
        //jet->auxdecor<char>("highPt") = 0;
        //jet->auxdecor<char>("veto") = 0;
        
        // std::cout << "attempting to get constituents ..." << std::endl;
        // attempt to get constituents of jet (currently not working)
        xAOD::JetConstituentVector jcv = jet->getConstituents();
        // std::vector<xAOD::JetConstituent> jc = jcv.asSTLVector();
        int njcv = jcv.size();
        // int njc = jc.size();
        int num = jet->numConstituents();
        xAOD::JetConstituentVector::iterator it = jcv.begin();
        xAOD::JetConstituentVector::iterator itE = jcv.end();
        int n = 0;
        for( ; it != itE; it++) n++;
        // int ncl = jet->constituentLinks().size();
        
        // debug printouts
        if (m_eventCounter < nDebugEvents) {
            Info("execute()", "\t Jet px = %f GeV", jet->px()/m_GeV);
            Info("execute()", "\t Jet py = %f GeV", jet->py()/m_GeV);
            Info("execute()", "\t Jet pT = %f GeV", jet->pt()/m_GeV);
            Info("execute()", "\t Jet energy = %f GeV", jet->e()/m_GeV);
            Info("execute()", "\t Jet phi = %f", jet->phi());
            Info("execute()", "\t Jet eta = %f", jet->eta());
            Info("execute()", "\t Jet number of constituents (size) = %i", njcv);
            // Info("execute()", "\t Jet number of constituents (STL) = %i", njc);
            Info("execute()", "\t Jet number of constituents (direct) = %i", num);
            Info("execute()", "\t Jet number of constituents (n) = %i", n);
            // Info("execute()", "\t Jet number of constituents (ncl) = %i", ncl);
            Info("execute()", " ");
        }
        
        // jet preselections
        // obtain selected jets
        if (jet->pt() > 30.*m_GeV && std::abs(jet->eta()) < 4.5) {
            xAOD::Jet * selectedJet = new xAOD::Jet;
            selectedJets->push_back(selectedJet);
            *selectedJet = *jet;
        }
    } // end loop over jets
    
    if (m_eventCounter < nDebugEvents) Info("execute()", "\t\t Number of truth jets = %i", (int)jets->size());
    
    // ********** OBJECT PRESELECTION: MET ********** //
    
    const xAOD::MissingET* missET = NULL;
    const xAOD::MissingET* missETInt = NULL;
    const xAOD::MissingET* missETIntMuons = NULL;

    // truth MET info
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EtMissStoreGateObjects
    
    // make copy of MET container for use in MET Significance calculation
    auto newMet = std::make_unique<xAOD::MissingETContainer>();
    auto newMetAux  = std::make_unique<xAOD::AuxContainerBase>();
    newMet->setStore(newMetAux.get());
    
    for (unsigned int i = 0; i < met->size(); ++i) {
        // NonInt = all non interacting particles (neutrinos and SUSY)
        if (met->at(i)->name() == "NonInt") {
            missET = met->at(i);
            if (m_eventCounter < nDebugEvents) {
                // Info("execute()", "MET name = %s", met->at(i)->name().c_str());
                Info("execute()", "\t NonInt MET px = %f GeV", missET->mpx()/m_GeV);
                Info("execute()", "\t NonInt MET py = %f GeV", missET->mpy()/m_GeV);
                Info("execute()", "\t NonInt MET pT = %f GeV", missET->met()/m_GeV);
                Info("execute()", "\t NonInt MET sumET = %f GeV", missET->sumet()/m_GeV);
                Info("execute()", "\t NonInt MET phi = %f", missET->phi());
            }
        }
        // Int = all interacting particles till abs(eta) <= 5 (muons excluded)
        if (met->at(i)->name() == "Int") {
            missETInt = met->at(i);
            if (m_eventCounter < nDebugEvents) {
                // Info("execute()", "MET name = %s", met->at(i)->name().c_str());
                Info("execute()", "\t Int MET px = %f GeV", missETInt->mpx()/m_GeV);
                Info("execute()", "\t Int MET py = %f GeV", missETInt->mpy()/m_GeV);
                Info("execute()", "\t Int MET pT = %f GeV", missETInt->met()/m_GeV);
                Info("execute()", "\t Int MET sumET = %f GeV", missETInt->sumet()/m_GeV);
                Info("execute()", "\t Int MET phi = %f", missETInt->phi());
            }
        }
        // IntMuons - all final state muons in |eta|<5 and with pt>6 GeV
        if (met->at(i)->name() == "IntMuons") {
            missETIntMuons = met->at(i);
            if (m_eventCounter < nDebugEvents) {
                // Info("execute()", "MET name = %s", met->at(i)->name().c_str());
                Info("execute()", "\t IntMuons MET px = %f GeV", missETIntMuons->mpx()/m_GeV);
                Info("execute()", "\t IntMuons MET py = %f GeV", missETIntMuons->mpy()/m_GeV);
                Info("execute()", "\t IntMuons MET pT = %f GeV", missETIntMuons->met()/m_GeV);
                Info("execute()", "\t IntMuons MET sumET = %f GeV", missETIntMuons->sumet()/m_GeV);
                Info("execute()", "\t IntMuons MET phi = %f", missETInt->phi());
            }
        }
        //copy
        //const xAOD::MissingET* missETOrig = met->at(i);
        //xAOD::MissingET* missETCopy = new xAOD::MissingET;
        //newMet->push_back(missETCopy);
        //*missETCopy = *missETOrig;
    }
    
    // ********** OBJECT PRESELECTION: DM Particles ********** //
    
    auto  selectedDMParticles = std::make_unique<xAOD::TruthParticleContainer>();
    auto selectedDMParticlesAux = std::make_unique<xAOD::AuxContainerBase>();
    selectedDMParticles->setStore(selectedDMParticlesAux.get());
    
    // loop over truth particles (BSM for TRUTH3_DAOD)
    //for (unsigned int i = 0; i < particles->size(); ++i){
    //const xAOD::TruthParticle* dm = particles->at(i);

    if(m_doTruth1){
      bsm_particles = particles;
    }

    for(unsigned int i = 0; i < bsm_particles->size(); ++i){
        const xAOD::TruthParticle* dm = bsm_particles->at(i);
        
        // only look at status 1 dm particles with the appropriate pdgID
        if (dm->status() != 1 ) continue;
        if(dm->absPdgId() != 1000022) continue;  //Separate dm particles from other bsm particles
        
        // debug printouts
        if (m_eventCounter < nDebugEvents) {
            Info("execute()", "\t\t Barcode = %i", dm->barcode());
            Info("execute()", "\t\t  PDG ID = %i", dm->pdgId());
            Info("execute()", "\t\t  Status = %i", dm->status());
            Info("execute()", "\t\t      pT = %f GeV", dm->pt()/m_GeV);
        }
        
        // obtain selected dm particles
        xAOD::TruthParticle* selectedDM = new xAOD::TruthParticle;
        selectedDMParticles->push_back(selectedDM);
        *selectedDM=*dm;
    }
    
    // ********** OVERLAP REMOVAL ********** //
    // Order of removal matters!
    
    // remove jets overlapping with electrons
    int nJetRem = OverlapRemoval(selectedJets.get(), selectedElectrons.get(), 0.2);
    //std::cout << "Number of jets removed = " << nJetRem << std::endl;
    
    // remove electrons overlapping with jets
    int nElRem = OverlapRemoval(selectedElectrons.get(), selectedJets.get(), 0.4);
    //std::cout << "Number of electrons removed = " << nElRem << std::endl;
    
    // remove jets overlapping with muons
    //nJetRem += OverlapRemoval(jets, muons, 0.4);
    
    // remove muons overlapping with jets
    int nMuRem = OverlapRemoval(selectedMuons.get(), selectedJets.get(), 0.4);
    //std::cout << "Number of muons removed = " << nMuRem << std::endl;
     
    
    if (m_eventCounter < nDebugEvents) {
        ANA_MSG_INFO("Number of removed jets = " << nJetRem);
        ANA_MSG_INFO("Number of removed electrons = " << nElRem);
        ANA_MSG_INFO("Number of removed muons = " << nMuRem);
    }
    

    //********** SORT PARTICLE CONTAINERS By PT **********//
    
    sortTruthParticles(lowPtElectrons.get());
    sortTruthParticles(selectedElectrons.get());
    sortTruthParticles(lowPtMuons.get());
    sortTruthParticles(selectedMuons.get());
    sortJets(selectedJets.get());
    sortTruthParticles(selectedDMParticles.get());
    
    
    // ********** SET EVENT ********** //
    
    // electrons
    int nLowPtEl = lowPtElectrons->size();
    int nSelEl = selectedElectrons->size();
    TLorentzVector el1, el2;
    double el1Charge=0 , el2Charge = 0;
    
    if(nSelEl > 0){
        el1.SetPtEtaPhiE(selectedElectrons->at(0)->pt(), selectedElectrons->at(0)->eta(), selectedElectrons->at(0)->phi(), selectedElectrons->at(0)->e());
        el1Charge = selectedElectrons->at(0)->charge();
    }
    if(nSelEl > 1){
        el2.SetPtEtaPhiE(selectedElectrons->at(1)->pt(), selectedElectrons->at(1)->eta(), selectedElectrons->at(1)->phi(), selectedElectrons->at(1)->e());
        el2Charge = selectedElectrons->at(1)->charge();
    }
    
    // muons
    int nLowPtMu = lowPtMuons->size();
    int nSelMu = selectedMuons->size();
    TLorentzVector mu1, mu2;
    double mu1Charge=0, mu2Charge = 0;
    
    if(nSelMu > 0){
        mu1.SetPtEtaPhiE(selectedMuons->at(0)->pt(), selectedMuons->at(0)->eta(), selectedMuons->at(0)->phi(), selectedMuons->at(0)->e());
        mu1Charge = selectedMuons->at(0)->charge();
    }
    if(nSelMu > 1){
        mu2.SetPtEtaPhiE(selectedMuons->at(1)->pt(), selectedMuons->at(1)->eta(), selectedMuons->at(1)->phi(), selectedMuons->at(1)->e());
        mu2Charge = selectedMuons->at(1)->charge();
    }
    
    // jets
    int nSelJet = selectedJets->size();
    int nVetoJet = 0;
    int nHighPtJet = 0;
    int nBJet = 0;
    TLorentzVector jet1, jet2;
    double jet_sumpx = 0.;
    double jet_sumpy = 0.;
    std::vector<float> jets_phi;
    std::vector<float> jets_pt;
    
    if(nSelJet > 0){
        jet1.SetPtEtaPhiE(selectedJets->at(0)->pt(), selectedJets->at(0)->eta(), selectedJets->at(0)->phi(), selectedJets->at(0)->e());
    }
    if(nSelJet > 1){
        jet2.SetPtEtaPhiE(selectedJets->at(1)->pt(), selectedJets->at(1)->eta(), selectedJets->at(1)->phi(), selectedJets->at(1)->e());
    }
    
    // loop over jets
    for (xAOD::JetContainer::const_iterator it = selectedJets->begin(); it != selectedJets->end(); ++it) {
        const xAOD::Jet* jet = *it;
        // obtain selected, veto, and high pT jets
        jet_sumpx += jet->px();
        jet_sumpy += jet->py();
        jets_phi.push_back(jet->phi());
        jets_pt.push_back(jet->pt());
        if (ghostBHadronsAcc(*jet) >= 1) nBJet++;
        // obtain veto jets
        if (jet->pt() > 25.*m_GeV && std::abs(jet->eta()) < 2.5) nVetoJet++;
        // obtain high pT jets
        if (jet->pt() > 30.*m_GeV && std::abs(jet->eta()) < 2.5) nHighPtJet++;
    }
    
    // dm particles
    int nSelDM = selectedDMParticles->size();
    TLorentzVector dm1, dm2;
    
    if(nSelDM > 0){
        dm1.SetPtEtaPhiE(selectedDMParticles->at(0)->pt(), selectedDMParticles->at(0)->eta(), selectedDMParticles->at(0)->phi(), selectedDMParticles->at(0)->e());
    }
    if(nSelDM > 1){
        dm2.SetPtEtaPhiE(selectedDMParticles->at(1)->pt(), selectedDMParticles->at(1)->eta(), selectedDMParticles->at(1)->phi(), selectedDMParticles->at(1)->e());
    }
    
    
    // store+release to allow use elsewhere and avoid memory leaks
    xAOD::TStore* store = wk()->xaodStore();
    ANA_CHECK(store->record(lowPtElectrons.release(), "PreselectedElectrons" ));
    ANA_CHECK(store->record(lowPtElectronsAux.release(), "PreselectedElectronsAux."));
    ANA_CHECK(store->record(lowPtMuons.release(), "PreselectedMuons" ));
    ANA_CHECK(store->record(lowPtMuonsAux.release(), "PreselectedMuonsAux."));
    ANA_CHECK(store->record(selectedJets.release(), "PreselectedJets" ));
    ANA_CHECK(store->record(selectedJetsAux.release(), "PreselectedJetsAux."));
    
    const xAOD::JetContainer* preSelectedJets = NULL;
    ANA_CHECK(m_event->retrieve(preSelectedJets, "PreselectedJets"));
    const xAOD::TruthParticleContainer* preSelectedElectrons = NULL;
    ANA_CHECK(m_event->retrieve(preSelectedElectrons, "PreselectedElectrons"));
    const xAOD::TruthParticleContainer* preSelectedMuons = NULL;
    ANA_CHECK(m_event->retrieve(preSelectedMuons, "PreselectedMuons"));
    
    // FIX ME (0 resolution for truth particles, does METSignificance need to be calculated before overlap removal.)
    // MET SIGNIFICANCE (Following Example of VHbb)
    TLorentzVector MET_Vec_Int, MET_Vec_IntMuons, MET_for_sig;
    
    //Fix ME Should MET be rebuilt for only "preselected" particles.
    MET_Vec_Int.SetPtEtaPhiM(missETInt->met()*1e-3,0.0,missETInt->phi(),0.0);
    MET_Vec_IntMuons.SetPtEtaPhiM(missETIntMuons->met()*1e-3,0.0,missETIntMuons->phi(),0.0);
    MET_for_sig = MET_Vec_Int + MET_Vec_IntMuons;
    
    double sumet = 0;
    
    // Truth MET container does not contain necessary object links,
    // recreate object links by defining fake containers for each reconstructed object,
    // jets, electrons, muons, taus, and photons.
    typedef ElementLink<xAOD::IParticleContainer> iplink_t;
    
    xAOD::MissingET* met_jet = nullptr;
    //MissingETBase::Types::bitmask_t metSource = MissingETBase::Source::jet();
    met::fillMET(met_jet, newMet.get(), "RefJet", MissingETBase::Source::jet());
    //newMet->push_back(met_jet);
    //met_jet->setName("RefJet");
    //met_jet->setSource(MissingETBase::Source::jet());
    static const SG::AuxElement::Decorator< std::vector<iplink_t> > dec_constitObjLinks("ConstitObjectLinks");
    std::vector<iplink_t>& uniqueLinks = dec_constitObjLinks(*met_jet);
    //for (const auto jet: *jets){
    for (const auto jet: *preSelectedJets){
        const xAOD::Jet *ref_jet = static_cast<const xAOD::Jet*>(jet);
        sumet += ref_jet->pt();
        uniqueLinks.push_back( iplink_t(*static_cast<const xAOD::IParticleContainer*>(ref_jet->container()),ref_jet->index()) );
    }
    xAOD::MissingET* met_muon = nullptr;
    met::fillMET(met_muon, newMet.get(), "RefMuon", MissingETBase::Source::muon());
    //newMet->push_back(met_muon);
    //met_muon->setName("RefMuon");
    //met_muon->setSource(MissingETBase::Source::muon());
    for (const auto muon: *preSelectedMuons){
        //if (muon->status() != 1 ) continue;
        //const xAOD::Muon *ref_muon = static_cast<const xAOD::Muon*>(muon);
        const xAOD::TruthParticle *ref_muon = static_cast<const xAOD::TruthParticle*>(muon);
        sumet += ref_muon->pt();
        uniqueLinks.push_back( iplink_t(*static_cast<const xAOD::IParticleContainer*>(ref_muon->container()), ref_muon->index()) );
    }
    xAOD::MissingET* met_electron =  nullptr;
    met::fillMET(met_electron, newMet.get(), "RefEle", MissingETBase::Source::electron());
    //newMet->push_back(met_electron);
    //met_electron->setName("RefEle");
    //met_electron->setSource(MissingETBase::Source::electron());
    for (const auto electron: *preSelectedElectrons){
        //if (electron->status() != 1 ) continue;
        //const xAOD::Electron *ref_electron = static_cast<const xAOD::Electron*>(electron);
        const xAOD::TruthParticle *ref_electron = static_cast<const xAOD::TruthParticle*>(electron);
        sumet += ref_electron->pt();
        uniqueLinks.push_back( iplink_t(*static_cast<const xAOD::IParticleContainer*>(ref_electron->container()), ref_electron->index()) );
    }
    /*
     xAOD::MissingET* met_tau = nullptr;
     met::fillMET(met_tau, newMet.get(), "RefTau", MissingETBase::Source::tau());
     //newMet->push_back(met_tau);
     //met_tau->setName("RefTau");
     //met_tau->setSource(MissingETBase::Source::tau());
     for (const auto tau: *taus){
     const xAOD::TruthParticle *ref_tau = static_cast<const xAOD::TruthParticle*>(tau);
     sumet += ref_tau->pt();
     uniqueLinks.push_back( iplink_t(*static_cast<const xAOD::IParticleContainer*>(ref_tau->container()),ref_tau->index()) );
     }
     
     xAOD::MissingET* met_photon = nullptr;
     met::fillMET(met_photon, newMet.get(), "RefPhoton", MissingETBase::Source::photon());
     //newMet->push_back(met_photons);
     //met_photons->setName("RefPhoton");
     //met_photons->setSource(MissingETBase::Source::photon());
     for (const auto photon: *photons){
     const xAOD::TruthParticle *ref_photon = static_cast<const xAOD::TruthParticle*>(photon);
     sumet += ref_photon->pt();
     uniqueLinks.push_back( iplink_t(*static_cast<const xAOD::IParticleContainer*>(ref_photon->container()), ref_photon->index()) );
     }
     */
    xAOD::MissingET* met_tot_trk = nullptr;
    met::fillMET(met_tot_trk, newMet.get(), "FinalTrk", MissingETBase::Source::total());
    //xAOD::MissingET* met_tot_trk = new xAOD::MissingET();
    //newMet->push_back(met_tot_trk);
    //met_tot_trk->setName("FinalTrk");
    //met_tot_trk->setSource(MissingETBase::Source::total());
    met_tot_trk->add(MET_for_sig.Px()*1000,MET_for_sig.Py()*1000,MET_for_sig.Pt()*1000);
    met_tot_trk->setSumet(sumet);
    xAOD::MissingET* met_tot_soft = nullptr;
    met::fillMET(met_tot_soft, newMet.get(), "PVSoftTrk", MissingETBase::Source::softEvent()+MissingETBase::Source::Track);
    //xAOD::MissingET* met_tot_soft = new xAOD::MissingET();
    //newMet->push_back(met_tot_soft);
    //met_tot_soft->setName("PVSoftTrk");
    //met_tot_soft->setSource(MissingETBase::Source::softEvent()+MissingETBase::Source::Track);
    met_tot_soft->add(1.0,0,1.0);//VF: Setting Soft Term to 1 MeV
    
    ANA_CHECK(m_METSignificanceTool->varianceMET(newMet.get(), 30., "RefJet", "PVSoftTrk", "FinalTrk"))
    
    if (m_eventCounter < nDebugEvents) {
        Info("execute()", "\t\t MET for METSignificance (Pt) = %f GeV", MET_for_sig.Pt());
        Info("execute()", "\t\t Sum ET for MET Significance = %f MeV", sumet);
        Info("execute()", "\t\t METSignificance = %f ", m_METSignificanceTool->GetSignificance());
    }
    
    // set event variables
    m_MonoZEvent->clear(); // FIXME realiable?
    
    m_MonoZEvent->info_isBadMuon                 = false;
    m_MonoZEvent->info_prwHash                   = 0;
    m_MonoZEvent->info_MC                        = m_isMC;
    m_MonoZEvent->info_mcWeight                  = m_mcWeight;
    m_MonoZEvent->info_mcChannelNumber           = m_mcChannelNumber;
    m_MonoZEvent->info_pileupWeight              = 1.;
    m_MonoZEvent->info_vertexWeight              = 1.;
    m_MonoZEvent->info_trigSFWeight              = 1.;
    m_MonoZEvent->info_muIsoSFWeight             = 1.;
    m_MonoZEvent->info_muRecoSFWeight            = 1.;
    m_MonoZEvent->info_muTTVASFWeight            = 1.;
    m_MonoZEvent->info_elIDSFWeight              = 1.;
    m_MonoZEvent->info_elIsoSFWeight             = 1.;
    m_MonoZEvent->info_elRecoSFWeight            = 1.;
    m_MonoZEvent->info_phIDSFWeight              = 1.;
    m_MonoZEvent->info_phIsoSFWeight             = 1.;
    m_MonoZEvent->info_btagSFWeight              = 1.;
    m_MonoZEvent->info_bnottagSFWeight           = 1.;
    m_MonoZEvent->info_jvtSFWeight               = 1.;
    m_MonoZEvent->info_extraWeight               = 1.;
    m_MonoZEvent->info_sysWeightsMap             = m_sysWeightsMap;
    m_MonoZEvent->info_sysMCWeightsMap           = m_sysMCWeightsMap;
    //
    m_MonoZEvent->info_evn                       = m_nEntries;
    m_MonoZEvent->info_evNum                     = m_eventNumber;
    m_MonoZEvent->info_runNum                    = m_runNumber;  // data: run number;  MC: unique identifier for MC condition (campaign.x.y label)
    m_MonoZEvent->info_randRunNum                = 0;   // data: 0; MC: data run number used for MC trigger conditions
    m_MonoZEvent->info_lbNum                     = m_lbNum;
    m_MonoZEvent->info_tStamp                    = m_tStamp;
    m_MonoZEvent->info_bcIdNum                   = m_bcIdNum;
    m_MonoZEvent->info_mu                        = m_avIntPerXing;
    m_MonoZEvent->info_isBadBatman               = 0;
    m_MonoZEvent->vtx_n                          = 0.;
    m_MonoZEvent->lowPtLep_n                     = nLowPtEl + nLowPtMu; // electrons and muons with pT > 7 and |eta| < 2.47 (el) (2.5 (muons))
    m_MonoZEvent->el_n                           = nSelEl; // selected electrons
    m_MonoZEvent->el1_charge                     = (nSelEl > 0) ? el1Charge : 0; // electrons that pass all cuts (pT > 20, |eta| < 2.47)
    m_MonoZEvent->el1_tight                      = (nSelEl > 0) ? 1 : 0;
    m_MonoZEvent->el1_trigMatch                  = (nSelEl > 0) ? 1 : 0;
    m_MonoZEvent->el1                            = (nSelEl > 0) ? el1 : TLorentzVector(); //if no el1, pass empty TLorentzVector
    m_MonoZEvent->el2_charge                     = (nSelEl > 1) ? el2Charge : 0;
    m_MonoZEvent->el2_tight                      = (nSelEl > 1) ? 1 : 0;
    m_MonoZEvent->el2_trigMatch                  = (nSelEl > 1) ? 1 : 0;
    m_MonoZEvent->el2                            = (nSelEl > 1) ? el2 : TLorentzVector();
    // FIXME
    m_MonoZEvent->el3_charge                     = 0;
    m_MonoZEvent->el3_tight                      = 0;
    m_MonoZEvent->el3_trigMatch                  = 0;
    m_MonoZEvent->el3                            = TLorentzVector();
    //
    m_MonoZEvent->mu_n                           = nSelMu;
    m_MonoZEvent->mu1_charge                     = (nSelMu > 0) ? mu1Charge : 0; // electrons that pass all cuts (pT > 20, |eta| < 2.47)
    m_MonoZEvent->mu1_tight                      = (nSelMu > 0) ? 1 : 0;
    m_MonoZEvent->mu1_trigMatch                  = (nSelMu > 0) ? 1 : 0;
    m_MonoZEvent->mu1                            = (nSelMu > 0) ? mu1 : TLorentzVector(); //if no el1, pass empty TLorentzVector
    m_MonoZEvent->mu2_charge                     = (nSelMu > 1) ? mu2Charge : 0;
    m_MonoZEvent->mu2_tight                      = (nSelMu > 1) ? 1 : 0;
    m_MonoZEvent->mu2_trigMatch                  = (nSelMu > 1) ? 1 : 0;
    m_MonoZEvent->mu2                            = (nSelMu > 1) ? mu2 : TLorentzVector();
    // FIXME
    m_MonoZEvent->mu3_charge                     = 0;
    m_MonoZEvent->mu3_tight                      = 0;
    m_MonoZEvent->mu3_trigMatch                  = 0;
    m_MonoZEvent->mu3                            = TLorentzVector();
    //
    // m_MonoZEvent->mu3.SetPtEtaPhiE(0,0,0,0);
    m_MonoZEvent->jet_sumpx                      = jet_sumpx; // with preselected jets
    m_MonoZEvent->jet_sumpy                      = jet_sumpy;
    m_MonoZEvent->jet_n                          = nSelJet; // jets that pass preselection cuts pT > 20, |eta| < 4.5
    m_MonoZEvent->jets_phi                       = jets_phi; // of all preselected jets
    m_MonoZEvent->jets_pt                        = jets_pt;
    m_MonoZEvent->vetoJet_n                      = nVetoJet; // jets that have pT > 25, |eta| < 2.5
    m_MonoZEvent->bJet_n                         = nBJet;
    m_MonoZEvent->highPtJet_n                    = nHighPtJet; // jets that have pT > 30, |eta| < 2.5
    if (nSelJet > 0 ) m_MonoZEvent->jet1         = jet1;
    if (nSelJet > 1 ) m_MonoZEvent->jet2         = jet2;
    // FIXME
    m_MonoZEvent->ph_n                           = 0;
    m_MonoZEvent->phLoose_n                      = 0;
    m_MonoZEvent->ph1                            = TLorentzVector();
    m_MonoZEvent->ph2                            = TLorentzVector();
    m_MonoZEvent->ph1_author                     = 0;
    m_MonoZEvent->ph1_conversionType             = 0;
    
    //For both track and cluster based METTST, use same truth containers:
    m_MonoZEvent->METTST_met                     = missET->met(); // nonInt MET
    m_MonoZEvent->METTST_phi                     = missET->phi();
    m_MonoZEvent->METTST_mpx                     = missET->mpx();
    m_MonoZEvent->METTST_mpy                     = missET->mpy();
    m_MonoZEvent->METTST_sumet                   = sumet; // Int
    
    m_MonoZEvent->METCST_met                     = missET->met(); // nonInt MET
    m_MonoZEvent->METCST_phi                     = missET->phi();
    m_MonoZEvent->METCST_mpx                     = missET->mpx();
    m_MonoZEvent->METCST_mpy                     = missET->mpy();
    m_MonoZEvent->METCST_sumet                   = sumet; // Int
    
    // FIXME !!!
    m_MonoZEvent->METTST_significance            =  (missET->met() > 0.) ? (missET->met() / m_GeV) / TMath::Sqrt(sumet / m_GeV) : 0;      //Use Event Based Approximation until Truth MET Significance is debugged
    //m_MonoZEvent->METTST_significance            = m_METSignificanceTool->GetSignificance();
    
    m_MonoZEvent->METSignifResoEleVarL           = m_METSignificanceTool->GetTermVarL(met::ResoEle) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoEleVarT           = m_METSignificanceTool->GetTermVarT(met::ResoEle) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoMuoVarL           = m_METSignificanceTool->GetTermVarL(met::ResoMuo) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoMuoVarT           = m_METSignificanceTool->GetTermVarT(met::ResoMuo) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoJetVarL           = m_METSignificanceTool->GetTermVarL(met::ResoJet) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoJetVarT           = m_METSignificanceTool->GetTermVarT(met::ResoJet) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoPhoVarL           = 999.;
    m_MonoZEvent->METSignifResoPhoVarT           = 999.;
    m_MonoZEvent->METSignifResoSoftVarL          = m_METSignificanceTool->GetTermVarL(met::ResoSoft) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoSoftVarT          = m_METSignificanceTool->GetTermVarT(met::ResoSoft) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoNoneVarL          = m_METSignificanceTool->GetTermVarL(met::ResoNone) * m_GeV * m_GeV;
    m_MonoZEvent->METSignifResoNoneVarT          = m_METSignificanceTool->GetTermVarT(met::ResoNone) * m_GeV * m_GeV;
    
   
    
    m_MonoZEvent->METRefEle_met                  = ((*newMet)["RefEle"])->met();
    m_MonoZEvent->METRefMuon_met                 = ((*newMet)["RefMuon"])->met();
    m_MonoZEvent->METRefJet_met                  = ((*newMet)["RefJet"])->met();
    m_MonoZEvent->METRefPhoton_met               = 0.;
    m_MonoZEvent->METSoftClus_met                = 0.;
    m_MonoZEvent->METPVSoftTrack_met             = ((*newMet)["PVSoftTrk"])->met();
    m_MonoZEvent->METPVSoftTrack_phi             = ((*newMet)["PVSoftTrk"])->phi();
    
    // loose MET
    // FIXME !!!
    m_MonoZEvent->METTST_met_loose            = missET->met(); // NonInt MET
    m_MonoZEvent->METTST_phi_loose            = missET->phi();
    m_MonoZEvent->METTST_mpx_loose            = missET->mpx();
    m_MonoZEvent->METTST_mpy_loose            = missET->mpy();
    m_MonoZEvent->METTST_sumet_loose          = sumet;
    //m_MonoZEvent->METTST_sumet_loose          = missET->sumet();
    m_MonoZEvent->METTST_significance_loose   = 999.;
    m_MonoZEvent->METRefEle_met_loose         = 0.;
    m_MonoZEvent->METRefMuon_met_loose        = 0.;
    m_MonoZEvent->METRefJet_met_loose         = 0.;
    m_MonoZEvent->METRefPhoton_met_loose      = 0.;
    m_MonoZEvent->METSoftClus_met_loose       = 0.;
    m_MonoZEvent->METPVSoftTrack_met_loose    = 0.;
    m_MonoZEvent->METPVSoftTrack_phi_loose    = 0.;
    
    // Truth Variables (use same definition as MonoZAlgo)
    m_MonoZEvent->truth_el_n                  = nLowPtEl;
    m_MonoZEvent->truth_mu_n                  = nLowPtMu;
    m_MonoZEvent->truth_dm_n                  = nSelDM;
    if(nLowPtEl > 0) m_MonoZEvent->truth_el1  = preSelectedElectrons->at(0)->p4();
    if(nLowPtEl > 1) m_MonoZEvent->truth_el2  = preSelectedElectrons->at(1)->p4();
    if(nLowPtMu > 0) m_MonoZEvent->truth_mu1  = preSelectedMuons->at(0)->p4();
    if(nLowPtMu > 1) m_MonoZEvent->truth_mu2  = preSelectedMuons->at(1)->p4();
    if(nSelDM > 0 ) m_MonoZEvent->truth_dm1   = dm1;
    if(nSelDM > 1 ) m_MonoZEvent->truth_dm2   = dm2;
    
    
    // compute quantities
    m_MonoZEvent->compute();
    // std::cout << "m_weight = " << m_weight << std::endl;
    
    // print event
    if (m_eventCounter < nDebugEvents) m_MonoZEvent->print();
    
    // event selection and histogram production
    
    TString sysTag = ""; // nominal
    m_MonoZSelections->apply(m_MonoZEvent, sysTag, m_MonoZHistosMap);
    
    // event number
    m_eventCounter++;
    
    //overlap checks
    // electron event
    if ((nSelEl) >= 2) {
        m_nTotElRem_e += nElRem;
        m_nTotMuRem_e += nMuRem;
        m_nTotJetRem_e += nJetRem;
        m_nTotElSel_e += nSelEl;
        m_nTotMuSel_e += nSelMu;
        m_nTotJetSel_e += nSelJet;
    }
    // muon event
    if ((nSelMu) >= 2) {
        m_nTotElRem_m += nElRem;
        m_nTotMuRem_m += nMuRem;
        m_nTotJetRem_m += nJetRem;
        m_nTotElSel_m += nSelEl;
        m_nTotMuSel_m += nSelMu;
        m_nTotJetSel_m += nSelJet;
    }

    return EL::StatusCode::SUCCESS;
}

// inherited from MonoZAlgo
// EL::StatusCode MonoZTruthAlgo :: postExecute () {}


EL::StatusCode MonoZTruthAlgo :: finalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.
    ANA_MSG_INFO("In MonoZTruthAlgo::finalize()");
    
    // print sumOfWeightsInfo for this job
    ANA_MSG_INFO("MonoZSumOfWeights for this job");
    m_MonoZSumOfWeights->print("all");
    
    // print cutflow histo yields for cutflow challenge
    if (m_printCutflow) {
        this->printCutflowYields();
        this->printFinalYields();
    }
    
    // delete MonoZClasses instantiated in initialize()
    if (m_MonoZEvent) {delete m_MonoZEvent; m_MonoZEvent = 0;}
    if (m_MonoZTools) {delete m_MonoZTools; m_MonoZTools = 0;}
    if (m_MonoZSelections) {delete m_MonoZSelections; m_MonoZSelections = 0;}
    for (auto p : m_MonoZHistosMap) delete p.second;
    m_MonoZHistosMap.clear();
    
    std::cout << "Electron events:" << std::endl;
    std::cout << "  Number of electrons selected = " << m_nTotElSel_e + m_nTotElRem_e << std::endl;
    std::cout << "  Number of electrons removed = " << m_nTotElRem_e << std::endl;
    std::cout << "    Fraction of electrons removed = " << ((m_nTotElSel_e > 0) ? m_nTotElRem_e/(m_nTotElRem_e+m_nTotElSel_e) : 0.) << std::endl;
    std::cout << "  Number of muons selected = " << m_nTotMuSel_e + m_nTotMuRem_e << std::endl;
    std::cout << "  Number of muons removed = " << m_nTotMuRem_e << std::endl;
    std::cout << "    Fraction of muons removed = " << ((m_nTotMuSel_e > 0) ? m_nTotMuRem_e/(m_nTotMuRem_e + m_nTotMuSel_e) : 0.) << std::endl;
    std::cout << "  Number of jets selected = " << m_nTotJetSel_e + m_nTotJetRem_e<< std::endl;
    std::cout << "  Number of jets removed = " << m_nTotJetRem_e << std::endl;
    std::cout << "    Fraction of jets removed = " << m_nTotJetRem_e/(m_nTotJetRem_e + m_nTotJetSel_e) << std::endl;
    
    std::cout << "Muon events:" << std::endl;
    std::cout << "  Number of electrons selected = " << m_nTotElSel_m + m_nTotElRem_m << std::endl;
    std::cout << "  Number of electrons removed = " << m_nTotElRem_m << std::endl;
    std::cout << "    Fraction of electrons removed = " << ((m_nTotElSel_m > 0) ? m_nTotElRem_m/(m_nTotElRem_m + m_nTotElSel_m) : 0.) << std::endl;
    std::cout << "  Number of muons selected = " << m_nTotMuSel_m + m_nTotMuRem_m << std::endl;
    std::cout << "  Number of muons removed = " << m_nTotMuRem_m << std::endl;
    std::cout << "    Fraction of muons removed = " << ((m_nTotMuSel_m > 0) ? m_nTotMuRem_m/(m_nTotMuRem_m+ m_nTotMuSel_m) : 0.) << std::endl;
    std::cout << "  Number of jets selected = " << m_nTotJetSel_m + m_nTotJetRem_m << std::endl;
    std::cout << "  Number of jets removed = " << m_nTotJetRem_m << std::endl;
    std::cout << "    Fraction of jets removed = " << m_nTotJetRem_m/(m_nTotJetRem_m + m_nTotJetSel_m) << std::endl;
    
    return EL::StatusCode::SUCCESS;
}


EL::StatusCode MonoZTruthAlgo :: histFinalize ()
{
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.
    ANA_MSG_INFO("In MonoZTruthAlgo::histFinalize()");
    
    // delete MonoZClasses instantiated in histInitialize()
    if (m_MonoZSumOfWeights) {delete m_MonoZSumOfWeights; m_MonoZSumOfWeights = 0;}
    
    return EL::StatusCode::SUCCESS;
}

bool MonoZTruthAlgo :: isSysMCWeight(TString sysType, TString weightName) {
    // sysType: "intraPDF", "interPDF", "QCDScale", "MGWeights"
    int dsid = m_mcChannelNumber;
    // std::cout << "in MonoZTruthAlgo::isSysMCWeight(" << sysType << ", " << weightName << ")" << std::endl;
    
    // treatment is DSID-specific
    if (dsid > 800000) {
        // treat specific (dummy) DSID with Truth info only
        if (sysType == "MGWeights") {
            if (weightName.Contains("TANB") || weightName.Contains("SINP")) return true;
        }
        return false;
    } else {
        // use MonoZAlgo::isSysMCWeight() for other DSID's
        return MonoZAlgo::isSysMCWeight(sysType, weightName);
    }
}

// remove particles in container a overlapping with particles in container b
// returns number of particles removed
int MonoZTruthAlgo :: OverlapRemoval(DataVector<xAOD::IParticle> * a, DataVector<xAOD::IParticle> * b, double deltaRCut ){
//int MonoZTruthAlgo :: OverlapRemoval(xAOD::IParticleContainer *a, xAOD::IParticleContainer *b, double deltaRCut ){
    int nRem = 0;
    DataVector<xAOD::IParticle>::iterator it_a = a->begin();
    while( it_a != a->end()){
        //for (DataVector<xAOD::IParticle>::const_iterator it_a = a->begin(); it_a != a->end(); ++it_a) {
        //if (selectAcc(*(*it_a))) {
        bool isOverlap = false;
        for (DataVector<xAOD::IParticle>::const_iterator it_b = b->begin(); it_b != b->end(); ++it_b) {
            //if (selectAcc(*(*it_b))) {
            //if(!overlapAcc(*(*it_b))) {
            double dY = (*it_a)->rapidity() - (*it_b)->rapidity();
            double dPhi = TVector2::Phi_mpi_pi((*it_b)->phi() - (*it_a)->phi());
            double deltaR = TMath::Sqrt(dY*dY + dPhi*dPhi);
            if (deltaR < deltaRCut) {
                isOverlap = true;
                //(*it_a)->auxdecor<char>("overlaps") = 1;
                break;
            }
        }
        if(isOverlap){
            a->erase(it_a);
            nRem++;
        } else {
            ++it_a;
        }
    }
    return nRem;
}

void MonoZTruthAlgo :: doTruth1 (bool flag){
  m_doTruth1 = flag;
}

