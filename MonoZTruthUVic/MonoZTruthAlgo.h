#ifndef MonoZTruthUVic_MonoZTruthAlgo_H
#define MonoZTruthUVic_MonoZTruthAlgo_H

#include <MonoZUVic/MonoZAlgo.h>
#include <TString.h>
//#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
//#include <DataVector.h>

class MonoZTruthAlgo : public MonoZAlgo
{
    // put your configuration variables here as public variables.
    // that way they can be set directly from CINT and python.
public:
    // this is a standard constructor
    MonoZTruthAlgo ();
    // float cutValue;
    
    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
public:
    // these are overloaded
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();
    // these are inherited from MonoZAlgo
    // virtual EL::StatusCode setupJob (EL::Job& job);
    // virtual EL::StatusCode histInitialize ();
    // virtual EL::StatusCode changeInput (bool firstFile);
    // virtual EL::StatusCode postExecute ();
    void doTruth1(bool flag = false);

    
private:
    // overlap removal checks
    // electron events
    double m_nTotElRem_e; //!
    double m_nTotMuRem_e; //!
    double m_nTotJetRem_e; //!
    double m_nTotElSel_e; //!
    double m_nTotMuSel_e; //!
    double m_nTotJetSel_e; //!
    // muon events
    double m_nTotElRem_m; //!
    double m_nTotMuRem_m; //!
    double m_nTotJetRem_m; //!
    double m_nTotElSel_m; //!
    double m_nTotMuSel_m; //!
    double m_nTotJetSel_m; //!
    
    // function that overloads the MonoZAlgo version
    bool isSysMCWeight(TString sysType, TString weightName);
    // remove particles in container a overlapping particles in container b
    int OverlapRemoval(DataVector<xAOD::IParticle> * container_a, DataVector<xAOD::IParticle> * container_b, double deltaRCut );
    //int OverlapRemoval(xAOD::IParticleContainer *a, xAOD::IParticleContainer *b, double deltaRCut );
    // this is needed to distribute the algorithm to the workers
    ClassDef(MonoZTruthAlgo, 1);

 private:
    // properties of the algorithm, initialized in the constructor (do not use //!)
    // flags
    bool m_doTruth1;

};

#endif
