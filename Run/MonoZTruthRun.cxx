void MonoZTruthRun (TString submitDir = "")
{
  // Run the MonoZAlgo algorithm locally
  // Your work directory is in $ROOTCOREBIN/..
    
  // The directory $ROOTCOREBIN/../jobs/ must exist
  // for example let your work directory be
  // wDir = MonoZROOTCore
  // then $ROOTCOREBIN is MonoZROOTCore/RootCoreBin
  // then the directory MonoZROOTCore/jobs must exist before using this script
  //
  // submitDir = "" --> the code uses "MonoZROOTCore/jobs/truth_" + timestamp_start
    
  // to run (assuming MonoZMeta)
  // root -l -q '../MonoZMeta/MonoZTruthUVic/Run/MonoZTruthRun.cxx()' 2>&1 | tee out_truthrun.dat
  // root -l -q '../MonoZMeta/MonoZTruthUVic/Run/MonoZTruthRun.cxx("someOutputDirPath")' 2>&1 | tee out_truthrun.dat
  // nohup root -l -q '../MonoZMeta/MonoZTruthUVic/Run/MonoZTruthRun.cxx()' > out_truthrun.dat &
    
  // user settings
  // ****************

  // number of events to run on
  bool doAllEvents = true;
  double nEvents = 1000; // only used if doAllEvents = false
  
  // user master regions list
  // possible values: {"SRMZ1", "WPSRMZ1", "CRMZGJ1", "CRMZZJ1", "CRMZZJ2", "CRMZ3L", "SRLM1", "CRLM1em", "SRHM1", "CRHMGJ1", "CRHMZJ1", "CRHM1em", "OPT", "SYS", "TRU", "TREE"};
  //std::vector<TString> userRegionsList = {"SRMZ1"};
  std::vector<TString> userRegionsList = {"SRMZ1", "TRU"};
  // std::vector<TString> userRegionsList = {"SRMZ1", "SRHM1"}; // use for nominal minitree production

  // list of MCWeightVariations to process
  //std::vector<TString> sysMCWeightTypes; // leave empty to not treat the MCweight variations
  std::vector<TString> sysMCWeightTypes = {"MGWeights"};
  // std::vector<TString> sysMCWeightTypes = {"interPDF", "intraPDF", "QCDScale"};

  bool applyWeightsAtALL = true; // apply the dilepton SF and b-jet inefficiency SF weights at ALL, not at the 2SFL and BJVETO requirements
  bool printCutflow      = true; // enable printing of cutflow (mainly for cutflow challenge)
  bool doTruth1          = false; // flag to run over TRUTH1_DAODS, by default assumes samples are TRUTH3_DAODs
  
  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  //SH::DiskListLocal list1("/hep300/data/cranelli/monoZ/MC16_JIRA_DMSimp_NLO/DAOD");
  //SH::scanDir(sh, list1, "*.root*", "*TRUTH1*");

  SH::DiskListLocal list1("/hep300/data-shared/MonoZ/Rel21/mcuser/signal_2HDMa_MGReweight/DAOD");
  SH::scanDir(sh, list1, "*.root*", "*sp0p35*v501*TRUTH3*");

  //SH::DiskListLocal list1("/hep300/data-shared/MonoZ/Rel21/mcuser/signal_2HDMa_ggF/DAOD");
  //SH::scanDir(sh, list1, "*.root*", "*TRUTH3.pool*");

  //SH::DiskListLocal list1("/hep300/data-shared/MonoZ/Rel21/samples/truth3/signal_2HDMa/");
  //SH::scanDir(sh, list1, "*.root*", "*TRUTH3_EXT0*");
  
  // *****
  // get work directory, above $ROOTCOREBIN
  TString rDir = Form("%s", gSystem->Getenv("ROOTCOREBIN"));  // $ROOTCOREBIN
  TString wDir = Form("%s", gSystem->DirName(rDir));          // work directory, above $ROOTCOREBIN
  std::cout << "wDir = " << wDir << std::endl;
    
  // Set up the job for xAOD access:
  xAOD::Init().ignore();
    
  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");
    
  // further sample handler configuration may go here
    
  // print out the samples we found
  sh.print ();
    
  // set sampleName meta-data for samples
  for (int i = 0; i < (int)sh.size(); ++i) {
    TString sname = sh[i]->name();
    std::cout << "Setting sampleName meta-data for sample " << sname << std::endl;
    sh.get(sname.Data())->setMetaString("sampleName", sname.Data());
  }
    
  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  // job.options()->setDouble (EL::Job::optMaxEvents, 10000); // for testing purposes, limit to run over the first few events only!

  // configure the algorithm
  MonoZTruthAlgo *alg = new MonoZTruthAlgo;
  alg->SetName("MonoZTruthAlgo");
  alg->setMasterRegions(userRegionsList);
  alg->setSysMCWeightTypes(sysMCWeightTypes);
  alg->applyWeightsAtALL(applyWeightsAtALL);
  alg->printCutflow(printCutflow);
  alg->doTruth1(doTruth1);
   
  // add our algorithm to the job
  job.algsAdd (alg);
  if (!doAllEvents) job.options()->setDouble(EL::Job::optMaxEvents, nEvents);
    
  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.
    
  // prepare time stamp string for output directory name
  TDatime now;
  TString timestamp_start = Form("%d_%06d", now.GetDate(), now.GetTime());
  // set output directory
  TString sDir = submitDir;
  if (sDir == "") sDir += "truth_out" + timestamp_start;
    
  // process the job using the driver
  driver.submit (job, sDir.Data());
    
  // time stamp at end of job
  now.Set();
    
  TString timestamp_end = Form("%d_%06d", now.GetDate(), now.GetTime());
    
  std::cout << "started: " << timestamp_start << std::endl;
  std::cout << "ended:   " << timestamp_end << std::endl;
  std::cout << "output directory: " << sDir << std::endl;
    
}
