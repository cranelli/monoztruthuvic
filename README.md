**NOTE:**  please use the same **release** as the MonoZUVic package

# Installing from scratch 
Instructions for Release 21
MonoZTruthUVic inherits from MonoZUVic
```
mkdir MonoZ
cd MonoZ
mkdir build run source
cd source
git clone https://gitlab.cern.ch/MonoZ/MonoZTruthUVic.git
git clone https://gitlab.cern.ch/MonoZ/MonoZUVic.git
cd ../build
asetup 21.2.75, AnalysisBase
cmake ../source
make
source */setup.sh
```

### Next login
```
cd build
asetup --restore
source */setup.sh
```

# Run the MonoZTruthAlgo
- Also read carefully the header in Run/MonoZTruthRun.cxx

```
cd run
root -l -q '../source/MonoZTruthUVic/Run/MonoZTruthRun.cxx()' 2>&1 | tee out.dat
```
By default - and recommended - MonoZTruth runs over **TRUTH3** DAODs.  One can also run over 
TRUTH1 DAODs by setting the doTruth1 flag to true in MonoZTruthRun.cxx.  *( N.B. There is
a known bug for the 2HDM+a samples generated with "19.2.5.33.3,MCProd".  The weight MetaData 
is not properly transfered to the TRUTH1 derivation and the [0] weight does not correspond to 
the nominal.)  



# Scripts
Before and after executing MonoZTruth, a number of support scripts exists for converting EVENT files 
into TRUTH Derivaitons, Extracting Cross Sections, and plotting:  

## Preparing TRUTH Derivations
MonoZTruthUVic runs over **TRUTH1** and **TRUTH3** derivations.  If the user produces their own samples  with the MonoZMCUVic package, 
then TRUTH derivations are automatically generated in the DAOD directory.  
If running over official samples, TRUTH derivations can be produced by running the Reco_tf transform over EVENT files.
- For TRUTH1
```
asetup 20.1.8.1, AtlasDerivation, here
Reco_tf.py --inputEVNTFile evt.pool.root* --outputDAODFile pool.root ----reductionConf TRUTH1
```
- For TRUTH3
```
asetup 21.2.68.0, AthDerivation, here
Reco_tf.py --inputEVNTFile evt.pool.root* --outputDAODFile pool.root --reductionConf TRUTH3
```
To streamline running over multiple samples, derivations can be run on the GRID - if the EVENT files also 
already exist on the GRID.
```
cd run
../source/MonoZTruthUVic/scripts/truth_derivation/create_grid_batch.sh "TRUTH3" /hep300/data-shared/MonoZ/Rel21/samples/evnt/mc16signal_2HDMa_EVNT  
```
The second input to create_grid_batch.sh, specifying a path, is just for finding the samples' names.  (TO DO change script to run over a list of samples, so EVENT files do not ned to 
be  downloaded locally.)
Similar to the scripts on MonoZMCUVic, "create_grid_batch.sh" produces secondary batch and recovery scripts:
```
asetup 21.2.68.0, AthDerivation,here # or 20.1.8.1,AtlasDerivation for TRUTH1 
lsetup rucio
lsetup panda
./batch_TRUTH3.sh >> submission.log 2>&1
 ```
 Once the grid jobs are complete, download them to /hep300/:
 ```
cd /hep300/data-shared/MonoZ/Rel21/samples/truth3/signal_2HDMa
./mc16_recovery_TRUTH3.sh
 ```
 
## Extracting Cross Section Info
  If  samples  are user produced with the MonoZMCUVic package, the 
  cross section information can be extracted from the (untarred) log files.
  This produces a text file containing the cross-section, filter-efficiency, 
  and if applicable reweighting information for each job.
```
../source/MonoZTruthUVic/scripts/xsec_info/extractXsec.py /hep300/data-shared/MonoZ/Rel21/mcuser/signal_2HDMa_MGReweight/Logs
```
The produced text file takes its name from the parent directory of "Logs", ie signal_2HDMa_MGReweight.txt.  
For the 2HDM+a samples, it is necessary to create seperate "bb" induced and "ggF" files. 
Also, when reweighting is used, to avoid having multiple instances of the same mass point, samples should
be seperated by their base "sinp" values.
```
head -n 1 signal_2HDMa_MGReweight.txt > signal_2HDMa_bb_MGReweight_base_sp0p35.txt
cat signal_2HDMa_MGReweight.txt | grep sp0p35 | grep bb >> signal_2HDMa_bb_MGReweight_base_sp0p35.txt
head -n 1 signal_2HDMa_MGReweight.txt > signal_2HDMa_bb_MGReweight_base_sp0p7.txt
cat signal_2HDMa_MGReweight.txt | grep sp0p7 | grep bb >> signal_2HDMa_bb_MGReweight_base_sp0p7.txt
head -n 1 signal_2HDMa_MGReweight.txt > signal_2HDMa_ggF_MGReweight_base_sp0p35.txt
cat signal_2HDMa_MGReweight.txt | grep sp0p35 | grep gg >> signal_2HDMa_ggF_MGReweight_base_sp0p35.txt
head -n 1 signal_2HDMa_MGReweight.txt > signal_2HDMa_ggF_MGReweight_base_sp0p7.txt
cat signal_2HDMa_MGReweight.txt | grep sp0p7 | grep gg >> signal_2HDMa_ggF_MGReweight_base_sp0p7.txt
```

For reweighting, one should similarly divide the DAOD files into separate "base_sp0p35" 
and "base_sp0p7" directories.

For many of the plotting scripts, it is useful to store this metadata in a python, pickle (.p) file. 
The script contains two flags, --rw, which is set for samples containing MG reweighting weights,
and --avg which averages the cross sections and filter efficiencies for samples containing multiple 
jobs.  (assumes all  jobs have the same number of events)
```
lsetup root # Necessary for rename_rw_samples.py
../source/MonoZTruthUVic/scripts/xsec_info/ConvertXsecTxtToPkle.py signal_2HDMa_ggF_MGReweight_base_sp0p7.txt --avg --rw
```
**Renaming** the rename_rw_samples.py script can run over the output of MonoZGather, 
creating new files and renaming the reweighted histograms so they are in the same format as
histograms produced running over regular samples.  Takes as an input a directory, 
may take a couple minutes to run.
```
../source/MonoZTruthUVic/scripts/xsec_info/rename_rw_samples.py plot_truth_2HDMa_MGReweight_base_sp0p35_20191018
```
**Adding ggF + bb** (Fix Me)  In the future this can be handled with MonoZGather, but there are also scripts for 
combining the 2HDM+a ggF and bb samples.  

#./AddXSecFiles.py signal_2HDMa_ggplusbb_MGReweight_base_sp0p7.txt ../../share/signal_2HDMa_ggF_MGReweight_base_sp0p7.p ../../share/signal_2HDMa_bb_MGReweight_base_sp0p7.p
#./ConvertXsecTxtToPkle.py --avg signal_2HDMa_ggplusbb_MGReweight_base_sp0p7.txt
```
../source/MonoZTruthUVic/scripts/xsec_info/weightAndAddHistograms.py --indir truth_signal_2HDMa_MGReweightbase_sp0p7_2019.11.26_Rename --xsec1 ../source/MonoZTruthUVic/share/signal_2HDMa_ggF_MGReweight_base_sp0p7.p --xsec2 ../source/MonoZTruthUVic/share/signal_2HDMa_bb_MGReweight_base_sp0p7.p
../source/MonoZTruthUVic/scripts/xsec_info/ConvertXsecTxtToPkle.py ../source/MonoZTruthUVic/share/signal_2HDMa_ggplusbb_MGReweight_base_sp0p7.txt --avg
```
**Pyami** If these are official samples available on AMI, then one can  extract similar information,  
cross section, filter efficiency, etc. utilizing getMetadata.py:
```
asetup AthAnalysis,21.2.39,here
lsetup pyAMI
voms-proxy-init -voms atlas
getMetadata.py --physicsGroups="PMG,MCGN" --fields="dataset_number,crossSection,genFiltEff,physics_short" --inDS="mc16_13TeV.346199.MGPy8EG_2HDMa_gg_MonoZ_ll_tb1p0_sp0p35_mH350_ma200_mDM10.deriv.DAOD_HIGG2D1.e7514_e5984_a875_r9364_r9315_p3654, ..." --outFile=metadata.txt
```

To access files associated with an individual subcampaign:
```
ami command BrowseSQLQuery -gLiteEntity="campaign" -gLiteQuery="SELECT campaign.prodsysIdentifier, campaign.subcampaign WHERE dataset.identifier='315789'" -processingStep="production" -project="mc16_001"
```

## Plotting
**Acceptance and Preliminary Limits **
```
../source/MonoZTruthUVic/scripts/plotting/AcceptanceTable.py --indir truth_signal_2HDMa_MGReweightbase_sp0p7_2019.11.26 --xsec ../source/MonoZTruthUVic/share/signal_2HDMa_ggF_MGReweight_base_sp0p7.p --rw
```

## 2HDMa Hist Based Reweighting
```
./CalcHistRW.py --xsec ../../share/official_signal_2HDMa_metadata.p --input ../../../../run/signal_2HMDa_truth_2019.11.18/
```
The root files produced can then be used by the MonoZUVic code.
In MonoZRun, set "do2HDMaReweighting" to true and add applicable root files to "THDMaHistoPathMap" 
```
root -l -q '../source/MonoZUVic/Run/MonoZRun.cxx()' 2>&1 | tee out.dat
root -l -b -q '../source/MonoZUVic/scripts/MonoZGather.cxx("input_dir")'
```
After running MonoZGather one can rename the histograms so that the reweighted histograms are in the same format as the nominal histograms
```
../source/MonoZTruthUVic/scripts/rename_rw_hists.py plot_2HDMa_HRW_2019.11.24
```
And perform a closure test between the reweighted and nominal histograms



//---------- (Deprecated Scripts)  ----------//

## Plot cutflow histos
- Read the header in scripts/MonoZCutflowPlot.cxx for the various options

```
cd MonoZROOTCore
cd MonoZUVic
root -l -q -b '$ROOTCOREDIR/scripts/load_packages.C' 'scripts/MonoZCutflowPlot.cxx("out_20150601_233855")' | tee 2>&1 out_cutflow.dat    
```

## Plot general histos
- Read the header in scripts/MonoZGenPlot.cxx for the various options

```
cd MonoZROOTCore
cd MonoZUVic
root -l -q -b '$ROOTCOREDIR/scripts/load_packages.C' 'scripts/MonoZGenPlot.cxx("out_20150601_233855")' | tee 2>&1 out_gen.dat
```

---



# git notes

## git ssh key instructions
This will need to be done once for symmetry (and once for lxplus, and once for your personal computer)
- Go to gitlab.cern.ch/MonoZ
- Go to 'Profile Settings' from the drop-down menu on the left
- Click on the 'SSH Keys' tab
- Under 'Add an SSH key' click on 'generate it'
- Follow the instructions for 'SSH keys', which involves generating a key and copying it to the 'My SSH Keys' section under the 'SSH' tab in the user profile

## checkout a working branch
```
git checkout -b myNewBranch
```

## push working branch to repository
```
git push -u origin myNewBranch
```

## add a file (or files) and commit it (them)
```
git add Root/Myfile.cxx
git commit -m "modifications to Myfile.cxx were made"
```

## push your changes to the repository
```
git push origin myNewBranch
```

## request merge of myNewBranch to master
go to webpage gitlab.cern.ch/MonoZ/MonoZUVic and click on 'request merge' in your myNewBranch branch

## check status of files
```
git status
```

## check status of branches
```
git branch
```

## move between master and any existing branch (note, files must be added and commited before this is done)
```
git checkout master
git branch
git checkout myNewBranch
git branch
```

---


//---------- (deprecated) svn notes ----------//

// first install on svn
svn mkdir svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Victoria/PhysicsAnalysis/ROOTCore/MonoZTruthUVic -m "Creating MonoZTruthUVic directory"
svn mkdir svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Victoria/PhysicsAnalysis/ROOTCore/MonoZTruthUVic/trunk -m "Creating trunk directory"
svn mkdir svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Victoria/PhysicsAnalysis/ROOTCore/MonoZTruthUVic/tags -m "Creating tags directory"
cd MonoZTruthUVic
// make sure the directory structure is clean!
svn import . svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Victoria/PhysicsAnalysis/ROOTCore/MonoZTruthUVic/trunk -m "initial import of MonoZTruthUVic"
// create a tag
svn cp svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Victoria/PhysicsAnalysis/ROOTCore/MonoZTruthUVic/trunk svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Victoria/PhysicsAnalysis/ROOTCore/MonoZTruthUVic/tags/MonoZTruthUVic-00-00-00 -m "first working version"

// add a file under version control
svn add file.cxx

// remove a file from version control
svn delete file.cxx

// before committing changes, look at status
svn status
svn status -u

// get meening of comments with
svn status -h

// after some changes, commit new versio
cd MonoZTruthUVic
svn ci -m "put comment here"



