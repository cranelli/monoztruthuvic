void printAllCanvases(TString fn = "out.pdf") {
  // print all canvases to file
  TCanvas *c = 0;
  int nc = gROOT->GetListOfCanvases()->GetSize();
  // std::cout << "number of canvas = " << nc << std::endl;
  int ic = 0;
  TIter next(gROOT->GetListOfCanvases());
  while ((c = (TCanvas *)next())) {
    if (nc == 1) {
      c->Print(fn, "pdf"); 
    } else {
      if (ic == 0) {
	c->Print(fn+"(", "pdf"); 
      } else if (ic == nc-1) {
	c->Print(fn+")", "pdf");
      } else {
	c->Print(fn, "pdf");
      }
    }
    ++ic;
  }
}

TString extractMedMass(TString dmSampleName) {
  // extract mass from histogram
  TString mMed = dmSampleName;
  TString cut = "_MM";
  // obtain the location of "_MM"
  int k = mMed.Index(cut);
  // remove all characters before
  // e.g. this removes dmV_DMX_MM
  mMed.Remove(0, k+cut.Length());
  return mMed;
}

void MonoZPSSysPrint(TString hnTag0 = "SRMZ1", TString hnTag1 = "METTST", TString hnTag2 = "Final", bool relativePath = true)
{
  // --- THIS SCRIPT: 
  // runs on MonoZTruthUVic output of DM signal samples with modified scale factors = 0.5, 1.0 (nominal), and 2.0
  // for each sample it obtains the final yields and calculates the systematic error on the signal acceptance
  // it then plots the error as a function of Mmed and fits a function for interpolation between mediator masses
  // outputs a text file with a complete list of DM signal samples and errors obtained from the fit

  // --- TO RUN:
  // root -l -b -q scripts/MonoZPSSysPrint.cxx

  // --- MODIFY UP TO **********

  // set which stats to use
  // these can both be set to true
  //bool oneMillevents = true;
  //bool oneHunThousevents = true;
  
  // directories with MonoZTruthUVic output
  // original TRUTH1 DAODs with 1M events are located here: /hep300/data/kaylamc/DMDAODFiles/dmA/psUncertainties
  std::vector<TString> dmDirs;
  //dmDirs.push_back("/hep300/data/kaylamc/MonoZ/git/AnalysisBase2.4.20/jobs/truth_20170301_013621"); // mDM = 25, mMed = 200
  //dmDirs.push_back("/hep300/data/kaylamc/MonoZ/git/AnalysisBase2.4.20/jobs/truth_20170316_070106"); // mDM = 10, mMed = 100
  dmDirs.push_back("/hep300/data/kaylamc/MonoZ/git/AnalysisBase2.4.20/jobs/truth_20170317_105513"); // mDM = 5, mMed = 30

  // set which correlation to use for rho(A,B)
  // only choose one
  TString rho = "0.5"; // default
  //TString rho = "0";
  //TString rho = "1";
  
  // list of samples file names used above
  // these are the files used to obtain the fit function
  std::map<TString, TString> truthfnMap;
  truthfnMap["dmA_5_30"] = "dmA_5_30";

  std::map<TString, TString> varfnMap;
  varfnMap["Nominal"]  = "dmA_5_30_Nominal";
  varfnMap["Var1Down"] = "dmA_5_30_Var1Down";
  varfnMap["Var1Up"]   = "dmA_5_30_Var1Up";
  varfnMap["Var2Down"] = "dmA_5_30_Var2Down";
  varfnMap["Var2Up"]   = "dmA_5_30_Var2Up";
  varfnMap["Var3aDown"] = "dmA_5_30_Var3aDown";
  varfnMap["Var3aUp"]   = "dmA_5_30_Var3aUp";
  varfnMap["Var3bDown"] = "dmA_5_30_Var3bDown";
  varfnMap["Var3bUp"]   = "dmA_5_30_Var3bUp";
  varfnMap["Var3cDown"] = "dmA_5_30_Var3cDown";
  varfnMap["Var3cUp"]   = "dmA_5_30_Var3cUp";

  // complete list of axial-vector DM models
  // PS errors are obtained for these samples
  std::map<TString, TString> dmARNMap;
  dmARNMap["307190"] = "dmA_DM5_MM1";
  dmARNMap["307191"] = "dmA_DM1_MM10";
  dmARNMap["307192"] = "dmA_DM10_MM10";
  dmARNMap["307193"] = "dmA_DM50_MM10";
  dmARNMap["307194"] = "dmA_DM5_MM30";
  dmARNMap["307195"] = "dmA_DM1_MM50";
  dmARNMap["307196"] = "dmA_DM30_MM50";
  dmARNMap["307197"] = "dmA_DM25_MM80";
  dmARNMap["307198"] = "dmA_DM1_MM100";
  dmARNMap["307199"] = "dmA_DM55_MM100";
  dmARNMap["307200"] = "dmA_DM100_MM100";
  dmARNMap["307201"] = "dmA_DM50_MM130";
  dmARNMap["307202"] = "dmA_DM1_MM150";
  dmARNMap["307203"] = "dmA_DM25_MM150";
  dmARNMap["307204"] = "dmA_DM80_MM150";
  dmARNMap["307205"] = "dmA_DM75_MM180";
  dmARNMap["307206"] = "dmA_DM1_MM200";
  dmARNMap["307207"] = "dmA_DM50_MM200";
  dmARNMap["307208"] = "dmA_DM105_MM200";
  dmARNMap["307209"] = "dmA_DM150_MM200";
  dmARNMap["307210"] = "dmA_DM100_MM230";
  dmARNMap["307211"] = "dmA_DM1_MM250";
  dmARNMap["307212"] = "dmA_DM75_MM250";
  dmARNMap["307213"] = "dmA_DM130_MM250";
  dmARNMap["307214"] = "dmA_DM125_MM280";
  dmARNMap["307215"] = "dmA_DM1_MM300";
  dmARNMap["307216"] = "dmA_DM100_MM300";
  dmARNMap["307217"] = "dmA_DM155_MM300";
  dmARNMap["307218"] = "dmA_DM200_MM300";
  dmARNMap["307219"] = "dmA_DM150_MM330";
  dmARNMap["307220"] = "dmA_DM1_MM350";
  dmARNMap["307221"] = "dmA_DM125_MM350";
  dmARNMap["307222"] = "dmA_DM180_MM350";
  dmARNMap["307223"] = "dmA_DM175_MM380";
  dmARNMap["307224"] = "dmA_DM1_MM400";
  dmARNMap["307225"] = "dmA_DM100_MM400";
  dmARNMap["307226"] = "dmA_DM150_MM400";
  dmARNMap["307227"] = "dmA_DM205_MM400";
  dmARNMap["307228"] = "dmA_DM250_MM400";
  dmARNMap["307229"] = "dmA_DM200_MM430";
  dmARNMap["307230"] = "dmA_DM1_MM450";
  dmARNMap["307231"] = "dmA_DM175_MM450";
  dmARNMap["307232"] = "dmA_DM230_MM450";
  dmARNMap["307233"] = "dmA_DM225_MM480";
  dmARNMap["307234"] = "dmA_DM1_MM500";
  dmARNMap["307235"] = "dmA_DM100_MM500";
  dmARNMap["307236"] = "dmA_DM200_MM500";
  dmARNMap["307237"] = "dmA_DM255_MM500";
  dmARNMap["307238"] = "dmA_DM250_MM530";
  dmARNMap["307239"] = "dmA_DM1_MM550";
  dmARNMap["307240"] = "dmA_DM225_MM550";
  dmARNMap["307241"] = "dmA_DM280_MM550";
  dmARNMap["307242"] = "dmA_DM1_MM600";
  dmARNMap["307243"] = "dmA_DM100_MM600";
  dmARNMap["307244"] = "dmA_DM250_MM600";
  dmARNMap["307245"] = "dmA_DM1_MM650";
  dmARNMap["307246"] = "dmA_DM1_MM700";
  dmARNMap["307247"] = "dmA_DM100_MM700";
  dmARNMap["307248"] = "dmA_DM1_MM750";
  dmARNMap["307249"] = "dmA_DM1_MM800";

  // complete list of vector DM models
  // PS errors are obtained for these samples
  std::map<TString, TString> dmVRNMap;
  dmVRNMap["303511"] = "dmV_DM1_MM10";
  dmVRNMap["303512"] = "dmV_DM1_MM100";
  dmVRNMap["303513"] = "dmV_DM1_MM300";
  dmVRNMap["303514"] = "dmV_DM1_MM2000";          
  dmVRNMap["303515"] = "dmV_DM10_MM10";
  dmVRNMap["303516"] = "dmV_DM10_MM100";
  dmVRNMap["303517"] = "dmV_DM10_MM10000";
  dmVRNMap["303518"] = "dmV_DM50_MM10";
  dmVRNMap["303519"] = "dmV_DM50_MM95";
  dmVRNMap["303520"] = "dmV_DM50_MM300";
  dmVRNMap["303521"] = "dmV_DM150_MM10";
  dmVRNMap["303522"] = "dmV_DM150_MM295";
  dmVRNMap["303523"] = "dmV_DM150_MM1000";
  dmVRNMap["303524"] = "dmV_DM500_MM10";
  dmVRNMap["303525"] = "dmV_DM500_MM995";
  dmVRNMap["303526"] = "dmV_DM500_MM2000";
  dmVRNMap["303527"] = "dmV_DM500_MM10000";
  dmVRNMap["303528"] = "dmV_DM1000_MM10";
  dmVRNMap["303529"] = "dmV_DM1000_MM1000";
  dmVRNMap["303530"] = "dmV_DM1000_MM1995";
  dmVRNMap["303551"] = "dmV_DM50_MM95";
  dmVRNMap["303552"] = "dmV_DM500_MM995";
  dmVRNMap["305710"] = "dmV_DM1_MM500";
  dmVRNMap["305711"] = "dmV_DM1_MM700"; 
  dmVRNMap["305712"] = "dmV_DM10_MM300";     
  dmVRNMap["305713"] = "dmV_DM10_MM500";
  dmVRNMap["305714"] = "dmV_DM30_MM10";       
  dmVRNMap["305715"] = "dmV_DM30_MM100";     
  dmVRNMap["305716"] = "dmV_DM30_MM300";     
  dmVRNMap["305717"] = "dmV_DM30_MM500";     
  dmVRNMap["305718"] = "dmV_DM30_MM700";  
  dmVRNMap["305719"] = "dmV_DM50_MM500";     
  dmVRNMap["305720"] = "dmV_DM50_MM700";
  dmVRNMap["305721"] = "dmV_DM100_MM100";    
  dmVRNMap["305722"] = "dmV_DM100_MM300";    
  dmVRNMap["305723"] = "dmV_DM100_MM500";    
  dmVRNMap["305724"] = "dmV_DM100_MM700";
  dmVRNMap["306080"] = "dmV_DM1_MM50";
  dmVRNMap["306081"] = "dmV_DM30_MM50";
  dmVRNMap["306082"] = "dmV_DM55_MM100";
  dmVRNMap["306083"] = "dmV_DM25_MM150";
  dmVRNMap["306084"] = "dmV_DM80_MM150";
  dmVRNMap["306085"] = "dmV_DM1_MM200";
  dmVRNMap["306086"] = "dmV_DM50_MM200";
  dmVRNMap["306087"] = "dmV_DM105_MM200";
  dmVRNMap["306088"] = "dmV_DM150_MM200";
  dmVRNMap["306089"] = "dmV_DM130_MM250";
  dmVRNMap["306090"] = "dmV_DM155_MM300";
  dmVRNMap["306091"] = "dmV_DM200_MM300";
  dmVRNMap["306092"] = "dmV_DM180_MM350";
  dmVRNMap["306093"] = "dmV_DM1_MM400";
  dmVRNMap["306094"] = "dmV_DM50_MM400";
  dmVRNMap["306095"] = "dmV_DM100_MM400";
  dmVRNMap["306096"] = "dmV_DM150_MM400";
  dmVRNMap["306097"] = "dmV_DM205_MM400";
  dmVRNMap["306098"] = "dmV_DM250_MM400";
  dmVRNMap["306099"] = "dmV_DM230_MM450";
  dmVRNMap["306100"] = "dmV_DM200_MM500";
  dmVRNMap["306101"] = "dmV_DM255_MM500";
  dmVRNMap["306102"] = "dmV_DM280_MM550";
  dmVRNMap["306103"] = "dmV_DM1_MM600";
  dmVRNMap["306104"] = "dmV_DM50_MM600";
  dmVRNMap["306105"] = "dmV_DM100_MM600";
  dmVRNMap["306106"] = "dmV_DM200_MM600";
  dmVRNMap["306107"] = "dmV_DM200_MM700";
  dmVRNMap["306108"] = "dmV_DM300_MM700";
  dmVRNMap["306109"] = "dmV_DM1_MM800";
  dmVRNMap["306110"] = "dmV_DM100_MM800";
  dmVRNMap["306111"] = "dmV_DM200_MM800";
  dmVRNMap["306112"] = "dmV_DM300_MM800";
  
  // **********

  TString rDir = Form("%s", gSystem->Getenv("ROOTCOREBIN"));  // $ROOTCOREBIN
  TString wDir = Form("%s", gSystem->DirName(rDir));          // work directory, above $ROOTCOREBIN
  TString oDir = wDir + "/MonoZUVic/";                        // output directory for text file

  // loop over truth directories 
  for (std::vector<TString>::iterator iDir = dmDirs.begin(); iDir != dmDirs.end(); ++iDir) {

    TString dmDir = *iDir;

    // loop over each truth DM sample 
    for (std::map<TString, TString>::iterator iSamp = truthfnMap.begin(); iSamp != truthfnMap.end(); ++iSamp) {

      // store largest variaton for sample
      double nom_ee = 0;
      double nom_mm = 0;
      double diff_ee = 0;
      double diff_mm = 0;
      
      // loop over variations
      for (std::map<TString, TString>::iterator iVar = varfnMap.begin(); iVar != varfnMap.end(); ++iVar) {

	TString var = iVar->first;
	TString name = iVar->second;
	std::cout << "Variation: " << var << std::endl;

	// open file
	TString fileName = dmDir + "/hist-mcUVic." + name + ".root";
	TFile* f = new TFile(fileName);

	TIter next(f->GetListOfKeys());
      	TKey* key;

      	// loop over histograms
      	while ((key=(TKey*)next())) {

      	  // get key (histo) name
      	  TString hn = key->GetName();       
      	  // only look at signal region cutflow histograms
      	  if (!(hn.Contains("SRMZ1") && hn.Contains("hcutflow"))) continue;
      	  TH1* h = (TH1*)f->Get(hn)->Clone(hn + "_clone");

      	  // loop over cutflow bins
      	  for (int bin = 1; bin <= h->GetNbinsX(); ++bin) {

      	    // extract yield in Final bin
      	    TString label = h->GetXaxis()->GetBinLabel(bin);
	    if (label.Contains("Final") && hn.Contains("SRMZ1ee")) {
	      std::cout << "  Final yield (ee): " << h->GetBinContent(bin) << " +/- " <<  h->GetBinError(bin) << std::endl;
	      if (var == "Nominal") nom_ee = h->GetBinContent(bin);
	      else if (std::abs(nom_ee - h->GetBinContent(bin)) > diff_ee) diff_ee = std::abs(nom_ee - h->GetBinContent(bin));
	    } else if (label.Contains("Final") && hn.Contains("SRMZ1mm")) {
	      std::cout << "  Final yield (mm): " << h->GetBinContent(bin) << " +/- " <<  h->GetBinError(bin) << std::endl;
	      if (var == "Nominal") nom_mm = h->GetBinContent(bin);
	      else if (std::abs(nom_mm - h->GetBinContent(bin)) > diff_mm) diff_mm = std::abs(nom_mm - h->GetBinContent(bin));
	    }
	  }
	}
      }
      std::cout << "Largest deviation from nominal (ee): " << diff_ee << " = " << (diff_ee/nom_ee)*100 << "% of nominal yield" << std::endl;
      std::cout << "Largest deviation from nominal (mm): " << diff_mm << " = " << (diff_mm/nom_mm)*100 << "% of nominal yield" << std::endl;
    } // end loop over each truth DM sample
  }

  // define errors from these results
  double errSmallMasses = 0.06; // <- relative error (6%)
  double errLargeMasses = 0.04; // <- relative error (4%)

  // axial-vector errors
  std::cout << "Outputting errors to psSys_dmA.txt..." << std::endl;
  ofstream out_dmA("psSys_dmA.txt");
  // header
  out_dmA << std::left << std::setw(10) << "runNumber";
  out_dmA << std::right << std::setw(20) << "SRMZ1ee(%)" << std::setw(20) << "SRMZ1mm(%)" << std::endl;
  // loop over all axial-vector samples
  for (std::map<TString, TString>::iterator idm = dmARNMap.begin(); idm != dmARNMap.end(); ++idm) {
    TString runNumber = idm->first;
    TString dmSample = idm->second;
    // find mediator mass 
    double m = std::stod((extractMedMass(dmSample)).Data());
    // assign error
    double error = 0;
    if (m < 100) error = errSmallMasses;
    if (m >= 100) error = errLargeMasses;
    // output to file
    out_dmA << std::left << std::setw(10) << runNumber;
    out_dmA << std::right << std::setw(20) << error << std::setw(20) << error << std::endl;
  }

  // vector errors
  std::cout << "Outputting errors to psSys_dmV.txt..." << std::endl;
  ofstream out_dmV("psSys_dmV.txt");
  // header
  out_dmV << std::left << std::setw(10) << "runNumber";
  out_dmV << std::right << std::setw(20) << "SRMZ1ee(rel)" << std::setw(20) << "SRMZ1mm(rel)" << std::endl;
  // loop over all axial-vector samples
  for (std::map<TString, TString>::iterator idm = dmVRNMap.begin(); idm != dmVRNMap.end(); ++idm) {
    TString runNumber = idm->first;
    TString dmSample = idm->second;
    // find mediator mass 
    double m = std::stod((extractMedMass(dmSample)).Data());
    // assign error
    double error = 0;
    if (m < 100) error = errSmallMasses;
    if (m >= 100) error = errLargeMasses;
    std::cout << "m: " << m << "  err: " << error << std::endl;
    // output to file
    out_dmV << std::left << std::setw(10) << runNumber;
    out_dmV << std::right << std::setw(20) << error << std::setw(20) << error << std::endl;
  }
  
  std::cout << "Normal termination of MonoZPSSysPrint!" << std::endl;
}
