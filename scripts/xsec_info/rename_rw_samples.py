#!/usr/bin/env python
# Python script to loop over all the root files in a directory,
# check if they contain reweighted histograms and copy these
# histograms into a new root file following the default naming scheme.
# Example:
# ./rename_rw_hists.py /home/cranelli/MonoZ/MonoZTruthDev/MonoZTruthUVicRel21/run/truth_2HDMa_MGReweight_20191018/


from ROOT import *
import os
import sys


def rename_rw_hists(path):
    dir = os.path.dirname(path)
    print "Scanning Directory", dir
   
    new_dir = path.replace(dir, dir+"_Rename")
    if not os.path.exists(new_dir):
        os.mkdir(new_dir)
        
    # Walk through input directory
    for root, dirs, files in os.walk(dir, topdown=True):
        for f in files:
            if "process" not in f and "hist" not in f: continue
            print f
            clone_histograms(os.path.join(root,f), new_dir)


            
def clone_histograms(input_file_path, output_dir):
    output_files = {}
    input_file_name = os.path.basename(input_file_path)
    input_file = TFile(input_file_path, "READ")
    
    for key in input_file.GetListOfKeys():
        hist_name = key.GetName()
        #print hist_name
        if "hsum" in hist_name: continue
        
        # Check Reweighting Case (MG vs Hist-based)
        case = "Base"
        if "MGWeights" in hist_name: case = "MGWeights"
        if "HWeights" in hist_name: case = "HWeights"
        
        output_file_name = input_file_name
        hist = input_file.Get(hist_name)
        
        if case != "Base":
            weight_name = extract_weight_name(hist_name)
            print weight_name
            if weight_name == "no_reweight": continue
            new_file_name = rename_file(input_file_name, weight_name, case)
            if(output_file_name == new_file_name): continue #skip if rw is same as base
            output_file_name = new_file_name
            
            # Rename Histogram
            hist.SetName(hist_name.replace("_"+case+"_"+weight_name, "")) # use nominal naming convention
        
        print output_file_name
        
        # If file does not exist for weight, create new file.
        if(output_file_name not in output_files):
            output_files[output_file_name] = TFile(os.path.join(output_dir, output_file_name), "RECREATE")
        output_files[output_file_name].cd()
        hist.Write()
        
    # Close Root Files
    input_file.Close()
    for key in output_files:
        print "Closing ", output_files[key].GetName()
        output_files[key].Close()



# Rename root file based on weight name
# MG Reweighting and Histogram Reweighting handled separately
def rename_file(input_file_name, weight_name, case="MGWeights"):
    #print weight_name
    original_short_name=""
    if("process" in input_file_name): 
        original_short_name = input_file_name.split('_')[2]
    else:
        original_short_name = input_file_name.split('.')[2]
    
    short_name = original_short_name
    
    
    if ( case == "MGWeights"):
        for param in weight_name.split('-'): # '-' used as delimiter for different reweighted parameters
            name, value = param.split('_')
            # 2HDM+a Model
            if "2HDMa" in original_short_name:
                #Cases
                if(name == "SINP"):
                    #short_name = replace_param_value(short_name, "sp", value)
                    short_name = replace_param_value(short_name, "sp", value)
                if(name == "TANB"):
                    #short_name = replace_param_value(short_name, "tb", value)
                    short_name = replace_param_value(short_name, "tb", value)
                
    if ( case == "HWeights"):
        params_base, params_rw = weight_name.split("to")
        if params_base in original_short_name:
            short_name = original_short_name.replace(params_base, params_rw)
        else:
            print "Warning weights do not match base sample"
        
    output_file_name = input_file_name.replace(original_short_name, short_name)
    #print output_file_name
    return output_file_name
                
            
# Find parameter in sample's short_name and replace with reweighted value.
def replace_param_value(short_name, param, value):
    # Split shortname into subelements
    #short_name_elems = short_name.split('_')
    
    # For values replace decimal point with 'p'
    value_str = value.replace('.', 'p')
    
    prev_value_str = ""
    index = short_name.find(param)
    if index == -1:
        print "Warning Parameter %s not in filename", param
    else:
        index +=len(param)
        while(index < len(short_name)):
            if (short_name[index].isalpha() and short_name[index] != "p") or short_name[index] == "_" : break
            prev_value_str += short_name[index]
            index +=1
                
        short_name = short_name.replace( param + prev_value_str, param + value_str)
    
    return short_name

        
# Passed the name of a reweighted histogram,
# uses formatting to returns the weight name.
def extract_weight_name(hist_name):
    #print hist_name
    start = hist_name.find("Weights_") + len("Weights_")
    if "cut" in hist_name:
        weight_name = hist_name[ start : ]
    else:
        stop = hist_name.rfind("_")
        weight_name = hist_name[ start : stop ]
    #print weight_name
    return weight_name
                        

if __name__=="__main__":
    if len(sys.argv) <2:
        print "Input Directory Required"
    else:
        rename_rw_hists(sys.argv[1] +"/")
