#!/usr/bin/env python
import os
import sys
import argparse as ap

#./extractXsec.py /hep300/data-shared/MonoZ/Rel21/mcuser/signal_2HDMa_ggF_MGReweight/Logs

sys.path.append(os.path.abspath(os.path.curdir))

# python script to extract cross section and filter efficiency and save them in a file.


###############################################################################

#outFileName = "2HDMA_tanb_sinp_mH_ma_mDM_official_request_slashh1_5f.txt"

# dictionary to replace strings
#kw = {}
def extractXsec(DIR):
    outFileName = DIR.split('/')[-2] + ".txt"
    print outFileName
    
    f = open(outFileName, 'w')
    f.write("Panda RunNumber  Cross Section (fb)  FilterFactor DataSetName  ReweightName SumOfWeights \n")

    #Change directories
    print "Extracting information from Log files in:"
    print DIR
    os.chdir(DIR)
    
    #print os.path.curdir
    for dataset_dir in os.listdir(os.path.curdir):
        datasetname, extension = os.path.splitext(dataset_dir)
    
        if extension != ".log": continue
        #print datasetname
        # Find Log File in Unpacked TarBall
        for log_dir in os.listdir(dataset_dir):
            if not "tarball" in log_dir: continue

            run_number = log_dir.split('_')[2]
            log_filename= dataset_dir +'/' + log_dir + '/' + "log.generate"
            if not(os.path.exists(log_filename)): continue
        
            # Check job did not fail, if not
            # open log file and read in values   
            flog = open(log_filename, 'r')
            if not "code 0" in flog.readlines()[-2]:
                print "Warning for %s one of the jobs, %s , has a failed log file" % (datasetname, log_dir)
                continue
            flog.seek(0) # Go back to begining of file
        
            for line in flog:
                index1 = line.find('cross-section (nb)')
                index2 = line.find('INFO Filter Efficiency')
                index3 = line.find('sumOfPosWeightsNoFilter')
                index4 =  line.find('sumOfNegWeightsNoFilter')
                if (index1>0):
                    #print(line)
                    list1 = line.split(' ')
                    cross_section = float(list1[4])
                    # Convert from nb to fb
                    cross_section *=1000000
                    # print cross_section
                if (index2>0):
                    list2 = line.split(' ')
                    filter_eff = float(list2[9])
                    # print filter_eff
                if(index3>0):
                    list3 = line.split(' ')
                    sumw_pos_nofilter = float(list3[4])
                if(index4>0):
                    list4 = line.split(' ')
                    sumw_neg_nofilter = float(list4[4])
            
            # Reweighting
            flog.seek(0)
            rw_cross_sections = {} # cross sections calculated by reweighting
            rw_sumws = {} # sum of weights for reweighting
            for line in flog:
                index5 = line.find('Computed cross-section')
                if (index5>0):
                    #print "Do Reweight"
                    for line in flog:
                        if(line.find('quit rwgt') > 0): break
                        list5 = line.split(' ')
                        rw_name = list5[2]
                        rw_cross_section = float(list5[4])
                        # Convert from pb to fb
                        rw_cross_section *=1000
                        rw_cross_sections[rw_name] = rw_cross_section
                        rw_sumws[rw_name] = (sumw_pos_nofilter + sumw_neg_nofilter) * rw_cross_section / cross_section
                        #print rw_name, rw_cross_section

            flog.close()
            line = "%12s %12s %12s %12s \n" %( run_number, cross_section, filter_eff, datasetname)
            f.write(line)
            
            #Reweigted Cross Sections
            for rw_name in rw_cross_sections:
                line = "%12s %12s %12s %12s %12s %12s \n" %( run_number, rw_cross_sections[rw_name], filter_eff, datasetname, rw_name, rw_sumws[rw_name])
                f.write(line)
    f.close()

    print "Samples' cross section, filter efficiency, and reweighting information written to '"'%s'"'" %outFileName  
    return


if __name__ == '__main__':

    parser = ap.ArgumentParser( description = '', formatter_class=ap.RawTextHelpFormatter )
    parser.add_argument( 'dir', type=str,
                         help = ( 'PATH to directory of log files') )
    args = parser.parse_args()

    if len(sys.argv) < 2:
         print "\n ERROR supply path to log files \n"
    print args.dir
    extractXsec(args.dir)
