#!/usr/bin/env python
#Python Code for weighting and summing  all
# the Histograms from two root files.
# Example:
# python weightAndAddHistograms.py gg_bb_sum.root

import os
import sys
import pickle
import argparse

from ROOT import TFile
from ROOT import TH1F
from ROOT import TH2F

"""
###### 2HDM+a Combination ######
OUT_DIR="/home/cranelli/MonoZ/MonooZRootCore/jobs/MC16_2HDM_ggplusbb/"
OUT_XSEC_FILE="MC16_2HDMa_ggplusbb.txt"
#OUT_ROOT_FILE_LOC="gg_bb_sum.root"
GG_XSEC_FILE="../MC16_2HDMa_gg.p"
BB_XSEC_FILE="../MC16_2HDMa_bb.p"
GG_DIR="/home/cranelli/MonoZ/MonooZRootCore/jobs/MC16_2HDM/"
BB_DIR="/home/cranelli/MonoZ/MonooZRootCore/jobs/MC16_2HDM/"
#################################
"""

ROOT_FILE_TEMPLATE="hist-%(sample)s_DAOD_TRUTH3.pool.root.root"
CUTFLOW_HIST="hcut_SRMZ1ee"
TOTAL_BIN=1 
LUMI=140

def weightAndAddHistograms(outFileLoc, weight1, file1Loc, weight2, file2Loc):

    print "weight1: ", weight1 , " weight2: ", weight2

    #Iterate over all the Histograms in the first File
    file1 = TFile(file1Loc, 'READ')
    file2 = TFile(file2Loc, 'READ')
    outFile = TFile(outFileLoc, "RECREATE")
    
    loopList(file1, file2, weight1, weight2)


def loopList(dir1, dir2, weight1, weight2):
    list = dir1.GetListOfKeys()
    for key in list:
        # Loop Over Sub Directories
        if key.GetClassName() == "TDirectoryFile":
            print "Directory" , key.GetName()
            new_dir1 = dir1.Get(key.GetName())
            new_dir2 = dir2.Get(key.GetName())
            loopList(new_dir1, new_dir2)
            continue
        
        # Skip Trees
        if key.GetClassName() == "TTree" : continue
    
        # Combine and Write Histograms
        histName = key.GetName()

        # print histName
        hist1 = dir1.Get(histName)
        hist2 = dir2.Get(histName)

        print hist1, hist2
        
        weightedSumHist = hist1.Clone(histName)
        weightedSumHist.Reset("CE")

        if "hraw" not in histName:
            weightedSumHist.Add(hist1, hist2, weight1, weight2)

        else:  #Handel raw histograms separately, just add them together
            weightedSumHist.Add(hist1, hist2)

        weightedSumHist.Write()

# Extracts the total sum of weights from the cut flow histogram
# (only valid for TRUTH1 DAOD)
def extractSOW(fileLoc):
    file1 = TFile(fileLoc, 'READ')
    sow = file1.Get(CUTFLOW_HIST).GetBinContent(TOTAL_BIN)
    return sow
    

if __name__=="__main__":
# Loop over mass points in gg (official request) and where possible
# sum with bb histograms
    parser = argparse.ArgumentParser(description='Add Histograms for two separate processes')
    parser.add_argument('--xsec1', type=str, help="Path1 to pickle file with xsec info")
    parser.add_argument('--xsec2', type=str, help="Path2 to pickle file with xsec info")
    parser.add_argument('--xsec_out', type=str, help="Path to text file with combined xsec info")
    parser.add_argument('--indir', type=str, help="Path to directory of histograms")
    parser.add_argument('--outdir', type=str, help="Path to output directory")
    args = parser.parse_args()

    if len(sys.argv) < 4:
        print "Pass two sets of pickle files with xsec info and histogram directory path"

    if(args.xsec_out == None): args.xsec_out = args.xsec1.replace("ggF", "ggplusbb").replace(".p", ".txt")
    if(args.outdir == None): args.outdir = args.indir +"_ggplusbb"

    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)

    f = open(args.xsec_out, 'w')
    f.write("RunNumber  Cross Section (fb)  FilterFactor DataSetName  \n")

    xsecs1, filters1, samples1, sumws1 = pickle.load(open(args.xsec1, "rb"))
    xsecs2, filters2, samples2, sumws2 = pickle.load(open(args.xsec2, "rb"))

    unique_mps = set(samples1.keys() + samples2.keys())

    for mp in unique_mps:
        print mp
        dummy_run_number = 999999
        sum_xsec = 0
        avg_filter = 0
        datasetname = 0
        weight1 = 0
        weight2 = 0

        # Case where both ggF and bb samples exist
        if mp in samples1 and mp in samples2:        
            sum_xsec = xsecs1[mp] + xsecs2[mp]
            avg_filter = (xsecs1[mp] * filters1[mp] + xsecs2[mp] * filters2[mp]) / (sum_xsec)
            datasetname = samples1[mp].replace("gg", "ggplusbb")

            fileLoc1 = os.path.join(args.indir, ROOT_FILE_TEMPLATE % {"sample": samples1[mp]})
            fileLoc2 = os.path.join(args.indir, ROOT_FILE_TEMPLATE % {"sample": samples2[mp]})

            sow1 = extractSOW(fileLoc1)
            sow2 = extractSOW(fileLoc2)
            weight1 = LUMI*(xsecs1[mp] * filters1[mp]) / sow1
            weight2 = LUMI*(xsecs2[mp] * filters2[mp]) / sow2
        
        # Case where only ggF sample exists
        if mp in samples1 and mp not in samples2:        
            sum_xsec = xsecs1[mp]
            avg_filter = filters1[mp] 
            datasetname = samples1[mp].replace("gg", "ggplusbb")
            
            fileLoc1 = os.path.join(args.indir, ROOT_FILE_TEMPLATE % {"sample": samples1[mp]})
            fileLoc2 = fileLoc1

            sow1 = extractSOW(fileLoc1)        
            weight1 = LUMI*(xsecs1[mp] * filters1[mp]) / sow1
            weight2 = 0

        # Case where only bb sample exists
        if mp not in samples1 and mp in samples2:        
            sum_xsec =  xsecs2[mp]
            avg_filter = filters2[mp] 
            datasetname = samples2[mp].replace("bb", "ggplusbb")
                
            fileLoc2 = os.path.join(args.indir , ROOT_FILE_TEMPLATE % {"sample": samples2[mp]} )
            fileLoc1 = fileLoc2

            sow2 = extractSOW(fileLoc2)
            weight1 = 0
            weight2 = LUMI*(xsecs2[mp] * filters2[mp]) / sow2

        # Write Combined Cross Section and Average Filter Efficiency to Txt File
        line = "%12s %12s %12s %12s \n" %(dummy_run_number, sum_xsec, avg_filter, datasetname)
        print line
        f.write(line)

        #Weight and Add Histograms
        outFileLoc = os.path.join(args.outdir, ROOT_FILE_TEMPLATE % {"sample": datasetname})
        weightAndAddHistograms(outFileLoc, weight1, fileLoc1, weight2, fileLoc2)
        
    f.close()
