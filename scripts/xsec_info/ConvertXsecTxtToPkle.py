#!/usr/bin/env python
import pickle
import sys 
import os
import argparse
from collections import defaultdict
import rename_rw_samples 

# Python Script for reading in a formatted xsec text file and converting it into a pickle file.
# Stores xsec_info as a list of dictionaries: [xsec_dict, filter_dict, samplename_dict]
# Mass Point is used as the dictionary key, (tb, sp, mH, ma, mDM)
# Example
#./ConvertXSecTxtToPkle "signal_2HDMa_ggF_MGReweight_base_sp0p7.txt" --avg --rw 

def ConvertXsecTxtToPkle(txt_path, doAvg=False, doRW=False):
    txt = open(txt_path, 'r')

    dsid_dict ={}
    xsec_dict = {}
    filter_dict = {}
    sample_dict = {}
    sumw_dict = {}

    print "Reading in File: ", txt_path
    ReadInFile(txt, dsid_dict, xsec_dict, filter_dict, sample_dict, sumw_dict, doAvg, doRW)

    xsec_info = [xsec_dict, filter_dict, sample_dict, sumw_dict]

    #Store as Pickle File
    txt_filename = os.path.split( txt_path )[1]
    pickle_filename = os.path.splitext(txt_filename)[0] + ".p" #pickle extension
    pickle.dump( xsec_info, open( pickle_filename, "wb" ) )
    print "Created Python Pickle File ", pickle_filename

    #Test
    test1, test2, test3, test4 = pickle.load(open(txt_filename.split('.')[0] + ".p", "rb"))
    #for mp in test1:
        #print mp, test1[mp] , test2[mp], test3[mp]


# Reads in Cross Section, Filter, and Sample Name values from formatted text file.
# Panda RunNumber  Cross Section (fb)  FilterFactor DataSetName                      
# If multiple entries exist for a single sample, can choose to take the average xsec, filter
# Check that sample corresponds to unique mass point
def ReadInFile(file, dsid_dict, xsec_dict, filter_dict, sample_dict, sumw_dict, doAvg, doRW):

    dsids = defaultdict(list)
    xsecs = defaultdict(list)
    filters = defaultdict(list)
    samples = defaultdict(list)
    sumws = defaultdict(list)
    
    header = file.readline()

    for line in file:
        entry = line.split()
        #print entry
        sample_name = entry[3]

        # Create pseudo-samplenames based on reweighted parameters
        if len(entry) > 4 and doRW==False:
            print "Warning Datasets Contain Reweighting, set doRW to True"

        if(doRW and len(entry) > 5):
            weight_name = entry[4]
            new_sample_name = rename_rw_samples.rename_file(sample_name, weight_name)
            # If rw is same as base sample, skip
            if (sample_name == new_sample_name) : continue
            else: sample_name = new_sample_name
            #print sample_name
            
        mp = parse_sample_name(sample_name)
        dsids[mp].append(int(entry[0]))
        xsecs[mp].append(float(entry[1]))
        filters[mp].append(float(entry[2]))
        samples[mp].append(sample_name)
        
        #include sum of weights if reweighting
        if(doRW and len(entry) > 5):
            sumws[mp].append(float(entry[5]))

    file.close()

    # Check samples correspond to unique mass point, and average if requested
    for mp in samples:
        unique_samples = set(samples[mp])
        if len(unique_samples) != 1: 
            print "WARNING: Error Sample does not correspond to unique mass point" 
            print unique_samples
                                 
        if(doAvg):
            dsid_dict[mp] = dsids[mp][0]
            xsec_dict[mp] = sum(xsecs[mp])/float(len(xsecs[mp]))
            filter_dict[mp] = sum(filters[mp])/float(len(filters[mp]))
            sample_dict[mp] = list(unique_samples)[0]
            if(doRW): sumw_dict[mp] = sum(sumws[mp])

        # If not averaging, take first element
        else:
            if len(samples[mp]) != 1: 
                print "WARNING: More than one entry per sample, Set doAvg True"
                print samples[mp]

            dsid_dict[mp] = dsids[mp][0]
            xsec_dict[mp] = xsecs[mp][0]
            filter_dict[mp] = filters[mp][0]
            sample_dict[mp] = samples[mp][0]
            if(doRW): sumw_dict[mp] = sumws[mp][0]

        #print dsid_dict[mp] , xsec_dict[mp], filter_dict[mp], sample_dict[mp]

    

#Parses sample names to get tb, sp, mA, and ma values.                      
# user.cranelli.2HDMa_MonoZ_ll_MET40_slashh1_tanb1p0_sinp0p35_mH300_ma150.v6    
def parse_sample_name(name):
    element = next(element for element in name.split(".") if "2HDMa" in element)
    substr = element.split("_tb")[1]
    tanb = float(substr.split("_sp")[0].replace("p", "."))
    sinp = float(substr.split("_sp")[1].split("_mH")[0].replace("p", "."))
    mH = float(substr.split("_mH")[1].split("_ma")[0])
    ma = float(substr.split("_ma")[1].split("_mDM")[0])
    mDM = float(substr.split("_mDM")[1].split("_MGReweight")[0])

    return (tanb, sinp, mH, ma, mDM)

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Read in Cross Section Information and store in Python objects')
    parser.add_argument('filename', type=str, help="Path to txt file with xsec info")
    parser.add_argument('--avg', action='store_true', default=False, dest='do_avg', help="Flag to avg xsec and filter efficiencies if datasets contains multiple jobs.")
    parser.add_argument('--rw', action='store_true', default=False, dest='do_rw', help="Flag if datasets contain reweighting weights")
    args = parser.parse_args()

    if len(sys.argv) < 2:
        print "Pass name of text file to convert"

    ConvertXsecTxtToPkle(args.filename, args.do_avg, args.do_rw)
