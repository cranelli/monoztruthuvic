#!/usr/bin/env python
# Example
# ./AddXSecFiles.py output input1 input2
# input1 is used to assign sample name
# ./AddXSecFiles.py 2HDMA_tanb_sinp_mH_ma_mDM_slashh1_5f_ggplusbb_v2.txt ../2HDMA_tanb_sinp_mH_ma_mDM_slashh1_5f.p 2HDMA_tanb_sinp_mH_ma_mDM_slashh1_5f_bb_v2.p 
# For WP
# ./AddXSecFiles.py 2HDMA_tanb_sinp_mH_ma_MCUser_slashh1_5f_ggplusbb.txt ../2HDMA_tanb_sinp_mH_ma_MCUser_slashh1_5f.p 2HDMA_tanb_sinp_mH_ma_mDM_slashh1_5f_bb_v2.p

import pickle
import sys
import os

def AddXSecFiles(output, pfile1, pfile2):
    xsecs1, filters1, samples1, sumws1 = pickle.load(open(pfile1, "rb"))
    xsecs2, filters2, samples2, sumws2 = pickle.load(open(pfile2, "rb"))

    f = open(output, 'w')

    for mp  in samples1:
        if mp not in samples2: continue

        xsecSum = xsecs1[mp] + xsecs2[mp]
        avgFilter = (xsecs1[mp] * filters1[mp] + xsecs2[mp]*filters2[mp]) / (xsecs1[mp] + xsecs2[mp])
        #print mp, xsecs1[mp] , xsecs2[mp]
        
        dummy_run_number = 999999
        print dummy_run_number, xsecSum, avgFilter, samples1[mp]


        line = "%12s %12s %12s %12s \n" %( str(dummy_run_number), str(xsecSum), str(avgFilter), samples1[mp])
        f.write(line)
        #f.write(str(dummy_run_number) + " " + str(xsecSum) + " " + str(avgFilter) + " " + samples1[mp] + '\n')
        #f.write( str(xsecSum) + " " + samples1[mp] + '\n')

        

# Pass two pickle files to add the cross-sections
# for each of their mass points
if __name__=="__main__":
    AddXSecFiles(sys.argv[1], sys.argv[2], sys.argv[3])
