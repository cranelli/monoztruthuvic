#!/usr/bin/env python 
from ROOT import *
import os
import argparse
import sys
import datetime
import math
import pickle
from array import array
from collections import namedtuple
from array import array

# Example
# ./CalcHistRW.py --xsec ../../share/signal_2HDMa_metadata_hreweight.p --input /home/cranelli/MonoZ/MonoZTruthDev/MonoZTruthUVicRel21/run/signal_2HDMa_truth_2019.11.24 --output 

# ATLAS Style 
ROOT_UTILS_DIR = "/home/cranelli/RootUtils/"
gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasStyle.C")                                                                              
gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasLabels.C")                                                                             
gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasUtils.C")                                                                              
SetAtlasStyle()
gROOT.SetBatch(kTRUE)

######################################################################################
OUTFILE_TEMPLATE="Reweight2HDMa_sp%s_tb%s_mH%s_ma%s_mDM%s_to_sp%s_tb%s_mH%s_ma%s_mDM%s.root"
FILE_TEMPLATE="hist-%s.root"

# REWEIGHTING_SAMPLES[ ( (BASE(tanb, sinp, mH, ma, mDM) , TEST(tanb, sinp, mH, ma, mDM)), .... ]
REWEIGHT_PAIRS= []
REWEIGHT_PAIRS.append( ( (1.0, 0.35, 1000, 350, 10) ,  (1.0, 0.35, 800, 450, 10) ) )
REWEIGHT_PAIRS.append( ( (1.0, 0.35, 1000, 350, 10) ,  (1.0, 0.35, 700, 400, 10) ) )
REWEIGHT_PAIRS.append( ( (1.0, 0.35, 1000, 350, 10) ,  (1.0, 0.35, 350, 200, 10) ) )
REWEIGHT_PAIRS.append( ( (1.0, 0.35, 350, 200, 10)  ,  (1.0, 0.35, 225, 100, 10) ) )


CHANNELS = ["ee", "mm"]
# HISTOGRAM NAMES ( Zwindow Cut )
HIST_NAME_TEMPLATES=[ "hgen_TRUSRMZ1%(chan)s_ZWIN_truthPtll", 
                      "hgen_TRUSRMZ1%(chan)s_ZWIN_truthPtdmdm",
                      "hgen_TRUSRMZ1%(chan)s_ZWIN_truthdmdmPTvsDP",
                      "hgen_TRUSRMZ1%(chan)s_ZWIN_truthllPTvsDP",
                      "hgen_TRUSRMZ1%(chan)s_ZWIN_truthllPTvsdmdmPT",
                      "hgen_TRUSRMZ1%(chan)s_ZWIN_truthllPTvsdmdmPTvsDP",]
                      #"hgen_TRUSRMZ1%(chan)s_ZWIN_MTZZll", 
                      #"hgen_TRUSRMZ1%(chan)s_ZWIN_MTZa350ll", 

CUTFLOW="hcut_SRMZ1"

#################################################################################

# Calculate weights for each base/test sample pairing.
def CalcHistRW(base_mp, test_mp, input_dir, output_dir, xsec_info, figure_dir):

    xsecs, filters, samples, sumws = pickle.load(open( xsec_info, "rb"))
    
    base_mp_str = tuple([ str(param).replace(".", "p") for param in base_mp ])
    test_mp_str = tuple([ str(param).replace(".", "p") for param in test_mp ])

    base_filename = FILE_TEMPLATE % samples[base_mp]
    test_filename = FILE_TEMPLATE % samples[test_mp]

    output_filename = OUTFILE_TEMPLATE % (base_mp_str + test_mp_str )
    output_path = os.path.join(args.output, output_filename)

    base_file = TFile(os.path.join(input_dir, base_filename), "READ")
    test_file = TFile(os.path.join(input_dir, test_filename), "READ")
    output_file = TFile(os.path.join(output_dir, output_filename), "RECREATE")

    base_file_scale = xsecs[base_mp] * filters[base_mp] / GetN(base_file)
    test_file_scale = xsecs[test_mp] * filters[test_mp] / GetN(test_file)

    for hist_name_template in HIST_NAME_TEMPLATES:
        ReweightHist(base_file, base_file_scale, test_file, test_file_scale, output_file, hist_name_template, figure_dir)

    base_file.Close()
    test_file.Close()
    output_file.Close()

# Take Ratio of Histograms from base and test samples
def ReweightHist(base_file, base_scale, test_file, test_scale, outfile, hist_name_template, figure_dir):
    
    # Sum channels (Lepton Universality at Truth Level)
    base_sum_hist = SumChannels(base_file, hist_name_template, CHANNELS)
    test_sum_hist = SumChannels(test_file, hist_name_template, CHANNELS)
    
    # Scale Histograms 
    base_sum_hist.Scale(base_scale)
    test_sum_hist.Scale(test_scale)
    
    # Rebin
    #base_sum_hist, test_sum_hist = MultiAxisRebin(base_sum_hist, test_sum_hist)

    # Take Ratio
    ratio_hist = test_sum_hist.Clone("weight_"+test_sum_hist.GetName())
    ratio_hist.Divide(base_sum_hist)

    # Write
    ratio_hist.Write()
    
    # Draw
    os.system("mkdir -p %s" % (figure_dir ) )
    base_short = FileShortName(base_file.GetName())
    test_short = FileShortName(test_file.GetName())
    DrawHists(base_sum_hist, test_sum_hist, ratio_hist, base_short, test_short, figure_dir)

# Draw Base, Test, and Weight Histograms
def DrawHists(base_sum_hist, test_sum_hist, ratio_hist, base_short, test_short, figure_dir):
    
    hist_short = base_sum_hist.GetName().split('_')[3]

    # 1D Hist
    if( base_sum_hist.ClassName() == "TH1F"):
        DrawHistToPDF(base_sum_hist, "ll", os.path.join(figure_dir, base_short + "_" + hist_short))
        DrawHistToPDF(test_sum_hist, "ll", os.path.join(figure_dir, test_short + "_" + hist_short))
        DrawHistToPDF(ratio_hist, "ll", os.path.join(figure_dir, "weight_"+hist_short))
    
    # 2D Hist
    if( base_sum_hist.ClassName() == "TH2F"):
        DrawTableToPDF(base_sum_hist, "ll", os.path.join(figure_dir, base_short + "_" + hist_short), "Normalized")
        DrawTableToPDF(test_sum_hist, "ll", os.path.join(figure_dir, test_short + "_" + hist_short), "Normalized")
        DrawTableToPDF(ratio_hist, "ll", os.path.join(figure_dir, "weight_"+hist_short), "Weight")

    # 3D Hist (Difficult to Draw)  Look at Projections
    #if( base_sum_hist.ClassName() == "TH3F"):

# Draw 1D Hists
def DrawHistToPDF(hist, ch, figure_path):
    c1 = TCanvas()
    c1.SetTopMargin(0.1)
    c1.SetRightMargin(0.2)
    
    gStyle.SetOptStat(0)
    gStyle.SetOptStat(0)
    TGaxis.SetMaxDigits(3)
    #gStyle.SetPalette(kBird)
    
    #gStyle.SetPaintTextFormat("4.2e")
    
    #hist.Draw("Colz");
    hist.SetTitle("")
    hist.GetXaxis().SetNoExponent(kTRUE)
    hist.GetYaxis().SetNoExponent(kTRUE)
    hist.SetMarkerSize(0.75)
    hist.Draw()
    
    c1.SaveAs(figure_path +"_" + ch + ".pdf")
    #c1.Write()

# Draw 2D Hists
def DrawTableToPDF(table, ch, figure_path, ztitle):
    c1 = TCanvas()
    c1.SetTopMargin(0.1)
    c1.SetRightMargin(0.2)
    
    gStyle.SetOptStat(0)
    gStyle.SetOptStat(0)
    TGaxis.SetMaxDigits(3)
    gStyle.SetPalette(kBird)
    #gStyle.SetPaintTextFormat("4.2e")

    table.Draw("Colz");
    table.SetTitle("")
    table.GetXaxis().SetNoExponent(kTRUE)
    table.GetYaxis().SetNoExponent(kTRUE)
    table.GetZaxis().SetTitle(ztitle)
    table.Draw("Colz")

    c1.SaveAs(figure_path +"_" + ch + ".pdf")
    #c1.Write()


# Sum Histograms for the different channels
# Works for both 1D and 2D histograms
def SumChannels(hist_file, hist_name_template, channels):
    hists = {}
    # Find Dimensino of Histogram
    hist_name = hist_name_template % {"chan" : channels[0]}
    sum_hist = hist_file.Get(hist_name).Clone(hist_name_template % {"chan" : "ll"})
    sum_hist.Reset("ICES")
    
    for ch in channels:
        hist_name = hist_name_template % {"chan" : ch}
        hists[ch] = hist_file.Get(hist_name)
        sum_hist.Add( hists[ch] )
    return sum_hist


"""
def MultiAxisRebin(base_hist, test_hist):

    base_hist_rebin=base_hist
    test_hist_rebin=test_hist

    # 1D Hist                                                                                                             
    if( base_hist.ClassName() == "TH1F"):
        #min_percent_error=0.1
        bin_edges = Rebin(base_hist)
        
        base_hist_rebin = base_hist.Rebin( len(bin_edges)-1, base_hist.GetName() + "_rebin", array('d', bin_edges) )
        test_hist_rebin = test_hist.Rebin( len(bin_edges)-1, test_hist.GetName() + "_rebin", array('d', bin_edges) )

    # 2D Hist
    if( base_hist.ClassName() == "TH2F"):
        base_hist_x = base_hist.ProjectionX("_px", 0, -1, "e")
        bin_edges_x = Rebin(base_hist_x)
        base_hist_y = base_hist.ProjectionY("_py", 0, -1, "e")
        bin_edges_y = Rebin(base_hist_y)

    return base_hist_rebin, test_hist_rebin

# Rebin Histogram so that the relative errors (in 1D) are less than 10%.
# Both Base and Test Histograms are rebinned in the same way.
def Rebin(base_hist):
    min_percent_error=0.33
    axis = base_hist.GetXaxis()
    i=1;
    bin_edges = []
    bin_edges.append(axis.GetXmin())
    content = sumw2 = 0
    for i in range(1, base_hist.GetNbinsX()):
        content += base_hist.GetBinContent(i)
        sumw2 += pow(base_hist.GetBinError(i) , 2)
        i +=1
        if pow(sumw2, 0.5) < min_percent_error * content:
            bin_edges.append(axis.GetBinLowEdge(i))
            content = sumw2 = 0
    bin_edges.append(axis.GetXmax())

    return bin_edges
"""

# Returns the total number of events in the sample,
# uses cut-flow hist
def GetN(sample_histfile):
    cutflow = sample_histfile.Get(CUTFLOW + "ee")
    total_bin=1
    N = cutflow.GetBinContent(total_bin)
    return N

# Returns short name for file
def FileShortName(full_name):
    return full_name[full_name.find("mH"):full_name.find("_mDM")]

# Returns short name for histogram
def HistShortName(full_name):
    return full_name.split('_')[3]


if __name__=="__main__":
    PATH = os.getcwd()
    parser = argparse.ArgumentParser(description='Plot Cross Section Information')
    parser.add_argument('--xsec', type=str, help="Path to txt file with xsec info")
    parser.add_argument('--input', type=str, help="Path to input directory of truth histograms")
    parser.add_argument('--output', type=str, help="Path to output directory")
    args = parser.parse_args()

    if(args.xsec == None):
        print "Specify --xsec, path to txt file with xsec info"

    if(args.input == None):
        print "Specify --input, path to directory of truth histograms"

    if(args.output==None): 
        args.output = os.getcwd() + "/2HDMaReweights_" + str(datetime.date.today())

    os.system("mkdir -p %s" % (args.output ) )

    figure_dir = os.path.join(args.output, "Figures")
    os.system("mkdir -p %s" % (figure_dir ) )
    
    for pair in REWEIGHT_PAIRS:
        base_mp, test_mp = pair
        #print output_path                           
        CalcHistRW(base_mp, test_mp, args.input, args.output, args.xsec, figure_dir)
