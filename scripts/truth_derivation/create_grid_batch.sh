#!/bin/bash

# Script for running GRID jobs to create TRUTH1 DAODs from EVNT files.
# Note files being run over must also exist on the GRID.  Local files only used
# for extract the datasets name. 

# Example
# ./create_grid_batch.sh "TRUTH3" /hep300/data-shared/MonoZ/Rel21/samples/evnt/mc16signal_2HDMa_EVNT

TRUTH=$1
INPUT_DIR=$2

BATCH_SCRIPT="batch_${TRUTH}.sh"
RECOVERY_SCRIPT="mc16_recovery_${TRUTH}.sh"

if [ $TRUTH = "TRUTH1" ]
then
    echo "#asetup 20.1.8.1,AtlasDerivation,here" > ${BATCH_SCRIPT}
    echo "#lsetup rucio" >> ${BATCH_SCRIPT}
    echo "#lsetup panda" >> ${BATCH_SCRIPT}
fi

if [ $TRUTH = "TRUTH3" ]
then 
    echo "#asetup 21.2.68.0, AthDerivation,here" > ${BATCH_SCRIPT}
    echo "#lsetup rucio" >> ${BATCH_SCRIPT}
    echo "#lsetup panda" >> ${BATCH_SCRIPT}
fi


for full_path in `ls -d $INPUT_DIR/*`;
#for sample in `ls -d mc16*`;
do
    if [ -d "${full_path}" ]  # Check that passed path is a directory
    then
	sample="${full_path##*/}"
	shortname=$(cut -d'.' -f1,2,3  <<< ${sample})
	echo $shortname

	echo "pathena --trf" '"'  "Reco_tf.py --inputEVNTFile=%IN --outputDAODFile=%OUT.pool.root --reductionConf ${TRUTH}" '"' "--inDS $sample --outDS user.cranelli.$shortname.DAOD_${TRUTH} --nFilesPerJob 10" >> ${BATCH_SCRIPT}
        # pathena --trf "Reco_tf.py --inputEVNTFile=%IN --outputDAODFile=%OUT.daod.root --reductionConf TRUTH1" --inDS $sample --outDS user.cranelli.$shortname --nFilesPerJob 10
	echo "rucio download user.cranelli.${shortname}.DAOD_${TRUTH}_EXT0" >> ${RECOVERY_SCRIPT}
    fi
done
