#!/usr/bin/env python 
# Example
# ./CrossSectionTable.py signal_2HDMa_ggF_MGReweight_base_sp0p7.txt --rw
from ROOT import *
import os
import sys
from collections import namedtuple
from collections import defaultdict
from collections import OrderedDict
import pickle
#import ConvertXsecTxtToPkle
from array import array
import argparse

gROOT.SetBatch(kTRUE)
ROOT_UTILS_DIR = "/home/cranelli/RootUtils/"
gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasStyle.C")
gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasLabels.C")
gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasUtils.C")
SetAtlasStyle()

#gROOT.LoadMacro('DMWGStyle2017.cxx')

DO_AVG=True # By Default avg cross sections for samples with multiple jobs.

OUTFILE_LOC="CrossSectionTables_2HDMa_MCUser.root"
FIGURE_DIR="Figures/"

########################################################################################################
#Define the Grids

BinSetting2D = namedtuple('BinSetting2D', ['xbins', 'xbinsLow', 'ybins', 'ybinsLow'])

#mA-ma Grid
mAma_titles = "; ma [GeV]; mA [GeV]"
mAma_bs=BinSetting2D(9, array('d', [75, 125, 175, 225, 275, 325, 375, 425, 475, 525]), 18, array('d', [75, 125, 175, 225, 275, 325, 375, 425, 475, 525, 575, 650, 750, 850, 950, 1050, 1150, 1250, 1750]) )
mAma_tanb_fixed = 1.0
mAma_sinp_fixed = 0.35
mAma_mDM_fixed = 10

#tanb-ma Grid
tanbma_titles = "; ma [GeV]; tan#beta"
tanbma_bs=BinSetting2D( 7, array('d', [75, 125, 175, 225, 275, 325, 375, 425]), 8, array('d', [0.2, 0.4, 0.75, 1.25, 2.5, 3.5, 7, 15, 25]) )
tanbma_sinp_fixed = 0.35
tanbma_mA_fixed = 600
tanbma_mDM_fixed = 10

#tanb-mA Grid
tanbmA_titles="; mA [GeV]; tan#beta"
tanbmA_bs=BinSetting2D( 16, array('d', [175, 225, 275, 325, 375, 425, 550, 650, 750, 850, 950, 1050, 1350, 1550, 1650, 1850, 2150]), 8, array('d', [0.2, 0.4, 0.75, 1.25, 2.5, 3.5, 7, 15, 25]) )
tanbmA_sinp_fixed = 0.7
tanbmA_ma_fixed = 250
tanbmA_mDM_fixed = 10

#sinp scan1
sinpscan1_mA_fixed=600
sinpscan1_ma_fixed=200
sinpscan1_tanb_fixed=1.0
sinpscan1_mDM_fixed=10

#sinp scan2 
sinpscan2_mA_fixed=1000
sinpscan2_ma_fixed=350
sinpscan2_tanb_fixed=1.0
sinpscan2_mDM_fixed=10

######################################
# Book Histograms

hstore={}
hstore["mAma_crosssection_table"]=TH2F("mAma_xsec", mAma_titles, mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
hstore["mAma_filter_table"]=TH2F("mAma_filter",mAma_titles,  mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)

hstore["tanbma_crosssection_table"]=TH2F("tanbma_xsec", tanbma_titles, tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
hstore["tanbma_filter_table"]=TH2F("tanbma_filter", tanbma_titles, tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)

hstore["tanbmA_crosssection_table"]=TH2F("tanbmA_xsec", tanbmA_titles, tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
hstore["tanbmA_filter_table"]=TH2F("tanbmA_filter", tanbmA_titles, tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)


def CrossSectionTable(xsec_filepath, do_rw):
   #Get Dictionary of cross sections and filters at each mass point from pickle file
   #ConvertXsecTxtToPkle.ConvertXsecTxtToPkle(xsec_filepath, DO_AVG, do_rw)
   xsecs, filters, samples, sumws = pickle.load(open(xsec_filepath, "rb"))
   
   sinpscan1=TGraph()
   sinpscan2=TGraph()

   sinpscan1b=TGraph()
   sinpscan2b=TGraph()

   point1 = 0
   point2 =0
   
   point1b=0
   point2b=0
   for mp in xsecs:
       tanb = mp[0]
       sinp = mp[1]
       mA = mp[2]
       mH = mA
       ma = mp[3]
       mDM = mp[4]
       
       #mA-ma Grid
       if(tanb == mAma_tanb_fixed and sinp == mAma_sinp_fixed and mDM == mAma_mDM_fixed):
          #CheckFill(hstore["mAma_crosssection_table"], ma, mA, xsecs[mp])
          #CheckFill( hstore["mAma_filter_table"],  ma, mA, filters[mp])
          hstore["mAma_crosssection_table"].Fill(ma, mA, xsecs[mp])
          hstore["mAma_filter_table"].Fill(ma, mA, filters[mp])
           
       #tanb-ma Grid
       if(sinp == tanbma_sinp_fixed and mA == tanbma_mA_fixed and mDM == tanbma_mDM_fixed):
          #CheckFill( hstore["tanbma_crosssection_table"], ma, tanb, xsecs[mp])
          #CheckFill( hstore["tanbma_filter_table"], ma, tanb, filters[mp])
          hstore["tanbma_crosssection_table"].Fill(ma, tanb, xsecs[mp])
          hstore["tanbma_filter_table"].Fill(ma, tanb, filters[mp])


       #tanb-mA Grid
       if(sinp == tanbmA_sinp_fixed and ma == tanbmA_ma_fixed and mDM == tanbmA_mDM_fixed):
          #CheckFill( hstore["tanbmA_crosssection_table"], mA, tanb, xsecs[mp])
          #CheckFill( hstore["tanbmA_filter_table"], mA, tanb, filters[mp])
          hstore["tanbmA_crosssection_table"].Fill(mA, tanb, xsecs[mp])
          hstore["tanbmA_filter_table"].Fill(mA, tanb, filters[mp])

       #sinp scan1
       if(mA == sinpscan1_mA_fixed and ma == sinpscan1_ma_fixed and tanb == sinpscan1_tanb_fixed and mDM == sinpscan1_mDM_fixed):
          print mp, xsecs[mp]
          sinpscan1.SetPoint(point1, sinp, xsecs[mp])
          point1 += 1
       #sinp scan2
       if(mA == sinpscan2_mA_fixed and ma == sinpscan2_ma_fixed and tanb == sinpscan2_tanb_fixed and mDM == sinpscan2_mDM_fixed):
          print mp, xsecs[mp]
          sinpscan2.SetPoint(point2, sinp, xsecs[mp])
          point2 += 1


   # Write Root Files
   outfile =TFile(OUTFILE_LOC, "RECREATE")
   for hist in hstore: hstore[hist].Write()

   # Draw Figures
   DrawTableToPDF(hstore["mAma_crosssection_table"], "mAma_CrossSections", (0.1 , 100), "Cross Section [fb]", False, True)
   DrawTableToPDF(hstore["mAma_filter_table"], "mAma_METFilter", (0.5 , 1.04), "MET Filter Efficiency", False, False)

   DrawTableToPDF(hstore["tanbma_crosssection_table"], "tanbma_CrossSections", (0.1 , 100), "Cross Section [fb]", True, True)
   DrawTableToPDF(hstore["tanbma_filter_table"],"tanbma_METFilter", (0.5 , 1.04), "MET Filter Efficiency", True, False)

   DrawTableToPDF(hstore["tanbmA_crosssection_table"], "tanbmA_CrossSections", (0.1 , 100), "Cross Section [fb]", True, True)
   DrawTableToPDF(hstore["tanbmA_filter_table"],"tanbmA_METFilter", (0.5 , 1.04), "MET Filter Efficiency", True, False)

   #sinpscan1.Draw("AC*")
   DrawSinpScan(sinpscan1, sinpscan2, sinpscan1_mA_fixed, sinpscan1_ma_fixed, sinpscan2_mA_fixed, sinpscan2_ma_fixed)


###############################################################
def DrawTableToPDF(table, title, zrange, ztitle, logy, logz):
   c1 = TCanvas()
   if(logy): c1.SetLogy()
   if(logz): c1.SetLogz()
   c1.SetRightMargin(0.16)
   gStyle.SetOptStat(0)
   gStyle.SetOptStat(0)
   gStyle.SetPalette(kBird)
   #gStyle.SetPalette(1)
   gStyle.SetPaintTextFormat("4.2f");
   table.Draw("Colz, text");

   table.GetZaxis().SetRangeUser(zrange[0], zrange[1]);

   table.GetZaxis().SetTitle(ztitle)
   #table.GetZaxis().SetTitleOffset(1.1)
   #table.GetZaxis().SetTitleSize(0.04)
   #table.GetZaxis().SetLabelSize(0.04)
   table.SetTitle("");
   table.Draw("Colz, text");

   pt = TPaveText(0.50,0.80,0.8,0.9,"brNDC")
   pt.SetTextSize( 0.05 )
   #pt.SetTextFont( 42 )
   pt.SetFillColor( 0 )
   pt.SetFillStyle( 0 )
   pt.SetBorderSize( 0 )
   #pt.AddText( "2HDMa ")
   pt.Draw()
   c1.SaveAs(FIGURE_DIR + "2HDMa_" + title + ".pdf")
   c1.SaveAs(FIGURE_DIR + "2HDMa_" + title + ".png")
   c1.Write()

   

def DrawSinpScan(sinpscan1, sinpscan2, mA1, ma1, mA2, ma2):
   c1 = TCanvas()
   gStyle.SetOptStat(0)
   sinpscan1.SetMarkerStyle(8)
   sinpscan1.SetMarkerSize(0.8)
   sinpscan1.SetMinimum(0)
   sinpscan1.SetMaximum(32)
   sinpscan1.GetXaxis().SetLimits(0,1)
   sinpscan1.GetXaxis().SetTitle("sin#theta")
   sinpscan1.GetYaxis().SetTitle("\sigma(pp#rightarrow#chi#chi ll) [fb]")

   sinpscan1.Draw("AP")

   sinpscan2.SetMarkerStyle(8)
   sinpscan2.SetMarkerColor(kRed)
   sinpscan2.SetMarkerSize(0.8)
   sinpscan2.SetMinimum(0)
   sinpscan2.GetXaxis().SetLimits(0,1)
   sinpscan2.Draw("P")


   #sinpscan1.Draw("AP")
   #c1.Update()

   f1 = TF1("f1", "[0]*x*x/([1]+x*x)")
   f1.SetParameter(0, 50)
   f1.SetLineStyle(2)
   f1.SetLineColor(kBlack)
   sinpscan1.Fit(f1)

   f2 = TF1("f2", "[0]*x*x/([1]+x*x)*(1-x*x)/((1-x*x)+[2]*x*x)")
   f2.SetParameter(0, 50)
   f2.SetLineStyle(2)
   f2.SetLineColor(kRed)
   sinpscan2.Fit(f2)

   #f1.Draw("same")
   leg = TLegend(0.12, 0.55, 0.62, 0.88)
   leg.AddEntry(sinpscan1, "m_{A}(m_{H})= " +str(mA1) + " GeV, m_{a}= " + str(ma1) + " GeV", "p")
   leg.AddEntry(f1, "[0] #frac{sin^{2}#theta}{[1]+sin^{2}#theta}")
   leg.AddEntry(sinpscan2, "m_{A}(m_{H})= " + str(mA2) + " GeV, m_{a}= " + str(ma2) + " GeV", "p")
   leg.AddEntry(f2, "[0] #frac{sin^{2}#theta cos^{2}#theta}{([1]+sin^{2}#theta)([2]sin^{2}#theta+cos^{2}#theta)}")
   leg.Draw()

   c1.SaveAs(FIGURE_DIR + "2HDMa_SinpScan.pdf")

# For xsec and filter efficiency table, 
# make sure mass point corresponds to the centrer of 
# the bin.
def CheckFill(hist, x, y, val):
   x_center = hist.GetXaxis().GetBinCenter(hist.GetXaxis().FindBin(x))
   y_center = hist.GetYaxis().GetBinCenter(hist.GetYaxis().FindBin(y))
   if x == x_center and y == y_center:
      hist.Fill(x, y, val)

   # Special Cases
   if  x == x_center and y == 600:
      hist.Fill(x, y, val)
   
   if x == x_center and y == 0.5:
      hist.Fill(x, y, val)
      

if __name__=="__main__":
   parser = argparse.ArgumentParser(description='Plot Cross Section Information')
   parser.add_argument('filepath', type=str, help="Path to pickle file with xsec info")
   parser.add_argument('--rw', action='store_true', default=False, dest='do_rw', help="Flag if datasets contain reweighting weights")
   args = parser.parse_args()

   if len(sys.argv) < 2:
      print "Pass name of text file to convert"

   CrossSectionTable(args.filepath, args.do_rw)
