#!/usr/bin/env python 
# Example 
# ./AcceptanceTable.py --xsec signal_2HDMa_ggF_MGReweight_base_sp0p7.p --indir truth_signal_2HDMa_MGReweightbase_sp0p7_2019.11.26_Rename --rw
# ./AcceptanceTable.py --xsec signal_2HDMa_ggplusbb_MGReweight_base_sp0p7.p --indir truth_signal_2HDMa_MGReweightbase_sp0p7_2019.11.26_Rename_ggplusbb

from ROOT import *
import os
import sys
import math
from array import array
from collections import namedtuple
import pickle
import argparse

# ATLAS Style
gROOT.SetBatch(kTRUE)
ROOT_UTILS_DIR = "/home/cranelli/RootUtils/"
#gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasStyle.C")
#gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasLabels.C")
#gROOT.LoadMacro(ROOT_UTILS_DIR + "AtlasUtils.C")
#SetAtlasStyle()

gROOT.LoadMacro('/home/cranelli/MonoZ/Latex/DMWG-2HDM-whitepaper/macros/DMWGStyle2017.cxx')
SetDMWGStyle(True) 

#####################################################################
#Configuration
#IS_WP=False 

# User Generated ##
#HIST_DIR="/home/cranelli/MonoZ/MonoZTruthDev/MonoZTruthUVicRel21/run/plot_truth_2HDMa_MGReweight_base_sp0p7_20191018_Rename/"
OUTFILE_LOC="AcceptanceTables_2HDMa.root"
HIST_FILE_TEMPLATE="hist-%(dataset_name)s_DAOD_TRUTH3.pool.root.root"
#HIST_FILE_TEMPLATE="hist-%(dataset_name)s.root"
FIGURE_DIR="2HDMa_Figures/"

#DO_AVG=True # By Default avg cross sections for samples with multiple jobs.

#cutflow
CUTFLOW="hcut_SRMZ1"
TOTAL_BIN=1
FINAL_BIN=10

CHANNELS= [ 'mm', 'ee' ]

BACKGROUND_FILE_LOC="/home/cranelli/MonoZ/MonoZTruthDev/MonoZTruthUVicRel21/source/MonoZTruthUVic/scripts/plotting/Backgrounds/process_mc16amc16dmc16e_allbackgrounds.root"

#####  Define the Grids #######################################################################

#BinSetting = namedtuple('BinSetting', ['xbins', 'xmin', 'xmax', 'ybins', 'ymin', 'ymax'])
BinSetting1D = namedtuple('BinSetting1D', ['xbins', 'xbinsLow'])
BinSetting2D = namedtuple('BinSetting2D', ['xbins', 'xbinsLow', 'ybins', 'ybinsLow'])

#mA-ma Grid   
mAma_titles = "; ma [GeV]; mA [GeV]"
mAma_bs=BinSetting2D(8, array('d', [75, 125, 175, 225, 275, 325, 375, 425, 475]), 17, array('d', [75, 125, 175, 225, 275, 325, 375, 425, 475, 525, 575, 650, 750, 850, 950, 1050, 1150, 1250]) )
mAma_tanb_fixed = 1.0
mAma_sinp_fixed = 0.35
mAma_mDM_fixed = 10

#tanb-ma Grid 
tanbma_titles = "; ma [GeV]; tan#beta"
tanbma_bs=BinSetting2D( 7, array('d', [75, 125, 175, 225, 275, 325, 375, 425]), 8, array('d', [0.2, 0.4, 0.75, 1.25, 2.5, 3.5, 7, 15, 25]) )
tanbma_sinp_fixed = 0.35
tanbma_mA_fixed = 600
tanbma_mDM_fixed = 10

#tanb-mA Grid 
tanbmA_titles="; mA [GeV]; tan#beta"
tanbmA_bs=BinSetting2D( 15, array('d', [175, 225, 275, 325, 375, 425, 550, 650, 750, 850, 950, 1050, 1350, 1550, 1850, 2150]), 8, array('d', [0.2, 0.4, 0.75, 1.25, 2.5, 3.5, 7, 15, 25]) )
tanbmA_sinp_fixed = 0.7
tanbmA_ma_fixed = 250
tanbmA_mDM_fixed = 10

#sinp scan1
sinpscan1_mA_fixed=600
sinpscan1_ma_fixed=200
sinpscan1_tanb_fixed=1.0
sinpscan1_mDM_fixed=10

#sinp scan2
sinpscan2_mA_fixed=1000
sinpscan2_ma_fixed=350
sinpscan2_tanb_fixed=1.0
sinpscan2_mDM_fixed=10

# mDM Scan
mDM_tanb_fixed = 1.0
mDM_sinp_fixed = 0.35
mDM_mA_fixed = 600
mDM_ma_fixed = 250


##############################################################################################
gROOT.ProcessLine(
"struct Sample {\
   Double_t     tanb;\
   Double_t     sinp;\
   Double_t     mA;\
   Double_t     ma;\
   Double_t     mDM;\
   Double_t     xsec;\
   Double_t     filter;\
   Double_t     sumw;\
};" );

gROOT.ProcessLine(
"struct SampleChannel {\
   Double_t     total;\
   Double_t     final;\
   Double_t     acceptance;\
   Double_t     ae;\
   Double_t     ae_error;\
   Double_t     sob;\
   Double_t     son;\
   Double_t     significance;\
   Double_t     significance_wsyst;\
   TH1F         mtzz_hist;\
};");

gROOT.ProcessLine(
"struct ZZllnunuChannel {\
   Double_t     total;\
   Double_t     final;\
   TH1F         mtzz_hist;\
};");

gROOT.ProcessLine(
"struct BackgroundChannel {\
  TH1F         mtzz_hist;\
};");

##############################################################################################

def AcceptanceTable(xsec_filepath, hist_dir, do_rw):
    #Get Dictionary of cross sections and filters at each mass point from pickle file
    #ConvertXsecTxtToPkle.ConvertXsecTxtToPkle(xsec_filepath, DO_AVG, do_rw)
    xsecs, filters, sample_names, sumws = pickle.load(open(xsec_filepath, "rb"))

    background_file = TFile(BACKGROUND_FILE_LOC, "READ")

    outfile =TFile(OUTFILE_LOC, "RECREATE")

    hstore = {}
    
    #mA-ma Grid Histograms (Combined Channels)
    hstore["mAma_xsec"]=TH2F("mAma_xsec", "mAma_xsec;  M_{a} [GeV]; M_{A} [GeV]", mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    hstore["mAma_sum_a"]=TH2F("mAma_sum_A", "mAma Sum A;  M_{a} [GeV]; M_{A} [GeV]", mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    hstore["mAma_sum_ae"]=TH2F("mAma_sum_AE", "mAma Sum Ae;  M_{a} [GeV]; M_{A} [GeV]", mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    hstore["mAma_yield"]=TH2F("mAma_yield", "mAma Yield Ae;  M_{a} [GeV]; M_{A} [GeV]", mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    hstore["mAma_combined_sob"]=TH2F("mAma_Combined_SoB", "mAma Combined SoB,  M_{a} [GeV], M_{A} [GeV]",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    hstore["mAma_combined_son"]=TH2F("mAma_Combined_SoN", "mAma Combined SoN,  M_{a} [GeV], M_{A} [GeV]",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    hstore["mAma_combined_significance"]=TH2F("mAma_Combined_Significance", "mAma Combined Significance;  M_{a} [GeV]; M_{A} [GeV]",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    hstore["mAma_combined_significance_wsyst"]=TH2F("mAma_Combined_Significance_wsyst", "mAma Combined Significance (with background systematic);  M_{a} [GeV]; M_{A} [GeV]",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    #hstore["difmAma500_combined_acceptance"]=TH1F("difmAma500_Combined_Acceptance", "Combined Acceptance for mA ma Difference 500; M_{a} [GeV]; Acceptance", mAma_bs.xbins, mAma_bs.xbinsLow); 
    #hstore["difmAma250_combined_acceptance"]=TH1F("difmAma250_Combined_Acceptance", "Combined Acceptance for mA ma Difference 500; M_{a} [GeV]; Acceptance", mAma_bs.xbins, mAma_bs.xbinsLow);
    #hstore["mA900_combined_acceptance"]=TH1F("mA900_Combined_Acceptance", "Combined Acceptance for mA equal 900; M_{a} [GeV]; Acceptance", mAma_bs.xbins, mAma_bs.xbinsLow);
    #hstore["mA800_combined_acceptance"]=TH1F("mA800_Combined_Acceptance", "Combined Acceptance for mA equal 800; M_{a} [GeV]; Acceptance", mAma_bs.xbins, mAma_bs.xbinsLow);
    #hstore["mA700_combined_acceptance"]=TH1F("mA700_Combined_Acceptance", "Combined Acceptance for mA equal 700; M_{a} [GeV]; Acceptance", mAma_bs.xbins, mAma_bs.xbinsLow);
    #hstore["mA600_combined_acceptance"]=TH1F("mA600_Combined_Acceptance", "Combined Acceptance for mA equal 600; M_{a} [GeV]; Acceptance", mAma_bs.xbins, mAma_bs.xbinsLow);


    #tanb-ma Grid Histograms (Combined Channels)
    hstore["tanbma_xsec"]=TH2F("tanbma_xsec", "tanbma xsec;  M_{a} (GeV); tan#beta",tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
    hstore["tanbma_sum_a"]=TH2F("tanbma_Average_A", "tanbma Average A;  M_{a} (GeV); tan#beta", tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
    hstore["tanbma_sum_ae"]=TH2F("tanbma_Average_AE", "tanbma Average Ae;  M_{a} (GeV); tan#beta", tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
    hstore["tanbma_yield"]=TH2F("tanbma_yield", "tanbma Yield;  M_{a} (GeV); tan#beta", tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
    hstore["tanbma_combined_sob"]=TH2F("tanbma_Combined_SoB", "tanbma Combined SoB;  M_{a} [GeV]; tan#beta",tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
    hstore["tanbma_combined_son"]=TH2F("tanbma_Combined_SoN", "tanbma Combined SoN;  M_{a} [GeV]; tan#beta",tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
    hstore["tanbma_combined_significance"]=TH2F("tanbma_Combined_Significance", "tanbma Combined Significance;  M_{a} [GeV]; tan#beta",tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
    hstore["tanbma_combined_significance_wsyst"]=TH2F("tanbma_Combined_Significance_wsyst", "tanbma Combined Significance (with background systematic);  M_{a} [GeV]; tan#beta",tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)

    #tanb-mA Grid Histograms (Combined Channels) 
    hstore["tanbmA_xsec"]=TH2F("tanbmA_xsec", "tanbmA xsec;  M_{A} (GeV); tan#beta",tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
    hstore["tanbmA_sum_a"]=TH2F("tanbmA_Average_A", "tanbmA Average A;  M_{A} (GeV); tan#beta", tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
    hstore["tanbmA_sum_ae"]=TH2F("tanbmA_Average_AE", "tanbmA Average Ae;  M_{A} (GeV); tan#beta", tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
    hstore["tanbmA_yield"]=TH2F("tanbmA_yield", "tanbmA Yield;  M_{A} (GeV); tan#beta", tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
    hstore["tanbmA_combined_sob"]=TH2F("tanbmA_Combined_SoB", "tanbmA Combined SoB;  M_{a} [GeV]; tan#beta",tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
    hstore["tanbmA_combined_son"]=TH2F("tanbmA_Combined_SoN", "tanbmA Combined SoN;  M_{a} [GeV]; tan#beta",tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
    hstore["tanbmA_combined_significance"]=TH2F("tanbmA_Combined_Significance", "tanbmA Combined Significance;  M_{a} [GeV]; tan#beta",tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
    hstore["tanbmA_combined_significance_wsyst"]=TH2F("tanbmA_Combined_Significance_wsyst", "tanbmA Combined Significance (with background systemAtic);  M_{a} [GeV]; tan#beta",tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)

    # mDM Scan Graphs and Histograms (Combined Channels)
    mDM_count=0
    hstore["mDM_xsec"]=TH1F("mDM_xsec", "mDM xsec; m_{DM};  Cross Section (fb)",500, 0, 500)
    #hstore["mDM_xsec_format"]=TGraph("mDM_xsec_formatted", "mDM xsec; #frac{2 m_{DM}}{M_{a}};  Cross Section (fb)",0, 5, 0, 200)
    hstore["mDM_xsec_format"]=TGraph()
    hstore["mDM_xsec_format"].SetName("mDM_xsec_formatted")
    
    #sob_error_table=TH2F("SoB_Error", "SoB Error",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    #sob_1sigma_up_table=TH2F("sob_1SigmaUp", "Signal over Background Two Sigma Up",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
    #sob_1sigma_dn_table=TH2F("sob_1SigmaDn", "Signal over Background Two Sigma Down",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)

    sample = Sample()

    tree = TTree("tree", "Tree of 2HDMa Samples")
    tree.Branch("tanb", AddressOf(sample, "tanb"), 'tanb/D') 
    tree.Branch("sinp", AddressOf(sample, "sinp"), 'sinp/D') 
    tree.Branch("mA", AddressOf(sample, "mA"), 'mA/D') 
    tree.Branch("ma", AddressOf(sample, "ma"), 'ma/D') 
    tree.Branch("mDM", AddressOf(sample, "mDM"), 'mDM/D')
    tree.Branch("xsec", AddressOf(sample, "xsec"), "xsec/D")
    tree.Branch("filter", AddressOf(sample, "filter"), "filter/D")
    tree.Branch("sumw", AddressOf(sample, "sumw"), "sumw/D")

    sample_ll = {}
    background_ll = {}

    for channel in CHANNELS:
        sample_ll[channel] = SampleChannel()

        tree.Branch("total_"+channel, AddressOf(sample_ll[channel], "total"), "total/D")
        tree.Branch("final_"+channel, AddressOf(sample_ll[channel], "final"), "final/D")
        tree.Branch("acceptance_"+channel, AddressOf(sample_ll[channel], "acceptance"), "acceptance/D")
        tree.Branch("ae_"+channel, AddressOf(sample_ll[channel], "ae"), "ae/D")
        tree.Branch("ae_error_"+channel, AddressOf(sample_ll[channel], "ae_error"), "ae_error/D")
        tree.Branch("sob_"+channel, AddressOf(sample_ll[channel], "sob"), "sob/D")
        tree.Branch("son_"+channel, AddressOf(sample_ll[channel], "son"), "son/D")
        tree.Branch("significance_"+channel, AddressOf(sample_ll[channel], "significance"), "significance/D")
        tree.Branch("significance_"+channel, AddressOf(sample_ll[channel], "significance_wsyst"), "significance_wsyst/D")
        hstore["mAma_Total_" + channel] = TH2F("mAma_Total_"+str(channel),  "mAma_Total", mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
        hstore["mAma_Final_" + channel] = TH2F("mAma_Final_"+str(channel),  "mAma_Final", mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
        hstore["mAma_acceptance_" + channel] = TH2F("mAma_A"+str(channel), "mAma_A",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
        hstore["mAma_ae_" + channel] = TH2F("mAma_AE"+str(channel), "mAma_Ae",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
        hstore["mAma_sob_" + channel] = TH2F("mAma_SoB"+str(channel), "mAma_SoB",mAma_bs.xbins, mAma_bs.xbinsLow, mAma_bs.ybins, mAma_bs.ybinsLow)
        hstore["tanbma_acceptance_" + channel] = TH2F("tanbma_A"+str(channel), "tanbma_A",tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
        hstore["tanbma_ae_" + channel] = TH2F("tanbma_AE"+str(channel), "tanbma_Ae",tanbma_bs.xbins, tanbma_bs.xbinsLow, tanbma_bs.ybins, tanbma_bs.ybinsLow)
        hstore["tanbmA_acceptance_" + channel] = TH2F("tanbmA_A"+str(channel), "tanbmA_A",tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)
        hstore["tanbmA_ae_" + channel] = TH2F("tanbmA_AE"+str(channel), "tanbmA_Ae",tanbmA_bs.xbins, tanbmA_bs.xbinsLow, tanbmA_bs.ybins, tanbmA_bs.ybinsLow)

    # Same background hist used for all signal samples
    
    for channel in CHANNELS:
        background_ll[channel] = BackgroundChannel()
        background_ll[channel].mtzz_hist = background_file.Get("hgen_SRMZ1" + channel + "_Final_MTZZll2HDMa")

    background_file.Close()


    ##########  Loop over Samples  ###########
    for mp in sample_names:
        print mp
        sample.tanb = mp[0]
        sample.sinp = mp[1]
        sample.mA = mp[2]
        #mH = mA
        sample.ma = mp[3]
        sample.mDM = mp[4]
        
        #Only fill histograms with points in 50 GeV steps
        #if sample.mA % 50 != 0 or sample.ma % 50 != 0: continue
        
        hist_file_name = HIST_FILE_TEMPLATE % {"dataset_name" : sample_names[mp]}
        
        sample.xsec = xsecs[mp]
        sample.filter = filters[mp]
        if(do_rw and sumws[mp] != 0): # Hack (sumw set to 0 for base sample)
            sample.sumw = sumws[mp]

        hist_file = TFile(os.path.join(hist_dir, hist_file_name), "READ")

        for channel in CHANNELS:
            cut_hist = hist_file.Get(CUTFLOW + channel)
            sample_ll[channel].total = cut_hist.GetBinContent(TOTAL_BIN)
            #sample_ll[channel].total = hist_file.Get("sumOfWeights").GetBinContent(2)
            sample_ll[channel].final = cut_hist.GetBinContent(FINAL_BIN)

            sample_ll[channel].acceptance =  sample_ll[channel].final / sample_ll[channel].total

            # Note Filter efficiency technically only applies to the base sample.  For reweighting use sumw.
            if(do_rw and sumws[mp] != 0): # Hack
                sample_ll[channel].ae = sample_ll[channel].final / sample.sumw
                sample_ll[channel].ae_error = CalculateBinomialError(sample.sumw, sample_ll[channel].final)
            else:
                sample_ll[channel].ae =  sample_ll[channel].acceptance * sample.filter
                sample_ll[channel].ae_error = CalculateBinomialError(sample_ll[channel].total / sample.filter, sample_ll[channel].final)
            
            sample_ll[channel].mtzz_hist = hist_file.Get("hgen_SRMZ1" + channel +"_Final_MTZZll2HDMa")

            CalculateSignificances(sample, sample_ll[channel], background_ll[channel])
            
            # mA-ma Grid
            if(sample.tanb == mAma_tanb_fixed and sample.sinp == mAma_sinp_fixed and sample.mDM == mAma_mDM_fixed):
                hstore["mAma_Total_"+channel].Fill(sample.ma, sample.mA, sample_ll[channel].total)
                hstore["mAma_Final_"+channel].Fill(sample.ma, sample.mA, sample_ll[channel].final)
                hstore["mAma_acceptance_" + channel].Fill(sample.ma, sample.mA, sample_ll[channel].acceptance)
                hstore["mAma_ae_" + channel].Fill(sample.ma, sample.mA, sample_ll[channel].ae)
                #hstore["mAma_sob_" + channel].Fill(sample.ma, sample.mA, sample_ll[channel].sob)

            # tanb-ma Grid
            if( sample.mA == tanbma_mA_fixed and sample.sinp == tanbma_sinp_fixed and sample.mDM == tanbma_mDM_fixed):
                hstore["tanbma_acceptance_" + channel].Fill(sample.ma, sample.tanb, sample_ll[channel].acceptance)
                hstore["tanbma_ae_" + channel].Fill(sample.ma, sample.tanb, sample_ll[channel].ae)


            # tanb-mA Grid
            if( sample.ma == tanbmA_ma_fixed and sample.sinp == tanbmA_sinp_fixed and sample.mDM == tanbmA_mDM_fixed):
                hstore["tanbmA_acceptance_" + channel].Fill(sample.mA, sample.tanb, sample_ll[channel].acceptance)
                hstore["tanbmA_ae_" + channel].Fill(sample.mA, sample.tanb, sample_ll[channel].ae)
                #print  sample_ll[channel].ae

        hist_file.Close()
        
        ####### Combine Channels ##########
        """
        #Average Channels
        mAma_average_ae = 0
        for channel in CHANNELS: mAma_average_ae += sample_ll[channel].ae
        mAma_average_ae/len(CHANNELS)
        """

        #Combine Channels
        sum_a = 0
        sum_ae = 0
        sum_ae_error = 0
        #event_yield = 0
        combined_sob = 0
        combined_son = 0
        combined_significance=0 
        combined_significance_wsyst=0

        for channel in CHANNELS:
            sum_a += sample_ll[channel].acceptance
            sum_ae += sample_ll[channel].ae
            sum_ae_error += math.pow(sample_ll[channel].ae_error,2)
            combined_sob += math.pow(sample_ll[channel].sob,2)
            combined_son += math.pow(sample_ll[channel].son,2)
            combined_significance += math.pow(sample_ll[channel].significance,2)
            combined_significance_wsyst += math.pow(sample_ll[channel].significance_wsyst,2)

        #event_yield = sample.xsec * sum_ae * LUMINOSITY
        print "Acceptance: ", sum_a
        
        sum_ae_error = math.pow(sum_ae_error, 0.5)
        combined_sob = math.pow(combined_sob, 0.5)
        combined_son = math.pow(combined_son, 0.5)
        combined_significance = math.pow(combined_significance, 0.5)
        combined_significance_wsyst = math.pow(combined_significance_wsyst, 0.5)

        #mAma Grid
        if(sample.tanb == mAma_tanb_fixed and sample.sinp == mAma_sinp_fixed and sample.mDM == mAma_mDM_fixed and sample.ma%50 == 0 and sample.mA%50 == 0): 
            if(sample.mA >= 600 and sample.mA%100 != 0): continue
            hstore["mAma_xsec"].Fill(sample.ma, sample.mA, sample.xsec)
            hstore["mAma_sum_a"].Fill(sample.ma, sample.mA, sum_a)
            hstore["mAma_sum_ae"].Fill(sample.ma, sample.mA, sum_ae)
            # hstore["mAma_avg_ae"].Fill(sample.ma, sample.mA, mAma_average_ae)
            #hstore["mAma_yield"].Fill(sample.ma, sample.mA, event_yield)
            hstore["mAma_combined_sob"].Fill(sample.ma, sample.mA, combined_sob)
            hstore["mAma_combined_son"].Fill(sample.ma, sample.mA, combined_son)
            hstore["mAma_combined_significance"].Fill(sample.ma, sample.mA, combined_significance)
            hstore["mAma_combined_significance_wsyst"].Fill(sample.ma, sample.mA, combined_significance_wsyst)
        

            """
            #For constant mA-ma 
            if(sample.mA - sample.ma == 500): 
                bin = hstore["difmAma500_combined_acceptance"].FindBin(sample.ma)
                hstore["difmAma500_combined_acceptance"].SetBinContent(bin, sum_ae)
                hstore["difmAma500_combined_acceptance"].SetBinError(bin, sum_ae_error)

            if(sample.mA - sample.ma == 250): 
                bin = hstore["difmAma250_combined_acceptance"].FindBin(sample.ma)
                hstore["difmAma250_combined_acceptance"].SetBinContent(bin, sum_ae)
                hstore["difmAma250_combined_acceptance"].SetBinError(bin, sum_ae_error)
           """

            #for constant mA
            """
            if(sample.mA == 900):
                bin = hstore["mA900_combined_acceptance"].FindBin(sample.ma)
                hstore["mA900_combined_acceptance"].SetBinContent(bin, sum_ae)
                hstore["mA900_combined_acceptance"].SetBinError(bin, sum_ae_error)

            if(sample.mA == 800):
                bin = hstore["mA800_combined_acceptance"].FindBin(sample.ma)
                hstore["mA800_combined_acceptance"].SetBinContent(bin, sum_ae)
                hstore["mA800_combined_acceptance"].SetBinError(bin, sum_ae_error)
            if(sample.mA == 700):
                bin = hstore["mA700_combined_acceptance"].FindBin(sample.ma)
                hstore["mA700_combined_acceptance"].SetBinContent(bin, sum_ae)
                hstore["mA700_combined_acceptance"].SetBinError(bin, sum_ae_error)
            if(sample.mA == 600):
                bin = hstore["mA600_combined_acceptance"].FindBin(sample.ma)
                hstore["mA600_combined_acceptance"].SetBinContent(bin, sum_ae)
                hstore["mA600_combined_acceptance"].SetBinError(bin, sum_ae_error)
             """

        #tanb ma Grid
        if(sample.sinp == tanbma_sinp_fixed and sample.mA == tanbma_mA_fixed and sample.mDM == tanbma_mDM_fixed and sample.ma%50==0):
            #print "tanb-ma:", mp
            hstore["tanbma_xsec"].Fill(sample.ma, sample.tanb, sample.xsec)
            hstore["tanbma_sum_a"].Fill(sample.ma, sample.tanb, sum_a)
            hstore["tanbma_sum_ae"].Fill(sample.ma, sample.tanb, sum_ae)
            #hstore["tanbma_yield"].Fill(sample.ma, sample.tanb, event_yield)
            hstore["tanbma_combined_sob"].Fill(sample.ma, sample.tanb, combined_sob)
            hstore["tanbma_combined_son"].Fill(sample.ma, sample.tanb, combined_son)
            hstore["tanbma_combined_significance"].Fill(sample.ma, sample.tanb, combined_significance)
            hstore["tanbma_combined_significance_wsyst"].Fill(sample.ma, sample.tanb, combined_significance_wsyst)

        #tanb mA Grid
        if(sample.sinp == tanbmA_sinp_fixed and sample.ma == tanbmA_ma_fixed and sample.mDM == tanbmA_mDM_fixed and sample.mA%50==0):
            print "tanb-mA:", mp
            hstore["tanbmA_xsec"].Fill(sample.mA, sample.tanb, sample.xsec)
            hstore["tanbmA_sum_a"].Fill(sample.mA, sample.tanb, sum_a)
            hstore["tanbmA_sum_ae"].Fill(sample.mA, sample.tanb, sum_ae)
            print sum_ae
            hstore["tanbmA_combined_sob"].Fill(sample.mA, sample.tanb, combined_sob)
            hstore["tanbmA_combined_son"].Fill(sample.mA, sample.tanb, combined_son)
            hstore["tanbmA_combined_significance"].Fill(sample.mA, sample.tanb, combined_significance)
            hstore["tanbmA_combined_significance_wsyst"].Fill(sample.mA, sample.tanb, combined_significance_wsyst)
            
        #mDM Scan
        if(sample.tanb == mDM_tanb_fixed and sample.sinp == mDM_sinp_fixed and sample.mA == mDM_mA_fixed and sample.ma == mDM_ma_fixed):
            mDM_count += 1
            hstore["mDM_xsec"].Fill(sample.mDM, sample.xsec)
            hstore["mDM_xsec_format"].SetPoint(mDM_count, 2 * sample.mDM / sample.ma, sample.xsec)

        tree.Fill()


    #sob_1sigma_up_table =  sob_table.Add(sob_error_table, 1) 
    #sob_1sigma_dn_table =  sob_table.Add(sob_error_table, - 1)


    ############ Write and Draw Histograms #############

    outfile.cd()
    tree.Write()
    for hist in hstore:
        print hist
        hstore[hist].Write()

    #DrawTableToPDF(table, title, channel, zrange, ztitle, zlog):
    
    """
    DrawTableToPDF(hstore["mAma_xsec"], "mAma_xsec", "ll", (0.1, 100), "Cross section [fb]", True)
    DrawTableToPDF(hstore["mAma_sum_ae"], "mAma_ae", "ll", (0.0, 0.6), "Acceptance", False)
    DrawTableToPDF(hstore["mAma_yield"], "mAma_yield", "ll", (0.0, 100.0), "Event Yield at 40 fb^{-1}", True)
    DrawTableToPDF(hstore["mAma_combined_son"], "mAma_SoN", "ll", (0.01, 2.0), "Signal over Noise", False)
    DrawTableToPDF(hstore["mAma_combined_significance"], "mAma_Significance", "ll", (0, 2.0), "significance", False)
    DrawTableToPDF(hstore["mAma_combined_significance_wsyst"], "mAma_Significance_wsyst", "ll", (0, 2.0), "significance", False)
    """
    
    DrawTableToPDF(hstore["tanbma_xsec"], "tanbma_xsec", "ll", (0.1, 100), "Cross section [fb]", True, True)
    DrawTableToPDF(hstore["tanbma_sum_a"], "tanbma_a", "ll", (0, 1.0), "Acceptance", True, False)
    DrawTableToPDF(hstore["tanbma_sum_ae"], "tanbma_ae", "ll", (0.01, 0.6), "Acceptance", True, False)
    DrawTableToPDF(hstore["tanbma_combined_son"], "tanbma_SoN", "ll", (0.01, 2.0), "Signal over Noise", True, False)
    DrawTableToPDF(hstore["tanbma_combined_significance"], "tanbma_Significance", "ll", (0, 2.0), "significance", True, False)
    DrawTableToPDF(hstore["tanbma_combined_significance_wsyst"], "tanbma_Significance_wsyst", "ll", (0, 2.0), "significance", True, False)
    
    DrawTableToPDF(hstore["tanbmA_xsec"], "tanbmA_xsec", "ll", (0.1, 100), "Cross section [fb]", True, True)
    DrawTableToPDF(hstore["tanbmA_sum_a"], "tanbmA_a", "ll", (0, 1.0), "Acceptance", True, False)
    DrawTableToPDF(hstore["tanbmA_sum_ae"], "tanbmA_ae", "ll", (0, 01.0), "Acceptance", True, False)
    #DrawTableToPDF(hstore["tanbma_yield"], "tanbma_yield", "ll", (0.0, 100.0), "Event Yield at 40 fb^{-1}", True)
    DrawTableToPDF(hstore["tanbmA_combined_son"], "tanbmA_SoN", "ll", (0.01, 2.0), "Signal over Noise", True, False)
    DrawTableToPDF(hstore["tanbmA_combined_significance"], "tanbmA_Significance", "ll", (0, 2.0), "significance", True, False)
    DrawTableToPDF(hstore["tanbmA_combined_significance_wsyst"], "tanbmA_Significance_wsyst", "ll", (0, 2.0), "significance", True, False)
    
    """
    DrawDifmAmaToPDF(hstore["difmAma500_combined_acceptance"], hstore["difmAma250_combined_acceptance"])
    DrawmAToPDF(hstore["mA900_combined_acceptance"], hstore["mA800_combined_acceptance"], hstore["mA700_combined_acceptance"], hstore["mA600_combined_acceptance"])
    """

def DrawTableToPDF(table, title, channel, zrange, ztitle, ylog, zlog):    
    c1 = TCanvas()
    c1.SetRightMargin(0.16)
    if(ylog): c1.SetLogy()
    if(zlog): c1.SetLogz()

    gStyle.SetOptStat(0)
    gStyle.SetOptStat(0)
    gStyle.SetPalette(kBird)
    #gStyle.SetPalette(kTemperatureMap)
    gStyle.SetPaintTextFormat("4.2f")
    #gStyle.SetTextColor(10)
    
    #contours = array('d', [0, 2, 100]),
    
    table.DrawCopy("Colz");

    table.GetZaxis().SetRangeUser(zrange[0], zrange[1]);
    
    table.GetZaxis().SetTitle(ztitle)
    table.GetZaxis().SetTitleOffset(1.0)
    
    #table.Draw("Colz, text");
    

    #table.GetYaxis().SetRangeUser(0.25, 10)
    # table.GetYaxis().SetMoreLogLabels()
    # table.GetYaxis().SetNdivisions(902);
    table.GetYaxis().SetNoExponent();
    table.DrawCopy("Colz, text");
    table.SetContour(1, array('d', [2]))
    #table.GetYaxis().SetRangeUser(0.3, 9)
    # table.GetYaxis().SetRangeUser(0.5, 9)
    # nbinsy = table.GetNbinsY()
    # table.GetYaxis().SetRangeUser(table.GetYaxis().GetBinCenter(1), table.GetYaxis().GetBinCenter(nbinsy))
    table.SetLineColor(kBlue+1)
    table.SetLineStyle(1) # solid line
    # table.SetLineStyle(9) # dashed line
    table.Draw("cont3, same,")

    gPad.Update()

    table.SetMarkerColor(1)
    table.SetMarkerSize(1.25)

    #table.GetZaxis().SetTitleSize(0.04)
    #table.GetZaxis().SetLabelSize(0.04)
    table.SetTitle("");
    #table.Draw("Colz");

    pt = TPaveText(0.50,0.85,0.77,0.95,"brNDC")
    pt.SetTextSize( 0.05 )
    pt.SetTextFont( 42 )
    pt.SetFillColor( 0 )
    pt.SetFillStyle( 0 )
    pt.SetBorderSize( 0 )
    #pt.AddText( "2HDMa")
    #pt.AddText("M_{a} = 600 GeV, sin#theta = 0.35")
    #pt.AddText( "(" + channel + ")")
    pt.Draw()

    plabel = TPaveText(0.11, 0.92, 0.5, 0.99, "brNDC")
    #plabel.SetNDC();
    plabel.SetTextAlign(11)
    plabel.SetTextFont(42)
    plabel.SetTextSize(0.04)
    plabel.SetTextColor(1)
    plabel.SetFillColor(0)
    plabel.SetFillStyle(0)
    plabel.SetBorderSize(0)
    plabel.AddText("Z(#rightarrow ll) + E_{T}^{miss}")
    plabel.Draw()
    #plabel.DrawLatex(0.1, 0.9, label)
    #plabel.AppendPad()

    c1.SaveAs(FIGURE_DIR + title +"_" + channel + "_2HDMa.png")
    c1.SaveAs(FIGURE_DIR + title +"_" + channel + "_2HDMa.pdf")
    c1.Write()


# Using the signal region events binned by MET, calculate the significance
# and Signal over Background
def CalculateSignificances(sample, sample_channel, background_channel):
    total_significance_wsyst = 0;
    total_significance = 0;
    total_sob = 0;
    total_son = 0;

    signal_hist = sample_channel.mtzz_hist.Clone()
    background_hist = background_channel.mtzz_hist.Clone()
    #zzllnunu_hist = zzllnunu_channel.mtzz_hist.Clone()   

    """
    #Scale Histograms to Luminosity
    signal_hist.Scale(sample.xsec*sample.filter*LUMINOSITY_SIGNIFICANCE/sample_channel.total)
    #zzllnunu_hist.Scale(ZZllnunu_CROSS_SECTION * ZZllnunu_FILTER_EFFICIENCY * LUMINOSITY / zzllnunu_channel.total)
    """

    # Assume a Reconstruction Efficiency of 75% 

    for i in range(1, signal_hist.GetNbinsX()+1):
        s = 0.75 * signal_hist.GetBinContent(i)
        b = background_hist.GetBinContent(i)
        if(b == 0): continue #Should not have 0 entry backgrounds

        # Assume 10% systematic for each bin
        b_syst = 0.1 * b
        #print zzllnunu_hist.GetBinContent(i)
        bin_sob = s / math.pow(b, 0.5)
        bin_son = s / math.pow( s + b + math.pow(0.1*s, 2) + math.pow(b_syst,2), 0.5 )

        total_sob += math.pow(bin_sob, 2)
        total_son += math.pow(bin_son, 2)
        
        bin_significance = 2*( (s + b) * math.log(1 + s/b) -s ) 
        bin_significance = math.pow(bin_significance, 0.5) if bin_significance > 0 else 0

        total_significance += math.pow(bin_significance, 2)

        #print (s + b) * math.log( (s+b)*(b + math.pow(0.2*b,2))/(math.pow(b,2) +(s+b)*math.pow(0.2*b,2)))
        bin_significance_wsyst = 2*( (s + b) * math.log( (s+b) * (b + math.pow(b_syst,2))/(math.pow(b,2) +(s+b)*math.pow(b_syst,2))) - math.pow(b/b_syst,2)*math.log(1 + math.pow(b_syst,2)*s/(b*(b+math.pow(b_syst,2)))))
        bin_significance_wsyst = math.pow(bin_significance_wsyst, 0.5) if bin_significance_wsyst > 0 else 0
        
        total_significance_wsyst += math.pow(bin_significance_wsyst,2)

    total_sob = math.pow(total_sob, 0.5)
    total_sob = math.pow(total_son, 0.5)
    total_significance = math.pow(total_significance, 0.5)
    total_significance_wsyst = math.pow(total_significance_wsyst, 0.5)
   
    sample_channel.sob = total_sob
    sample_channel.son = total_son
    sample_channel.significance = total_significance
    sample_channel.significance_wsyst = total_significance_wsyst


def CalculateBinomialError(total, final):
        n = total 
        n_pass = final
        n_fail = n - n_pass
        error = math.pow( (n_pass * n_fail) / math.pow(n,3), 0.5)
        #print error
        return error

def DrawDifmAmaToPDF(hist500, hist250):
    c1 = TCanvas()
    gStyle.SetOptStat(0)
    hist500.GetYaxis().SetTitle("Acceptance")
    hist500.GetXaxis().SetTitle("M_{a} [GeV]")
    #hist.GetXaxis().SetRangeUser(xrange[0], xrange[1])
    #hist500.SetTitle("Constant M_{a} - M_{a}")
    hist500.SetTitle("")
    
    hist500.SetLineColor(kBlue)
    f1 = TF1("histdif500_fit", "pol0")
    f1.SetRange(125, 450)
    f1.SetLineColor(kBlue)
    f1.SetLineStyle(2)
    hist500.Fit(f1, "R")
 


    hist250.SetLineColor(kRed)
    f2 = TF1("histdif250_fit", "pol0")
    f2.SetRange(125, 450)
    f2.SetLineColor(kRed)
    f2.SetLineStyle(2)
    hist250.Fit(f2, "R")

    hist500.Draw()
    hist250.Draw("same")

    leg = TLegend(0.15, 0.20, 0.5, 0.35)
    leg.AddEntry(hist500, "M_{a} - M_{a} = 500", "l")
    leg.AddEntry(hist250, "M_{a} - M_{a} = 250", "l")

    leg.Draw()

    c1.SaveAs(FIGURE_DIR +"DifmAmaConstant.pdf")

def DrawmAToPDF(hist900, hist800, hist700, hist600):
    c1 = TCanvas()
    gStyle.SetOptStat(0)
    hist800.GetYaxis().SetTitle("Acceptance")
    hist800.GetXaxis().SetTitle("M_{a} [GeV]")
    #hist.GetXaxis().SetRangeUser(xrange[0], xrange[1])                                                                                                                           
    #hist500.SetTitle("Constant M_{a} - M_{a}")                                                                                                                                   
    
    hist800.SetTitle("")
    hist800.SetLineColor(kBlue)
    hist800.GetYaxis().SetRangeUser(0, 0.55)

    f1 = TF1("hist800_fit", "pol2")
    f1.SetLineColor(kBlue)
    #f1.SetLineStyle(2)
    hist800.Fit(f1)

    hist700.SetLineColor(kRed)

    f2 = TF1("hist700_fit", "pol2")
    f2.SetLineColor(kRed)
    hist700.Fit(f2)

    hist600.SetLineColor(kGreen+1)
    f3 = TF1("hist600_fit", "pol2")
    f3.SetLineColor(kGreen+1)
    hist600.Fit(f3)
    
    #hist600.SetLineColor(kYellow)
    #f4 = TF1("hist600_fit", "pol2")
    #f4.SetLineColor(kYellow)
    #hist600.Fit(f4)

   # hist900.Draw()
    hist800.Draw()
    hist700.Draw("same")
    hist600.Draw("same")

    leg = TLegend(0.15, 0.20, 0.5, 0.35)
    #leg.AddEntry(hist900, "M_{a} = 900", "l")
    leg.AddEntry(hist800, "M_{a} = 800", "l")
    leg.AddEntry(hist700, "M_{a} = 700", "l")
    leg.AddEntry(hist600, "M_{a} = 600", "l")
    leg.Draw()

    c1.SaveAs(FIGURE_DIR +"mAConstant.pdf")


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Plot Acceptance Information')
    parser.add_argument('--xsec', type=str, help="Path to pickle file with xsec info")
    parser.add_argument('--indir', type=str, help="Pat to directory of histograms")
    parser.add_argument('--rw', action='store_true', default=False, dest='do_rw', help="Flag if datasets contain reweighting weights")
    args = parser.parse_args()

    if len(sys.argv) < 3:
        print "Pass name of text file to convert and directory of histograms"
    AcceptanceTable(args.xsec, args.indir, args.do_rw)
