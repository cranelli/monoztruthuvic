# Producing Acceptance Tables
> ./extractXsec.py /hep300/data-shared/MonoZ/Rel21/mcuser/signal_2HDMa_ggF_MGReweight/Logs

Scripts run over the samples in the txt file.  If you want to run over a subset of the samples, 
create a new txt file containing only samples of interest. For example:
> cat signal_2HDMa_ggF_MGReweight.txt | grep v404 | grep sp0p7 > signal_2HDMa_ggF_MGReweight_base_sp0p7.txt

ConvertXsecTxtToPkle.py runs over samples in txt files and stores them in python data structures (dictionaries)
The --avg and --rw flags can be used to combine xsec info for samles that have multiple jobs, or to handle 
samples that contain reweighing weights.
Note, it is usually not necessary to run this script directly, and it is instead called from within other
python scripts (a pickle file is still created).
> ./ConvertXsecTxtToPkle.py signal_2HDMa_ggF_MGReweight_base_sp0p7.txt --avg --rw

CrossSectionTable.py runs just over information in xsec txt file.
> ./CrossSectionTable.py signal_2HDMa_ggF_MGReweight_base_sp0p7.txt --rw

AcceptanceRatioTable.py uses 
> ./AcceptanceTables.py signal_2HDMa_ggF_MGReweight_base_sp0p7.txt --rw
