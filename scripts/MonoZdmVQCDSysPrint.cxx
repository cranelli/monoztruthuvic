void printAllCanvases(TString fn = "out.pdf") {
  // print all canvases to file
  TCanvas *c = 0;
  int nc = gROOT->GetListOfCanvases()->GetSize();
  // std::cout << "number of canvas = " << nc << std::endl;
  int ic = 0;
  TIter next(gROOT->GetListOfCanvases());
  while ((c = (TCanvas *)next())) {
    if (nc == 1) {
      c->Print(fn, "pdf"); 
    } else {
      if (ic == 0) {
	c->Print(fn+"(", "pdf"); 
      } else if (ic == nc-1) {
	c->Print(fn+")", "pdf");
      } else {
	c->Print(fn, "pdf");
      }
    }
    ++ic;
  }
}

TString extractMedMass(TString dmSampleName) {
  // extract mass from histogram
  TString mMed = dmSampleName;
  TString cut = "_MM";
  // obtain the location of "_MM"
  int k = mMed.Index(cut);
  // remove all characters before
  // e.g. this removes dmV_DMX_MM
  mMed.Remove(0, k+cut.Length());
  return mMed;
}

void MonoZdmVQCDSysPrint(TString hnTag0 = "SRMZ1", TString hnTag1 = "METTST", TString hnTag2 = "Final", bool relativePath = true)
{
  // --- THIS SCRIPT: 
  // runs on MonoZTruthUVic output of DM signal samples with modified scale factors = 0.5, 1.0 (nominal), and 2.0
  // for each sample it obtains the final yields and calculates the systematic error on the signal acceptance
  // it then plots the error as a function of Mmed and fits a function for interpolation between mediator masses
  // outputs a text file with a complete list of DM signal samples and errors obtained from the fit

  // --- TO RUN:
  // root -l -b -q scripts/MonoZdmVQCDSysPrint.cxx

  // --- MODIFY UP TO **********

  // original TRUTH1 DAODs with 1M events are located here: /hep300/data/kaylamc/DMDAODFiles/dmV/1M
  // directories with MonoZTruthUVic output
  std::vector<TString> dmDirs;
  // Moriond 2017
  dmDirs.push_back("/hep300/data/kaylamc/MonoZ/git/AnalysisBase2.4.20/jobs/truth_20170207_061811");
  // ICHEP 2016
  //dmDirs.push_back("/home1s/kaylamc/Mono-Z/MonoZROOTCoreBase2.4.14/jobs/truth_20160708_134331"); // dmV_DM1_MM10
  //dmDirs.push_back("/home1s/kaylamc/Mono-Z/MonoZROOTCoreBase2.4.14/jobs/truth_20160708_142416"); // dmV_DM1_MM100
  //dmDirs.push_back("/home1s/kaylamc/Mono-Z/MonoZROOTCoreBase2.4.14/jobs/truth_20160708_151512"); // dmV_DM1_MM300
  //dmDirs.push_back("/home1s/kaylamc/Mono-Z/MonoZROOTCoreBase2.4.14/jobs/truth_20160708_160322"); // dmV_DM10_MM100
  //dmDirs.push_back("/home1s/kaylamc/Mono-Z/MonoZROOTCoreBase2.4.14/jobs/truth_20160708_164458"); // dmV_DM50_MM95
  //dmDirs.push_back("/home1s/kaylamc/Mono-Z/MonoZROOTCoreBase2.4.14/jobs/truth_20160708_173845"); // dmV_DM50_MM300
  //dmDirs.push_back("/home1s/kaylamc/Mono-Z/MonoZROOTCoreBase2.4.14/jobs/truth_20160708_181928"); // dmV_DM500_MM995

  // set which correlation to use for rho(A,B)
  // only choose one
  TString rho = "0.5"; // default
  //TString rho = "0";
  //TString rho = "1";
  
  // list of samples file names used above
  // these are the files used to obtain the fit function
  std::map<TString, TString> truthfnMap;
  truthfnMap["dmV_DM1_MM10"]    = "dmV_1_10";
  truthfnMap["dmV_DM1_MM100"]   = "dmV_1_100";
  truthfnMap["dmV_DM1_MM300"]   = "dmV_1_300";
  truthfnMap["dmV_DM10_MM100"]  = "dmV_10_100";
  truthfnMap["dmV_DM50_MM95"]   = "dmV_50_95";
  truthfnMap["dmV_DM50_MM300"]  = "dmV_50_300";
  truthfnMap["dmV_DM500_MM995"] = "dmV_500_995";

  // complete list of vector DM models
  // QCD errors are obtained for these using the mass points above
  std::map<TString, TString> userrnMap;
  userrnMap["303511"] = "dmV_DM1_MM10";
  userrnMap["303512"] = "dmV_DM1_MM100";
  userrnMap["303513"] = "dmV_DM1_MM300";
  userrnMap["303514"] = "dmV_DM1_MM2000";          
  userrnMap["303515"] = "dmV_DM10_MM10";
  userrnMap["303516"] = "dmV_DM10_MM100";
  userrnMap["303517"] = "dmV_DM10_MM10000";
  userrnMap["303518"] = "dmV_DM50_MM10";
  userrnMap["303519"] = "dmV_DM50_MM95";
  userrnMap["303520"] = "dmV_DM50_MM300";
  userrnMap["303521"] = "dmV_DM150_MM10";
  userrnMap["303522"] = "dmV_DM150_MM295";
  userrnMap["303523"] = "dmV_DM150_MM1000";
  userrnMap["303524"] = "dmV_DM500_MM10";
  userrnMap["303525"] = "dmV_DM500_MM995";
  userrnMap["303526"] = "dmV_DM500_MM2000";
  userrnMap["303527"] = "dmV_DM500_MM10000";
  userrnMap["303528"] = "dmV_DM1000_MM10";
  userrnMap["303529"] = "dmV_DM1000_MM1000";
  userrnMap["303530"] = "dmV_DM1000_MM1995";
  userrnMap["303551"] = "dmV_DM50_MM95";
  userrnMap["303552"] = "dmV_DM500_MM995";
  userrnMap["305710"] = "dmV_DM1_MM500";
  userrnMap["305711"] = "dmV_DM1_MM700"; 
  userrnMap["305712"] = "dmV_DM10_MM300";     
  userrnMap["305713"] = "dmV_DM10_MM500";
  userrnMap["305714"] = "dmV_DM30_MM10";       
  userrnMap["305715"] = "dmV_DM30_MM100";     
  userrnMap["305716"] = "dmV_DM30_MM300";     
  userrnMap["305717"] = "dmV_DM30_MM500";     
  userrnMap["305718"] = "dmV_DM30_MM700";  
  userrnMap["305719"] = "dmV_DM50_MM500";     
  userrnMap["305720"] = "dmV_DM50_MM700";
  userrnMap["305721"] = "dmV_DM100_MM100";    
  userrnMap["305722"] = "dmV_DM100_MM300";    
  userrnMap["305723"] = "dmV_DM100_MM500";    
  userrnMap["305724"] = "dmV_DM100_MM700";
  userrnMap["306080"] = "dmV_DM1_MM50";
  userrnMap["306081"] = "dmV_DM30_MM50";
  userrnMap["306082"] = "dmV_DM55_MM100";
  userrnMap["306083"] = "dmV_DM25_MM150";
  userrnMap["306084"] = "dmV_DM80_MM150";
  userrnMap["306085"] = "dmV_DM1_MM200";
  userrnMap["306086"] = "dmV_DM50_MM200";
  userrnMap["306087"] = "dmV_DM105_MM200";
  userrnMap["306088"] = "dmV_DM150_MM200";
  userrnMap["306089"] = "dmV_DM130_MM250";
  userrnMap["306090"] = "dmV_DM155_MM300";
  userrnMap["306091"] = "dmV_DM200_MM300";
  userrnMap["306092"] = "dmV_DM180_MM350";
  userrnMap["306093"] = "dmV_DM1_MM400";
  userrnMap["306094"] = "dmV_DM50_MM400";
  userrnMap["306095"] = "dmV_DM100_MM400";
  userrnMap["306096"] = "dmV_DM150_MM400";
  userrnMap["306097"] = "dmV_DM205_MM400";
  userrnMap["306098"] = "dmV_DM250_MM400";
  userrnMap["306099"] = "dmV_DM230_MM450";
  userrnMap["306100"] = "dmV_DM200_MM500";
  userrnMap["306101"] = "dmV_DM255_MM500";
  userrnMap["306102"] = "dmV_DM280_MM550";
  userrnMap["306103"] = "dmV_DM1_MM600";
  userrnMap["306104"] = "dmV_DM50_MM600";
  userrnMap["306105"] = "dmV_DM100_MM600";
  userrnMap["306106"] = "dmV_DM200_MM600";
  userrnMap["306107"] = "dmV_DM200_MM700";
  userrnMap["306108"] = "dmV_DM300_MM700";
  userrnMap["306109"] = "dmV_DM1_MM800";
  userrnMap["306110"] = "dmV_DM100_MM800";
  userrnMap["306111"] = "dmV_DM200_MM800";
  userrnMap["306112"] = "dmV_DM300_MM800";
  
  // **********

  TString rDir = Form("%s", gSystem->Getenv("ROOTCOREBIN"));  // $ROOTCOREBIN
  TString wDir = Form("%s", gSystem->DirName(rDir));          // work directory, above $ROOTCOREBIN
  TString oDir = wDir + "/MonoZUVic/";                        // output directory for text file

  ////////////////////////////////////////////////////////
  // CALCULATE SYSTEMATIC ERROR FOR INPUT TRUTH SAMPLES //
  ////////////////////////////////////////////////////////

  // prepare maps to store systematics for each sample
  std::map<TString, double> errMapee;
  std::map<TString, double> errMapmm;
  // also keep their statistical errors for plotting
  std::map<TString, double> errMapeeStat;
  std::map<TString, double> errMapmmStat;

  std::cout << "Ignore the following block of errors:" << std::endl;

  // loop over truth directories 
  for (std::vector<TString>::iterator iDir = dmDirs.begin(); iDir != dmDirs.end(); ++iDir) {

    TString dmDir = *iDir;

    // loop over each truth DM sample 
    for (std::map<TString, TString>::iterator iSamp = truthfnMap.begin(); iSamp != truthfnMap.end(); ++iSamp) {

      TString dmSampleName = iSamp->first;
      TString dmFileName = iSamp->second;
      std::cout << dmSampleName << " ........................................." << std::endl;
      // store yields for this sample
      double yield0ee = 0; double yield0mm = 0;
      double yield5ee = 0; double yield5mm = 0;
      double yield2ee = 0; double yield2mm = 0;
      // store errors
      double yieldErr0ee = 0; double yieldErr0mm = 0;
      double yieldErr5ee = 0; double yieldErr5mm = 0;
      double yieldErr2ee = 0; double yieldErr2mm = 0;
      // path to truth files
      std::vector<TString> truthFilePaths;
      truthFilePaths.push_back(dmDir + "/hist-mcUVic." + dmFileName + "_Sc0_1M.root");
      truthFilePaths.push_back(dmDir + "/hist-mcUVic." + dmFileName + "_Sc5_1M.root");
      truthFilePaths.push_back(dmDir + "/hist-mcUVic." + dmFileName + "_Sc2_1M.root");

      // test if files exist
      // this will produce an error but it can be safely ignored, although the output will look a bit scary
      TFile* ftest = new TFile(truthFilePaths[0]);
      if (ftest->IsZombie()) {
	ftest->Close();
	delete ftest;
	ftest = 0;
	continue;
      }
      else (ftest->Close());

      // loop over scale factors (0.5, 1.0 (nominal), 2.0)
      for (std::vector<TString>::size_type iScale = 0; iScale != truthFilePaths.size(); iScale++) {

	TFile* f = new TFile(truthFilePaths[iScale]);
	TIter next(f->GetListOfKeys());
	TKey* key;

	// loop over histograms
	while ((key=(TKey*)next())) {

	  // get key (histo) name
	  TString hn = key->GetName();       
	  // only look at signal region cutflow histograms
	  if (!(hn.Contains("SRMZ1") && hn.Contains("hcutflow"))) continue;
	  TH1* h = (TH1*)f->Get(hn)->Clone(hn + "_clone");

      	  // loop over cutflow bins
      	  for (int bin = 1; bin <= h->GetNbinsX(); ++bin) {

      	    // extract yield in Final bin
      	    TString label = h->GetXaxis()->GetBinLabel(bin);
	    
      	    // nominal
      	    if (label.Contains("Final") && truthFilePaths[iScale].Contains("Sc0")) {
      	      if (hn.Contains("ee")) {
      		yield0ee = h->GetBinContent(bin);
      		yieldErr0ee = h->GetBinError(bin);
      	      }
      	      else if (hn.Contains("mm")) {
      		yield0mm = h->GetBinContent(bin);
      		yieldErr0mm = h->GetBinError(bin);
      	      }
      	    }
      	    // x0.5 scale
      	    if (label.Contains("Final") && truthFilePaths[iScale].Contains("Sc5")) {
      	      if (hn.Contains("ee")) {
      		yield5ee = h->GetBinContent(bin);
      		yieldErr5ee = h->GetBinError(bin);
      	      }
      	      else if (hn.Contains("mm")) {
      		yield5mm = h->GetBinContent(bin);
      		yieldErr5mm = h->GetBinError(bin);
      	      }
      	    }
      	    // x2.0 scale
      	    if (label.Contains("Final") && truthFilePaths[iScale].Contains("Sc2")) {
      	      if (hn.Contains("ee")) {
      		yield2ee = h->GetBinContent(bin);
      		yieldErr2ee = h->GetBinError(bin);
      	      }
      	      else if (hn.Contains("mm")) {
      		yield2mm = h->GetBinContent(bin);
      		yieldErr2mm = h->GetBinError(bin);
      	      }
      	    }
	    
      	  } // end loop over cutflow bins
	} // end loop over histograms
      } // end loop over scale factors
      
      
      std::cout << "  ee channel:" << std::endl;
      std::cout << "    SF*1.0: " << yield0ee << " +/- " << yieldErr0ee << std::endl;
      std::cout << "    SF*0.5: " << yield5ee << " +/- " << yieldErr5ee << std::endl;
      std::cout << "    SF*2.0: " << yield2ee << " +/- " << yieldErr2ee << std::endl;
      
      // prepare systematics  and their statistical errors 
      double eeSyst = 0;
      double eeSystErr = 0;
      // calculate difference wrt nominal
      double diff5ee = abs(yield0ee - yield5ee);
      double diff2ee = abs(yield0ee - yield2ee);
      std::cout << "      Diff wrt nominal: " << std::endl;
      // ee
      std::cout << "        SF*0.5: " << diff5ee << std::endl;;
      std::cout << "        SF*2.0: " << diff2ee << std::endl;
      if (diff5ee > diff2ee) {
	std::cout << "          Taking error from SF*0.5 " << std::endl;
      	eeSyst = diff5ee/yield0ee;
	if (rho == "1") eeSystErr = (1/yield0ee)*sqrt(std::abs(yieldErr0ee*yieldErr0ee - yieldErr5ee*yieldErr5ee)); // rho = 1
	if (rho == "0") eeSystErr = (yield5ee/yield0ee)*sqrt(pow(yieldErr0ee/yield0ee,2) + pow(yieldErr5ee/yield5ee,2)); // rho = 0
	if (rho == "0.5") eeSystErr = sqrt((pow(yield5ee,2)/pow(yield0ee,4))*pow(yieldErr0ee,2) + (1/pow(yield0ee,2))*pow(yieldErr5ee,2) - (yield5ee/pow(yield0ee,3))*yieldErr0ee*yieldErr5ee); // rho = 0.5
      }
      if (diff5ee < diff2ee) {
	std::cout << "          Taking error from SF*2.0 " << std::endl;
      	eeSyst = diff2ee/yield0ee;
	if (rho == "1") eeSystErr = (1/yield0ee)*sqrt(std::abs(yieldErr0ee*yieldErr0ee - yieldErr2ee*yieldErr2ee)); // rho = 1
	if (rho == "0") eeSystErr = (yield2ee/yield0ee)*sqrt(pow(yieldErr0ee/yield0ee,2) + pow(yieldErr2ee/yield2ee,2)); // rho = 0
	if (rho == "0.5") eeSystErr = sqrt((pow(yield2ee,2)/pow(yield0ee,4))*pow(yieldErr0ee,2) + (1/pow(yield0ee,2))*pow(yieldErr2ee,2) - (yield2ee/pow(yield0ee,3))*yieldErr0ee*yieldErr2ee); // rho = 0.5
      }
      std::cout << "          Systematic: " << eeSyst << " +/- " << eeSystErr << " (" << (eeSystErr/eeSyst)*100 << "%)" << std::endl;

      std::cout << "  mm channel:" << std::endl;
      std::cout << "    SF*1.0: " << yield0mm << " +/- " << yieldErr0mm << std::endl;
      std::cout << "    SF*0.5: " << yield5mm << " +/- " << yieldErr5mm << std::endl;
      std::cout << "    SF*2.0: " << yield2mm << " +/- " << yieldErr2mm << std::endl;

      // prepare systematics and their statistical errors 
      double mmSyst = 0;
      double mmSystErr = 0;
      // calculate difference wrt nominal
      double diff5mm = abs(yield0mm - yield5mm);
      double diff2mm = abs(yield0mm - yield2mm);
      std::cout << "      Diff wrt nominal: " << std::endl;
      // mm
      std::cout << "        SF*0.5: " << diff5mm << std::endl;
      std::cout << "        SF*2.0: " << diff2mm << std::endl;
      if (diff5mm > diff2mm) {
	std::cout << "          Taking error from SF*0.5 " << std::endl;
      	mmSyst = diff5mm/yield0mm;
	if (rho == "1") mmSystErr = (1/yield0mm)*sqrt(std::abs(yieldErr0mm*yieldErr0mm - yieldErr5mm*yieldErr5mm)); // rho = 1
	if (rho == "0") mmSystErr = (yield5mm/yield0mm)*sqrt(pow(yieldErr0mm/yield0mm,2) + pow(yieldErr5mm/yield5mm,2)); // rho = 0 
	if (rho == "0.5") mmSystErr = sqrt((pow(yield5mm,2)/pow(yield0mm,4))*pow(yieldErr0mm,2) + (1/pow(yield0mm,2))*pow(yieldErr5mm,2) - (yield5mm/pow(yield0mm,3))*yieldErr0mm*yieldErr5mm); // rho = 0.5
      }
      if (diff5mm < diff2mm) {
	std::cout << "          Taking error from SF*2.0 " << std::endl;
      	mmSyst = diff2mm/yield0mm;
	if (rho == "1") mmSystErr = (1/yield0mm)*sqrt(std::abs(yieldErr0mm*yieldErr0mm - yieldErr2mm*yieldErr2mm)); // rho = 1
	if (rho == "0") mmSystErr = (yield2mm/yield0mm)*sqrt(pow(yieldErr0mm/yield0mm,2) + pow(yieldErr2mm/yield2mm,2)); // rho = 0
	if (rho == "0.5") mmSystErr = sqrt((pow(yield2mm,2)/pow(yield0mm,4))*pow(yieldErr0mm,2) + (1/pow(yield0mm,2))*pow(yieldErr2mm,2) - (yield2mm/pow(yield0mm,3))*yieldErr0mm*yieldErr2mm); // rho = 0.5
      }
      std::cout << "          Systematic: " << mmSyst << " +/- " << mmSystErr << " (" << (mmSystErr/mmSyst)*100 << "%)" << std::endl;
      std::cout << std::endl;

      // extract systematic error
      errMapee[dmSampleName] = eeSyst;
      errMapmm[dmSampleName] = mmSyst;
      errMapeeStat[dmSampleName] = eeSystErr;
      errMapmmStat[dmSampleName] = mmSystErr;

    } // end loop over DM samples
  } // end loop over directories

  ///////////////////////////////////////////
  // INTERPOLATE BETWEEN SYSTEMATIC ERRORS //
  ///////////////////////////////////////////

    // create dataset vectors from Mmed (x) and error (y)
  std::vector<double> mMedVec;
  std::vector<double> mMedCombVec;
  std::vector<double> erreeVec;
  std::vector<double> errmmVec;
  std::vector<double> errCombVec; // comb
  std::vector<double> erreeStatVec;
  std::vector<double> errmmStatVec;
  std::vector<double> errCombStatVec; // comb
  // loop over all input samples
  for (std::map<TString, double>::iterator idm = errMapee.begin(); idm != errMapee.end(); ++idm) {
    // extract mediator mass from sample key
    TString dmSampleName = idm->first;
    TString m = extractMedMass(dmSampleName);
    mMedVec.push_back(std::stod(m.Data()));
    mMedCombVec.push_back(std::stod(m.Data()));           // comb
    mMedCombVec.push_back(std::stod(m.Data()));           // comb
    erreeVec.push_back(errMapee[dmSampleName]);
    errmmVec.push_back(errMapmm[dmSampleName]);
    errCombVec.push_back(errMapee[dmSampleName]);         // comb 
    errCombVec.push_back(errMapmm[dmSampleName]);         // comb
    erreeStatVec.push_back(errMapeeStat[dmSampleName]);
    errmmStatVec.push_back(errMapmmStat[dmSampleName]);
    errCombStatVec.push_back(errMapeeStat[dmSampleName]); // comb
    errCombStatVec.push_back(errMapmmStat[dmSampleName]); // comb
  }

  // convert vectors to correct format for TGraph
  int N = mMedVec.size();
  int M = mMedCombVec.size();
  Double_t mMed[N];
  Double_t mMedComb[M]; // comb
  Double_t erree[N];
  Double_t errmm[N];
  Double_t errComb[M]; //comb
  Double_t erreeStat[N];
  Double_t errmmStat[N];
  Double_t errCombStat[M]; //comb
  for (int j = 0; j < N; j++) {
    mMed[j] = mMedVec[j];
    erree[j] = erreeVec[j];
    errmm[j] = errmmVec[j];
    erreeStat[j] = erreeStatVec[j];
    errmmStat[j] = errmmStatVec[j];

  }
  for (int j = 0; j < M; j++) {
    mMedComb[j] = mMedCombVec[j];
    errComb[j] = errCombVec[j]; // comb
    errCombStat[j] = errCombStatVec[j]; // comb
  }

  // fit function
  TF1 *fitf = new TF1("fitf", "[0] + [1]*exp(-1*[2]*x)", 0, 1000);
  fitf->SetParameters(0.001, 0.001, 0.001, 0.001);
  fitf->SetParLimits(2, 0, 10000); // forces exponential to decay -->

  // graphs to plot and fit to
  TGraphErrors *g = new TGraphErrors(M, mMedComb, errComb, 0, errCombStat);
  //TGraphErrors *gee = new TGraphErrors(N, mMed, erree, 0, erreeStat);
  TGraphErrors *gmm = new TGraphErrors(N, mMed, errmm, 0, errmmStat);

  // prepare canvas
  TCanvas *c = new TCanvas("c", "c", 700, 500);
  c->SetLeftMargin(0.1);
  c->SetTopMargin(0.02);
  c->SetRightMargin(0.02);
  //c->SetLogy();
  // cosmetics
  g->SetTitle("");
  g->GetXaxis()->SetTitle("Mediator mass [GeV]");
  g->GetYaxis()->SetTitle("Systematic error (relative)");
  // colours
  g->SetMarkerStyle(21); 
  g->SetMarkerColor(kRed);
  g->GetYaxis()->SetLabelSize(0.04);
  g->GetXaxis()->SetLabelSize(0.04);
  g->GetYaxis()->SetTitleSize(0.04);
  g->GetXaxis()->SetTitleSize(0.04);
  g->GetYaxis()->SetTitleOffset(1.15);
  // // draw
  g->Draw("AP"); 
  // make fit
  std::cout << "Making fit (ee and mm combined)..." << std::endl; 
  g->Fit(fitf); 
  g->GetFunction("fitf")->SetLineColor(kBlack); 
  // draw mumu points overtop
  gmm->SetMarkerStyle(21);
  gmm->SetMarkerColor(kBlue);
  gmm->Draw("sameP");
  // extract fit
  TF1 *fit = g->GetFunction("fitf"); //comb

  std::cout << std::endl;
  std::cout << "  Chi square = " << fit->GetChisquare() << std::endl;
  std::cout << "  Ndof = " << fit->GetNDF() << std::endl;
  std::cout << "  Prob = " << fit->GetProb() << std::endl;
  std::cout << std::endl;

  // use fit to obtain error for a given mediator mass
  // output to file
  std::cout << "Outputting errors to qcdSys_dmV.txt..." << std::endl;
  ofstream out_data("qcdSys_dmV.txt");
  // header
  out_data << std::left << std::setw(10) << "runNumber";
  out_data << std::right << std::setw(20) << "SRMZ1ee(%)" << std::setw(20) << "SRMZ1mm(%)" << std::endl;
  // loop over all DM samples
  for (std::map<TString, TString>::iterator idm = userrnMap.begin(); idm != userrnMap.end(); ++idm) {
    TString runNumber = idm->first;
    TString dmSample = idm->second;
    // find mediator mass 
    double m = std::stod((extractMedMass(dmSample)).Data());
    // output to file
    out_data << std::left << std::setw(10) << runNumber;
    out_data << std::right << std::setw(20) << fit->Eval(m) << std::setw(20) << fit->Eval(m) << std::endl;
  }

  // print canvases to pdf
  TString pdfFileName = "qcdSys_dmVplots.pdf";
  printAllCanvases(pdfFileName);
  std::cout << "Printed canvases to file " << pdfFileName << std::endl;

  std::cout << "Normal termination of MonoZdmVQCDSysPrint!" << std::endl;
}
